package com.company.codewars;

import java.util.Arrays;

public class OddOrEven {
    public static void main(String[] args) {

    }

    public static String oddOrEven(int[] array) {
        if (array.length == 0) {
            return "even";
        }
        return (Arrays.stream(array).sum()) % 2 == 0 ? "even" : "odd";
    }

    public static double find_average (int[]array){
        double numOfElements = array.length;
        if(array!=null) {
            return Arrays.stream(array).sum() / numOfElements;
        }
        return 0;
    }
}
