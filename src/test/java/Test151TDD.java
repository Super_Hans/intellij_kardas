import com.company.zadanie151.Persona;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Test151TDD {
    private Persona persona;

    // will call before EVERY test
    @BeforeEach
    void setPersona() {
        persona = new Persona();
    }

    @Test
    void shouldReturnFullDataWhenNameAndSurnameIsSet() {
        persona.setName("Marian");
        persona.setSurname("Nowak");

        Assertions.assertEquals("Marian Nowak", persona.getFullData());
    }

    @Test
    void shouldReturnOnlyNameWhenIsSet() {
        persona.setName("Joanna");

        Assertions.assertEquals("Joanna", persona.getFullData());
    }

    @Test
    void shouldReturnOnlySurnameWhenIsSet() {
        String surname = "Miller";
        persona.setSurname(surname);

        Assertions.assertEquals(surname, persona.getFullData());
    }

    @Test
    void shouldReturnNullWhenNothingIsSet() {
        Assertions.assertNull(persona.getFullData());
    }

    @Test
    void shouldReturnTrueWhenAgeIsSameOrAbove18() {
        int age = 19;
        persona.setAge(age);

        Assertions.assertTrue(persona.isAdult());
    }

    @Test
    void shouldReturnFalseWhenAgeBelow18() {
        int age = 7;
        persona.setAge(age);

        Assertions.assertFalse(persona.isAdult());
    }

    @Test
    void shouldReturnFalseWhenNegativeAge() {
        Assertions.assertEquals(0, persona.getAge());

        int positiveAge = 20;
        persona.setAge(positiveAge);
        Assertions.assertEquals(20, persona.getAge());

        int negativAge = -4;
        persona.setAge(negativAge);
        Assertions.assertEquals(positiveAge, persona.getAge());
        Assertions.assertNotEquals(negativAge, persona.getAge());

    }

    /**
     * test for years to pension
     */
    @Test
    void shouldReturnYearsOfPensionForWoman() {
        int age = 66;
        persona.setAge(age);
        persona.setMan(false);

        Assertions.assertEquals(0, persona.howManyYearsToPension());

    }

    @Test
    void shouldReturnYearsOfPensionForMan() {
        int age = 90;
        persona.setAge(age);
        persona.setMan(true);

        Assertions.assertEquals(0, persona.howManyYearsToPension());
    }

    @Test
    void shouldReturnYearsToPensionForWoman() {
        int age = 25;
        persona.setAge(age);
        persona.setMan(false);

        Assertions.assertEquals(40, persona.howManyYearsToPension());
    }

    @Test
    void shouldReturnYearsToPensionForMan() {
        int age = 45;
        persona.setAge(age);
        persona.setMan(true);

        Assertions.assertEquals(22, persona.howManyYearsToPension());
    }

    @Test
    void shouldReturnMinusOneWhenAgeToPensionIsZero() {
        persona.setMan(false);
        Assertions.assertEquals(-1,persona.howManyYearsToPension());

        persona.setMan(true);
        Assertions.assertEquals(-1,persona.howManyYearsToPension());

    }
    /**Homework testing*/

    @Test
    void shouldReturnCorrectLoginWhileNameAndSurnameSet() {
        persona.setName("Eben");
        persona.setSurname("Etezbeth");

        Assertions.assertEquals("ebeete12",persona.setLogin());
    }

    @Test
    void shouldReturnXXXInNameWhenNameNotSet () {
        persona.setSurname("Miller");

        Assertions.assertEquals("XXXmil6",persona.setLogin());
    }

    @Test
    void shouldReturnYYYInSurnameWhenSurameNotSet () {
        persona.setName("Karl");

        Assertions.assertEquals("karYYY4",persona.setLogin());
    }
    @Test
    void shouldReturnNullWhenNoDataHasBeenSet() {

        Assertions.assertNull(persona.setLogin());
    }

    @Test
    void shouldReturnLoginWithZIfNameOrSurnameLowerThanThreeChars(){
        persona.setName("Ed");
        persona.setSurname("Engel");

        Assertions.assertEquals("edZeng7",persona.setLogin());
    }


}

