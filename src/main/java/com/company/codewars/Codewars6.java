package com.company;

public class Codewars6 {
    public static void main(String[] args) {
        System.out.print(summation(8));
    }

    public static int summation(int n) {
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            sum += i;
        }
        return sum;
    }
}





