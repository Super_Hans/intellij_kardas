package com.company.codewars;

public class CorrectTimeString {
    public static void main(String[] args) {
        System.out.println(timeCorrect("23:72:99"));
    }

    static String timeCorrect(String timestring) {

        if (timestring==null ||
                !timestring.matches("^\\d{2}:[0-9]{2}:[0-9]{2}$")) {
            return null;
        }

        int timeDivisor = 60;
        int dayDivisor = 24;
        String[] strings = timestring.split(":");

        int hours = Integer.parseInt(strings[0]);
        int minutes = Integer.parseInt(strings[1]);
        int seconds = Integer.parseInt(strings[2]);
        //fixing seconds
        minutes += seconds / timeDivisor;
        seconds = seconds % timeDivisor;
        //fixing minutes
        hours += minutes / timeDivisor;
        minutes = minutes % timeDivisor;

        // hours must skip to another day
        hours = hours % dayDivisor;

        return String.format("%02d:%02d:%02d",
                hours,
                minutes,
                seconds);
    }
}
