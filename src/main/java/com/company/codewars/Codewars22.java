package com.company.codewars;

public class Codewars22 {
    public static void main(String[] args) {
        System.out.println(squareDigits(91));
    }

    static int squareDigits(int n) {
        String s = n + "";
        String[] digits = s.split("");
        StringBuilder output = new StringBuilder();

        for (String str : digits) {
            int i = Integer.parseInt(str);
            output.append(i * i);
        }

        return Integer.parseInt(output.toString());
    }
}
