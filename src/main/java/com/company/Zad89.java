package com.company;

//*ZADANIE #89*
//Utwórz metodę, która przyjmuje ciąg w formie `"<nazwa operacji> <operacja>"`.
// Na bazie nazwy operacji należy rozpoznać separator operacji.
//> Dla `"add 5+4+99"` zwróci `108`
//> Dla `"subtract 100-20"` zwróci `80`
//> Dla `"multiply 8*17"` zwróci `136`
//> Dla `"divide 200/25"` zwróci `8`
public class Zad89 {
    // static variable
    private static final String SYMBOL = " ";

    private static final String OPERATION_ADDING = "add";
    private static final String ADDING_SYMBOL = "\\+";
    private static final String OPERATION_SUBTRACT = "subtract";
    private static final String SUBTRACT_SYMBOL = "\\-";
    private static final String OPERATION_MULTPLY = "multiply";
    private static final String MULTIPLY_SYMBOL = "\\*";
    private static final String OPERATION_DIVIDE = "divide";
    private static final String DIVIDE_SYMBOL = "\\/";

    public static void main(String[] args) {
        System.out.println(getRes("add 7+66+13"));
        System.out.println(getRes("subtract 100-20"));
        System.out.println(getRes("multiply 8*17"));
        System.out.println(getRes("divide 200/25"));

    }

    public static int getRes(String sentence) {
        String[] array = sentence.split(SYMBOL);
        int result = 0;
        // switch condition for first position
        switch (array[0]) {
            // fields in class should not have text in code
            case OPERATION_ADDING:
                String[] arrayAdd = array[1].split(ADDING_SYMBOL);
                for (String addons : arrayAdd) {
                    //parsing from text to num value
                    result += Integer.parseInt(addons);
                }
                return result;
            case OPERATION_SUBTRACT:
                String[] arrayElement = array[1].split(SUBTRACT_SYMBOL);
                result = Integer.parseInt(arrayElement[0]);
                for (int i = 1; i < arrayElement.length; i++) {
                    result -= Integer.parseInt(arrayElement[i]);
                }
                break;
            case OPERATION_MULTPLY:
                String[] arrayForMultiply = array[1].split(MULTIPLY_SYMBOL);
                result = Integer.parseInt(arrayForMultiply[0]);
                for (int i = 1; i < arrayForMultiply.length; i++) {
                    result *= Integer.parseInt(arrayForMultiply[i]);
                }
                break;
            case OPERATION_DIVIDE:
                String [] arrayDivide = array[1].split(DIVIDE_SYMBOL);
                result = Integer.parseInt(arrayDivide[0]);
                for (int i = 1; i < arrayDivide.length; i++) {
                    result /= Integer.parseInt(arrayDivide[i]);
                }
                break;
            default:
        }
        return result;
    }
}
