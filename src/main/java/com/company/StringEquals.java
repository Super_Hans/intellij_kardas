package com.company;

public class StringEquals {
    public static void main(String[] args) {
        String x = "dog";
        String y = "dog";
        System.out.println(x==y);

        String z = new String ("dog");
        System.out.println(x==z);
        // we should use .equals every time when object typ String occurs
        System.out.println(x.equals(z));
        // String pull
        System.out.println(x.hashCode());
        System.out.println(y.hashCode());
        System.out.println(z.hashCode());

        String a = "d";
        System.out.println(a.hashCode());

        //that's why we collect passwords as char's array, not string;
        a+="og";
        System.out.println(a.hashCode());
    }
}
