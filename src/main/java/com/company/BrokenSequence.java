package com.company;

import java.util.Arrays;

public class BrokenSequence {
    public static void main(String[] args) {
        String seq = "1 2 3 b";
        System.out.println(findMissingNumber(seq));
    }

    private static int findMissingNumber(String sequence) {
        String[] array = sequence.split(" ");
        Arrays.sort(array);
        int missing = 0;
        for (int index = 0; index < array.length; index++) {
            if (sequence.charAt(index) >= 'a') {
                return 1;
            }
            break;
        }
        return ((array.length) * (array.length + 1)) / 2;
    }
}
