package com.company.generic_types;

import com.company.generic_types.interf.FruitInterface;
import com.company.generic_types.interf.FruitInterfaceBox;
import com.company.generic_types.methods.ReturnMethods;
import com.company.generic_types.methods.VoidMethods;
import com.company.generic_types.shape.*;
import com.company.generic_types.simple_generics.BiggerFruitBox;
import com.company.generic_types.simple_generics.FruitBox;
import com.company.generic_types.without_generic.Apple;
import com.company.generic_types.without_generic.AppleBox;
import com.company.generic_types.without_generic.Orange;
import com.company.generic_types.without_generic.OrangeBox;
import com.company.generic_types.without_generic.object.ObjectFruit;

public class Zad170 {
    public static void main(String[] args) {
        /**dedidacted box class (OrangeBox, AppleBox) for every class inside (Orange, Apple)
         *duplications of solutions
         */
        method1();

        /** Class boxing accepting objects of class Object
         * which provides acceptation of EVERY object of class
         * that inherits after class Object.
         *
         * So in boxing class we can put absolutely Everything
         */
        //method2();


        /** Boxing class accepts only objects of only those classes
         * that implements given interface - they have common
         */
        //method3();

        /** Class boxing is generic class so we have to set type of element
         * present inside class
         */
        //      method4();

        /**
         *Boxing class is generic class - it accepts two Objects of class
         * that we should indicate, i.e: BiggerFruitBox<String, Integer>
         */
        //method5();

        /**
         *Checking if generic class can take only those objects
         * which were indicated during declaration of new object
         *
         * so i.e.:
         * List<Integer> a = new ArrayList<>();
         * can add only objects of Integer class and all extends after Integer class
         * Cannot add objects from any other upper classes
         */
        //method6();

        /**
         * Methods that take generic types as parameters
         */
        //method7();

        /**
         * Methods that has generic type in signatures
         */
        method8();
    }

    private static void method8() {
        String name = "Kasia";

        ReturnMethods.method5(name);
        ReturnMethods.method5(4);
        ReturnMethods.method5(false);

        String naming = ReturnMethods.method6("Bazyl");
        int number = ReturnMethods.method6(4);

        ReturnMethods.method7(new Circle());

        ReturnMethods.method8("Dupsko", 4);
        ReturnMethods.method8(true, 0.7);

        boolean result = ReturnMethods.method9(new Square(), new Orange());

    }

    private static void method7() {
        ShapeBox<Circle> circleBox = new ShapeBox<>(new Circle());
        ShapeBox<Square> squareBox = new ShapeBox<>(new Square());
        ShapeBox<Rectangle> rectangleBox = new ShapeBox<>(new Rectangle());

        //ShapeBox<Shape> shapeBox = new ShapeBox<>(new Rectangle());
        /**accepts only specified box*/
        VoidMethods.method1(circleBox);
        //VoidMethods.method1(squareBox);
        //VoidMethods.method1(rectangleBox);
        /**accepts everything inside whats inside shapeBox */
        VoidMethods.method2(rectangleBox);
        VoidMethods.method2(squareBox);
        VoidMethods.method2(circleBox);
        /** accepts class which extends Rectangle*/
        VoidMethods.method3(squareBox);
        VoidMethods.method3(rectangleBox);
        //VoidMethods.method3(circleBox); //INVALID!

        /**accepts only objects from class and upper class only!*/
        //VoidMethods.method4(squareBox);//INVALID (too specific)
        //VoidMethods.method4(circleBox);//INVALID (not object of class and/or upper class
        VoidMethods.method4(rectangleBox);


    }

    private static void method6() {
        //new object created
        Rectangle rectangle = new Rectangle();

        //boxing class created - type must be specified
        ShapeBox<Rectangle> box = new ShapeBox<>(rectangle);

        //calling method on element inside boxing class
        System.out.println(box.getNameOfShape());

        Circle circle = new Circle();
        ShapeBox<Circle> box2 = new ShapeBox<>(circle);

        //operation's fine - more specified element with specifications of parent
        ShapeBox<Rectangle> box3 = new ShapeBox<>(new Rectangle());
        ShapeBox<Rectangle> box4 = new ShapeBox<>(new Square());
        // operation invalid - certain object should be more specific
        //ShapeBox<Square> box5 = new ShapeBox<>(new Rectangle());
    }

    private static void method5() {
        String name = "Kasia";
        Integer age = 27;

        //creating object of genric boxing class
        // elements can be ONLY instances of class
        //i.e. not int, but Integer OK (cannot be primitive type)
        BiggerFruitBox<String, Integer> box
                = new BiggerFruitBox<>(name, age);

        //printing element from inside
        System.out.println(box.getFirst());
        System.out.println(box.getSecond());

        BiggerFruitBox<String, String> box2
                = new BiggerFruitBox<>("Dupa", "Sołtysa");

        //printing element from inside
        System.out.print(box2.getFirst());
        System.out.println(box2.getSecond());
    }

    private static void method4() {
        //create ne object
        Apple apple = new Apple();
        // creating generic of class boxing
        FruitBox<Apple> box = new FruitBox<>(apple);
        // getting element from boxing object
        Apple fruit = box.getFruit();

//        Orange orange = new Orange();
//        box.setFruit(orange); // INVALID OPERATION

        Orange orange = new Orange();
        FruitBox<Orange> box2 = new FruitBox<>(orange);

        Boolean ifWinter = true;
        FruitBox<Boolean> box3 = new FruitBox<>(ifWinter);

    }

    private static void method3() {
        // new object
        Apple apple = new Apple();

        // object of boxing class
        FruitInterfaceBox box = new FruitInterfaceBox(apple);

        // display of name of the class
        System.out.println(box.getFruit().getClass().getSimpleName());

        // getting element from inside
        FruitInterface fruit = box.getFruit();
        // casting get object on class Apple
        Apple appleFruit = (Apple) fruit;

        // change of element for object of class Orange
        Orange orange = new Orange();
        box.setFruit(orange);
        System.out.println(box.getFruit().getClass().getSimpleName());

        //object of class Object is not accepted cause it is not implement
        // interface
        // box.setFruit(new Object());

    }

    private static void method2() {
        Apple apple = new Apple();
        ObjectFruit objectFruit = new ObjectFruit(apple);
        //check if object is instance of given class
        System.out.println(objectFruit.getFruit() instanceof Apple);


        //create new Object
        Orange orange = new Orange();
        //changing element inside
        objectFruit.setFruit(orange);
        //check if instance is correct
        System.out.println(objectFruit.getFruit() instanceof Orange);


        //get name of class from another another element inside class
        Object fruit = objectFruit.getFruit();
        String className = fruit.getClass().getSimpleName();
        System.out.println(className);

        // Casting so get object is classified as Orange
        Orange orange1 = (Orange) objectFruit.getFruit();
        // String orange1 = (String) objectFruit.getFruit(); - throws exception

        String info = "Kapitan Bomba";
        // Change of element inside box
        objectFruit.setFruit(info);
        // getting element from box
        Object fruit2 = objectFruit.getFruit();
        // getting class name from inside box
        String className2 = fruit.getClass().getSimpleName();
        System.out.println(className2);

        String info2 = (String) objectFruit.getFruit();
        System.out.println(info2);


    }

    private static void method1() {
        Apple apple = new Apple();
        AppleBox box = new AppleBox(apple);
        box.getApple();

        Orange orange = new Orange();
        OrangeBox box2 = new OrangeBox(orange);
    }
}
