package com.company;
//*ZADANIE #108*
//Porównaj czasy łączenia (np. 10 000) napisów przy wykorzystaniu
//- `+=`  (zwykłej konkatenacji)
//- `StringBuilder`-a
//- `StringBuffer`-a
public class Zad108 {
    private final static int NUM_OF_EXEC = 50_000;
    private final static String TEST_STRING = "dog";
    public static void main(String[] args) {
        long timeStart = System.currentTimeMillis();
        conactenate();
        long timeEnd = System.currentTimeMillis();
        System.out.println("Concatenation time: " + (timeEnd - timeStart));

        timeStart = System.currentTimeMillis();
        concatStringBuild();
        timeEnd = System.currentTimeMillis();
        System.out.println("Concatenation SB time: " + (timeEnd - timeStart));

        timeStart = System.currentTimeMillis();
        concatStrBuff();
        timeEnd = System.currentTimeMillis();
        System.out.println("Concatenation SBuff time: " + (timeEnd - timeStart));
    }
    private static String conactenate() {
        String phrase = ";";

        for (int i = 0; i <NUM_OF_EXEC ; i++) {
            phrase += TEST_STRING;
        }
        return phrase;
    }
    private static String concatStringBuild() {
        // does not concatenate Strings - only at the end of operation
        StringBuilder phrase = new StringBuilder();
        for (int i = 0; i < NUM_OF_EXEC; i++) {
            phrase.append(TEST_STRING);

        }
        return phrase.toString();
    }
    private static String concatStrBuff() {
        StringBuffer phrase = new StringBuffer();
        for (int i = 0; i < NUM_OF_EXEC; i++) {
            phrase.append(TEST_STRING);

        }
        return phrase.toString();
    }
}
