package com.company;
//**ZADANIE #55*
//Utwórz metodę, która przyjmuje dwie tablice.
// Metoda ma zwrócić nową tablicę w której elementy będą wstawione na zmianę.
//>Dla [5, 1, 10, 1, 89, -9]`
//>oraz [-2, 2, 7]`
//>Ma zwrócić [5, -2, 1, 2, 10, 7, 1, 89, -9]

import java.util.Arrays;

public class Zad55 {
    public static void main(String[] args) {
        int[] tablica1 = new int[]{1, 1, 1, 1, 1, 11, 1, 7, 1, 19, 1, 22};
        int[] tablica2 = new int[]{2, 2, 2, 2, 2, 2, 2};
        System.out.println(Arrays.toString(scalTablice(tablica1, tablica2)));
    }

    public static int[] scalTablice(int[] tablica1, int[] tablica2) {
        int[] tablicaKoncowa = new int[tablica1.length + tablica2.length];
        int[] dluzszaTablica = tablica1.length > tablica2.length ? tablica1 : tablica2;
        int[] krotszaTablica = tablica1.length < tablica2.length ? tablica1 : tablica2;
        int indexDluzszejTablicy = krotszaTablica.length;

        for (int i = 0; i < krotszaTablica.length; i++) {
            tablicaKoncowa[i * 2] = tablica1[i];
        }
        for (int j = 0; j < krotszaTablica.length; j++) {
            tablicaKoncowa[j * 2 + 1] = tablica2[j];
        }
        for (int i = krotszaTablica.length * 2; i < tablicaKoncowa.length; i++) {
            tablicaKoncowa[i] = dluzszaTablica[indexDluzszejTablicy];
            indexDluzszejTablicy++;
        }
        return tablicaKoncowa;
    }
}

