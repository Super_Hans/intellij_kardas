package com.company;
//**ZADANIE #47*
//Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz wartość logiczną.
// Gdy drugi parametr ma wartość `true` metoda ma zwrócić *największa* liczbę z tablicy.
// Gdy parametr będzie miał wartość `false`, metoda ma zwrócić *najmniejszą* liczbę z tablicy.
public class Zad47Array {
    public static void main(String[] args) {
        int [] tablica = new int [] {4,2,5,4,5};

        System.out.println(zwrocMaxLubMin(tablica, true));
        System.out.println(zwrocMaxLubMin(tablica, false));
    }

    public static int zwrocMaxLubMin (int [] tablica, boolean czyMAX) {
        int min = tablica[0];
        int max = tablica[0];
        for (int i = 1; i < tablica.length ; i++) {
            if (tablica[i] > max) {
                max = tablica[i];
            }
            if (tablica[i] < min) {
                min = tablica[i];
            }
        }
        return czyMAX ? max : min; //zmienna ? ternary operator
    }
}
