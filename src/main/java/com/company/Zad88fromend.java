package com.company;

//*ZADANIE #88*
//Utwórz metodę, która przyjmuje ciąg znaków, a odwraca każdy z wyrazów i zwraca całość jako jeden napis.
//> DLa `"ala ma kaca"` zwróci `"ala am acak"`
//> DLa `"dzisiaj jest sobota"` zwróci `"jaisizd tsej atobos"`
public class Zad88fromend {
    public static void main(String[] args) {
        System.out.println("->" + turnWordsInSentence("Ala ma kaca") + "<-");
        System.out.println(turnWordsInSentence("Kobyła ma mały bok"));
        System.out.println(turnWords("Kac"));

    }

    private static String turnWordsInSentence(String sentence) {
        //initialization and declaration of variable
        StringBuilder newSentence = new StringBuilder();
        //evocation of method split
        String[] words = sentence.split(" ");
        //you should not write comments next to code line, but above
        for (String word : words) {
            //we concatenate strings - we ADD it on the end of sentence
            newSentence.append(turnWords(word)).append(" ");

        }
        //method trim replaces
        return newSentence.toString().trim();
    }

    private static String turnWords(String word) {
        // initialize and declare new var
        String newWord = "";
        for (int letterPos = 0; letterPos < word.length(); letterPos++) {
            //concatenation of strings - conc. ONLY for text; it adds from start
            newWord = word.charAt(letterPos) + newWord;

        }
        return newWord;
    }
}
