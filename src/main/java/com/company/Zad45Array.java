package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Zad45Array {
    public static void main(String[] args) {
        podajRozmiarTablicy();
    }

    static void podajRozmiarTablicy() {
        System.out.println("Podaj rozmiar tablicy: ");
        Scanner scan = new Scanner(System.in);
        int suma = 0;
        int rozmiarTablicy = scan.nextInt();
        int[] tablica = new int[rozmiarTablicy];

        for (int i = 0; i < rozmiarTablicy; i++) {
            System.out.print("Liczba: " + (i + 1) + " ");
            tablica [i]= scan.nextInt();
            suma += tablica[i];

        }
        System.out.println("Podana tablica to " + Arrays.toString(tablica));
        System.out.println("Suma elementów = " + suma);
    }

}
