package com.company.generic_types.zadanie171;

public class GenericClass<X, Y, Z> {

    private X first;
    private Y second;
    private Z third;

    GenericClass(X first, Y second, Z third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    @Override
    public String toString() {
        return "GenericClass{" +
                "first=" + first +
                ", second=" + second +
                ", third=" + third +
                '}';
    }
}
