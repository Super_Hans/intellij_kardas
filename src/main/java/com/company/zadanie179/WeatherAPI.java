package com.company.zadanie179;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import java.util.List;

public interface WeatherAPI {

    //query of type get - adding form of URL
    @GET("location/search")
    // in query adding special text
    Call<List<CitySearch>> searchCities(@Query("query") String city);
    //query = san

    @GET("location/{city_id}")
    Call<City> getCity(@Path("city_id") String id);
}
