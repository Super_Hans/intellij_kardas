package com.company.wzorceProjektowe7;

class Zad165 {
    public static void main(String[] args) {

        DocFactory generator = new DocFactory();
        String text = "Jest niedziela,\nale nie pada śnieg";

        Doc txt = generator.createDoc(text, DocFactory.DocumentType.TXT);
        txt.safeFile("zadanko165");

        Doc html = generator.createDoc(text, DocFactory.DocumentType.HTML);
        html.safeFile("zadanie_165");
    }
}
