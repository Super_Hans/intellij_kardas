package com.company;
//*ZADANIE #83*
//Utwórz metodę, która przyjmuje wyraz,
// a następnie zwraca informacje czy zawiera on samogłoski czy nie.
public class Zad83 {
    public static void main(String[] args) {
        String text = "KAJAK";
        System.out.println(czyZawieraSamogloski(text));
        System.out.println(czyZawieraSamogloski2(text));

    }
    public static boolean czyZawieraSamogloski (String wyraz) { // inside static method main we can use only static methods
        String [] tablicaSamoglosek = new String [] {"a","e","u","y","i","o"};
        //check in array foreach loop;
        for (String literka : tablicaSamoglosek) {
            // if letter(changed to lower case) contains element from array = true
            if(wyraz.toLowerCase().contains(literka)) {
                return true;
            }
        }
        return false;
    }
    static boolean czyZawieraSamogloski2 (String word){
        String nowyWyraz = word.toLowerCase();
        return nowyWyraz.contains("a")
                ||nowyWyraz.contains("o")
                ||nowyWyraz.contains("y")
                ||nowyWyraz.contains("i")
                ||nowyWyraz.contains("u")
                ||nowyWyraz.contains("e");
    }
}
