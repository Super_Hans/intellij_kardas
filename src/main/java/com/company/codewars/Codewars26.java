package com.company.codewars;

public class Codewars26 {
    public static void main(String[] args) {
        System.out.println(isIsogram("mlnop"));
        System.out.println(isIsogram("Jezus"));
        System.out.println(isIsogram("contractor"));
        System.out.println(isIsogram(""));

    }

    public static boolean isIsogram(String str) {
        int wordLength = str.length();
        if (str.isEmpty()) return true;

        for (int i = 0; i < wordLength; i++) {
            for (int j = i+1; j < wordLength; j++) {
                if (str.toLowerCase().charAt(i) == str.toLowerCase().charAt(j)) {
                    return false;
                }
            }
        }
        return true;
    }
    //from best practice in Codewars
    public static boolean  isIsogram2(String str) {
        return str.toLowerCase().chars().distinct().count() == str.length();
    }
}
