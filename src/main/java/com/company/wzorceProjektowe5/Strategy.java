package com.company.wzorceProjektowe5;

public interface Strategy {
    double calculate(double number1, double number2);

    // default implementation of method (Java JDK 8+)
    default String nameOfOps(){
        return "new strategy: ";
    }
}
