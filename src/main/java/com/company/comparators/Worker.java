package com.company.comparators;

import org.jetbrains.annotations.NotNull;

public class Worker implements Comparable<Worker> {
    private String name;
    private String surname;
    private int age;
    private int salary;

    Worker (String name, String surname, int age, int salary) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public int getSalary() {
        return salary;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return String.format(" %-10s| %-10s | %-3s | %-5s",
                name,
                surname,
                age,
                salary);
    }

    @Override
    //we sort it by -1 (before returned object) or by 1 (after returned object)
    public int compareTo(@NotNull Worker w) {
//        if (getAge()==w.age){
//            //check if objects are same
//            return 0;
//        }else if (getAge()<w.age){
//            //current object is first
//            return 1;
//        }else {
//            //current parameter is first
//            return -1;
//        }
        //easier way below
   return Integer.compare(getAge(),w.age);

        //from oldest to youngest in case of age(from biggest to smallest integer)
        //return w.age - getAge();
    }
}
