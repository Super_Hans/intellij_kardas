package com.company.wzorceProjektowe2;

class Account {
    private double balanceAcc = 0;

    double getBalance() {
        return balanceAcc;
    }

    void putMoney(double money) {
        balanceAcc += money;
    }

    void withdrawnMoney(double money) {
        balanceAcc -= money;
    }
}
