package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zad103 {
    public static void main(String[] args) {
        Integer[] arrayyList = new Integer[]{1, 2, 3, 4, 5, 8, 99, -12, 7, 5, 13, 5, 78, 5};
        List<Integer> array = new ArrayList<>(Arrays.asList(arrayyList));
        System.out.println(getPositionList(array, 5));

    }

    private static List<Integer> getPositionList(List<Integer> list, int parameter) {
        List<Integer> resultList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals(parameter)) {
                resultList.add(i);
            }
        }
        return resultList;
    }
}
