package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//*ZADANIE #104*
//Utwórz metodę, która przyjmuje dwa parametry - listę liczb (np. `ArrayList`)
// oraz indeks za którym należy “przeciąć” tablicę i zwrócić drugą część.
//> Dla `3` oraz `<1, 7, 8, 22, 10, -2, 33>`
//> powinno zwrócić `<10, -2, 33>`
public class Zad104 {
    public static void main(String[] args) {
        List<Integer> array = new ArrayList<>(Arrays.asList(1,7,8,22,10,-2,33));
        System.out.println(getCutTable(array,3));

    }
    private static List<Integer> getCutTable (List<Integer> list, int cutIndex ) {
        List <Integer> result = new ArrayList<>();
        for (int i = cutIndex+1; i < list.size() ; i++) {
            int number = list.get(i);
        result.add(number);
        }
        return result;
    }
}
