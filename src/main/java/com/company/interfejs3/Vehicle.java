package com.company.interfejs3;

abstract class Vehicle implements VehicleSpecs {

    protected String name;
    protected int maxSpeed;
    protected int numberOfPassengers;

    public Vehicle(String name) {
        this.name = name;
    }

    @Override
    // final field cannot change values!
    public final int maxSpeed() {
        return maxSpeed;
    }

    @Override
    // check up!
    public final int numberOfPassengers() {
        return numberOfPassengers;
    }
    void showParameters(int maxSpeed, int numberOfPassengers) {
        this.maxSpeed = maxSpeed;
        this.numberOfPassengers = numberOfPassengers;

    }
}
