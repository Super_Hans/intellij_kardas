package com.company;
//*Utwórz metodę, która przyjmuje jeden parametr (który jest liczbą wierszy ) oraz wyświetla “choinkę” nie przerywając numerowania
//> Dla `4` wyświetli:
//>
//1
//2 3
//4 5 6
//7 8 9 10

public class Zad32 {
    public static void main(String[] args) {
        pokazeNowąChoinke(4);
    }
    public static void pokazeNowąChoinke (int lineNum) {
        int currentValue= 1;
        for (int i = 1; i <= lineNum ; i++) {
            for (int j = 1; j <= i ; j++) {
                System.out.print(currentValue++ + " ");
            }
            System.out.println();
        }
    }
}
