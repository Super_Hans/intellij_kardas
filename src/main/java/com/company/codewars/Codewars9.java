package com.company;

import java.util.Arrays;

public class Codewars9 {
    public static void main(String[] args) {
        int[] array = new int[]{1, 2, 6, 4, -5};
        int[] array2 = new int[]{1, -2, 61, 3, -5};
        System.out.println(Arrays.toString(invert(array)));
        System.out.println(Arrays.toString(invert2(array2)));
    }

    public static int[] invert(int[] array) {
        int[] invertedTable = new int[array.length];
        for (int i = 0; i < array.length; i++) {

            invertedTable[i] = array[i] * (-1);

        }
        return invertedTable;
    }

    public static int[] invert2(int[] array2) {

        for (int i = 0; i < array2.length; i++) {
            array2[i] *= -1;  //good practice - check operator!
        }

        return array2;
    }
}

