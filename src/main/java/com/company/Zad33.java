package com.company;
//*Utwórz metodę, która wyświetli prostokąt o podanych wymiarach
// (użytkownik podaje jego wymiary jako parametry)
//XXXXXXXXXX
//X        X
//X        X
//XXXXXXXXXX

public class Zad33 {
    public static String znak = "X";
    private static String przerwa = " ";


    public static void main(String[] args) {
        pokaKwadrat(10,4);


    }

    static void pokaKwadrat(int szer, int wys) {
        pokaWiersz(szer);

        for (int j = 0; j < wys - 2; j++) {
            System.out.print(znak);
            for (int x = 0; x < szer - 2 ; x++) {

                System.out.print(przerwa);

            }
            System.out.println(znak);


        }

        pokaWiersz(szer);

    }

    static void pokaWiersz(int wys) {
        for (int i = 1; i <= wys; i++) {
            System.out.print(znak);
        }
        System.out.println();
    }

}

