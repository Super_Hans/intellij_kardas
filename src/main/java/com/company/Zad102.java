package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//*ZADANIE #102*
//Utwórz metodę, która przyjmuje trzy parametry - listę liczb (np. `ArrayList`),
// początek zakresu (`int`) oraz koniec zakresu (`int`) a zwrócić liczbę.
//
//Metoda ma policzyć różnicę pomiędzy największym i najmniejszym elementem w tym zakresie.
//Policzona różnica jest indeksem elementu z tablicy wejściowej który należy zwrócić
// (jeśli wartość będzie poza zakresem indeksów metoda ma zwrócić `-1`)
public class Zad102 {
    public static void main(String[] args) {
        List<Integer> array = new ArrayList<>(Arrays.asList(
                1, 2, 4, 6, 7, 9, 0, 3, 4, 5
        ));
        //System.out.println(returnDiffBetweenElements(array, 2, 4));
        System.out.println("Szukana liczba: " + showIndexOfElements(array, 2, 6));

    }

    private static Integer showIndexOfElements(List<Integer> list, int start, int end) {
        Integer index = returnDiffBetweenElements(list, start, end);
        //check if position in array is less than 0 OR greater than list size
        if (index < 0 || index > list.size() - 1) {
            return -1;
        }

        return list.get(index);

    }

    private static Integer returnDiffBetweenElements(List<Integer> list, int start, int end) {
        Integer max = list.get(start);
        Integer min = list.get(start);

        for (int i = start; i <= end; i++) {
            if (list.get(i) > max) {
                max = list.get(i);
            }
            if (list.get(i) < min) {
                min = list.get(i);
            }

        }
        return max - min;
    }
}
