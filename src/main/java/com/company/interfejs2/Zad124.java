package com.company.interfejs2;

import static com.company.interfejs2.Person.Gender.MAN;
import static com.company.interfejs2.Person.Gender.WOMAN;

public class Zad124 {
    public static void main(String[] args) {
        Person person = new Person("Peter","Parker",37, MAN);
        Person person1 = new Person("Dorota","Siup",57, WOMAN);
        System.out.println(person.showDataFull());
        System.out.println(person.yearsToPension());

        System.out.println(person1.showDataFull());
        System.out.println(person1.yearsToPension());
        Worker worker = new Worker("Dick", "Gentle",33,MAN);
        worker.setSalary(3500.0);
        System.out.println(worker.showDataFull());

    }
}
