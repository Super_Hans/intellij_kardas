package com.company.interfejs3;

public interface VehicleSpecs {
    int maxSpeed();
    int numberOfPassengers();

}
