package com.company.optionals;

import java.util.Optional;

public class Car {
    private String name;
    private Integer age;

    public Optional<Integer> getAge() {
        return Optional.ofNullable(age);
    }

    public String getName() {
        return name;
    }
}
