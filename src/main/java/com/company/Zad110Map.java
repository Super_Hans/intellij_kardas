package com.company;

import java.util.HashMap;
import java.util.Map;

//*ZADANIE #110*
//Utwórz metodę, które zwróci mapę gdzie kluczem jest skrót kraju (np. “PL”, “DE”, “ES”)
// a wartościami pełna nazwa (np. `Polska`, `Niemcy`, `Hiszpania`).
//
//Utwórz drugą metodę, która zwróci mapę gdzie kluczem jest numer kierunkowy kraju (np. “48", “47”)
// a wartościami pełna nazwa (np. `Polska`, `Niemcy`, `Hiszpania`).
public class Zad110Map {
    public static void main(String[] args) {
        Map<String, String> map = getMapWithKey();
        System.out.println(map);
        Map<Integer,String> mapA = getCountyCode();
        System.out.println(mapA);
        System.out.println(getCountry(getCountyCode(),353));
    }

    //Map <key, value>
    //HashMap - implementation
    /** KEYs are unique; VALUE's can duplicate*/
    private static Map<String, String> getMapWithKey() {

        Map<String, String> map = new HashMap<>();
        map.put("PL", "Poland");
        map.put("ES", "Spain");
        map.put("DE", "Germany");
        map.put("IE", "Ireland");

        return map;
    }
    private static Map <Integer, String> getCountyCode() {
        Map <Integer,String> map = new HashMap<>();
                map.put(48, "Poland");
                map.put(44, "United Kingdom");
                map.put(353, "Ireland");

                return map;
    }
    private static String getCountry(Map<Integer, String> map, int countryCode) {
        return map.get(countryCode);

    }
}
