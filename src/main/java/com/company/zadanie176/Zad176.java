package com.company.zadanie176;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Zad176 {
    public static void main(String[] args) {
        method1();
    }

    private static void method1() {
        ExecutorService es1 = Executors.newFixedThreadPool(4);
        System.out.println("Thread: " + Thread.currentThread().getId());
        for (int i = 0; i < 10; i++) {
            Runnable runnable = getRunnable();
            es1.submit(runnable);

        }
        es1.shutdown();
    }

    @NotNull
    private static Runnable getRunnable() {
        return () -> {
            System.out.println("Task complied by: " +
                    Thread.currentThread().getId());

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        };
    }
}
