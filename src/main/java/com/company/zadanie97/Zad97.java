package com.company.zadanie97;

/**ZADANIE #97*
 Utwórz typ wyliczeniowy `Plec` (zawierająca dwie opcje). Utwórz klasę `Osoba`, gdzie jednym z pól będzie ten typ.
 Utwórz tablicę obiektów klasy `Osoba` oraz metodę która zwróci nam liczbę pracowników wybranej (przez parametr) płci.*/
public class Zad97 {
    public static void main(String[] args) {
        Person[] persons = new Person[]{
                new Person("Tomas", "Nowak", 19, Gender.MAN),
                new Person("Violetta", "Kwas", 77, Gender.WOMAN),
                new Person("Barbara", "Męczydło", 103, Gender.WOMAN),

        };
        for (Person person : persons) {
            System.out.println(person);
        }
    }
}
