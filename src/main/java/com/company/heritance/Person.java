package com.company.heritance;

public class Person {
    private String name;
    private String surname;
    private int age;

    Person(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }
    // method evoked here is method toString
    String getNameAndSurnameOfAPerson (){
        return String.format("This is %s %s (yrs. %s)",name, surname, age);
    }

    @Override
    // we override method be method toString
    public String toString() {
        //we insert method
        return getNameAndSurnameOfAPerson();
    }
}
