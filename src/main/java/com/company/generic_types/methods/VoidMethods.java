package com.company.generic_types.methods;

import com.company.generic_types.shape.Circle;
import com.company.generic_types.shape.Rectangle;
import com.company.generic_types.shape.ShapeBox;

public class VoidMethods {

    //demand for object of Circle shape box inside
    public static void method1(ShapeBox<Circle> circleShapeBox) {
        System.out.println(circleShapeBox.getNameOfShape());
    }

    // shapebox with something inside - accepts everything
    //verified by Shape Box itself
    public static void method2(ShapeBox<?> box) {
        System.out.println(box.getNameOfShape());

    }

    public static void method3(ShapeBox<? extends Rectangle> box) {
        System.out.println(box.getNameOfShape());
    }

    // accepts objects of class Rectangle and ALL upper classes
    public static void method4(ShapeBox<? super Rectangle> box) {
        System.out.println(box.getNameOfShape());
    }
}
