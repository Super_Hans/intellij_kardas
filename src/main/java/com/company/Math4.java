package com.company;

public class Math4 {
    public static void main(String[] args) {
        int a = 3;
        int b = a++;
        int c = ++a;
        int d = 0;
        //a = 3-->4-->5   b=3-->3 c -->5
        d = --b + ++c * --d - ++a / --d % ++c; //d=2 + 6 * (-1) - 6 / (-2) % 7 = 2+(-6) + 3%7 = -4 +3 = -1
        System.out.println(d);
    }
}
