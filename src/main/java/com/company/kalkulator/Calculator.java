package com.company.kalkulator;

public class Calculator {

    public Integer summation(Integer num1, Integer num2) {
        if (num1 == null) {
            return num2;
        }
        if (num2 == null) {
            return num1;
        }
        return num1 + num2;

    }

    public Integer sumOfManyNums(Integer... numbers) {
        if (numbers == null) {
            return null;
        }
        int sum = 0;
        for (Integer number : numbers) {
            sum += number;

        }
        return sum;
    }

    public Integer deducting(Integer num1, Integer num2) {

        if (num2 == null) {
            return num1;
        }
        if (num1 == null) {
            return -num2;
        }
        return num1 - num2;
    }

}
