package com.company.obiektowe;

//**ZADANIE #77*
//Utwórz klasę `Person` (`Osoba`) posiadającą
//*pola reprezentujące:*
//>* imie
//>* nazwisko
//>* wiek
//>* czyMezczyzna (lub czyKobieta)
//
//*konstruktory oraz metody służące do:*
//>* przedstawienia się (`Witaj, jestem Adam Nowak, mam 20 lat i jestem mężczyzną`)
//>* sprawdzenia czy osoba jest pełnoletnia
public class Osoba {
    private String imie;
    private String nazwisko;
    private int wiek;
    private Boolean czyMezczyzna;

    Osoba(String imie, String nazwisko, int wiek, boolean czyMezczyzna) {
        //konstruktory. kolejność nie gra roli, ale dobrą praktyką są pola na górze a pod spodem są konstruktory
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.wiek = wiek;
        this.czyMezczyzna = czyMezczyzna;
    }

    Osoba(String imie, String nazwisko) {
        this.imie = imie; //pola bez wartości dostają defaultem "null"
        this.nazwisko = nazwisko;

    }


    @Override
    public String toString() {
        if (wiek == 0 && czyMezczyzna == null) {
            return String.format("%s %s",
                    imie,
                    nazwisko);
        } else {
            return String.format("%s %s (l. %s ) - %s",
                    imie,
                    nazwisko,
                    wiek,
                    czyMezczyzna ? "mężczyzna " : "kobieta ");
        }
        // "imie='" + imie + '\'' +
        // ", nazwisko='" + nazwisko + '\'' +
        //  ", wiek=" + wiek +
        // ", czyMezczyzna=" + czyMezczyzna +
        //  '}';
    }

    //metoda
    boolean czyPelnoleni() {
        return wiek < 18 ? false : true;
        //return wiek>=18;
    }

    void przedstawSię() {
        System.out.printf("Witaj! Nazywam się %s %s mam %s lata i jestem %s\n",
                imie,
                nazwisko,
                wiek,
                czyMezczyzna ? "mężczyzną " : "kobietą ");
    }
}
