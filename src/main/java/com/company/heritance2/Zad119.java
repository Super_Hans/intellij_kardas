package com.company.heritance2;

import com.company.codewars.StudySubject;
import com.company.heritance2.Person.Sex;

import java.util.HashMap;

import static com.company.heritance2.Teacher.MasterDegree.DR;
import static com.company.heritance2.Teacher.MasterDegree.PROF;

public class Zad119 {
    public static void main(String[] args) {
        Person person = new Person("Ziuta",39, Sex.WOMAN);
        System.out.println(person);

        Teacher teacherMath = new Teacher("Witold",81,Sex.MAN, PROF);
        Teacher teacherPhys = new Teacher("Maryja",55,Sex.MAN, DR);
        University university = new University("WSLiB","Rojna 69");
        Student student = new Student("Mariusz",24,Sex.MAN,university,2,new HashMap<>());
        StudySubject mathematics = new StudySubject("Math", teacherMath);
        StudySubject physics = new StudySubject("Physics", teacherPhys);
        student.addDegree(physics,3);
        student.addDegree(mathematics,4);
        university.addStudent(student);
        university.showStudents();
    }
}
