package com.company.generic_types.without_generic.object;

public class ObjectFruit {
    private Object fruit;

    public void setFruit(Object fruit) {
        this.fruit = fruit;
    }

    public Object getFruit() {
        return fruit;
    }

    public ObjectFruit(Object fruit) {
        this.fruit = fruit;
    }
}
