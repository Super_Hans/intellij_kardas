package com.company;
//*ZADANIE #85*
//Utwórz metodę, która przyjmuje imię i nazwisko a następnie zwraca w formie napisu login, który składa się z:
//>- 3 liter imienia
//>- 3 liter nazwiska
//>- liczby, która jest sumą długości imienia i nazwiska
//
//> Dla `"Maciej Czapla"`, zwróci `"maccza12"`
//> Dla `"Anna Nowak"`, zwróci `"annnow9"`
//> Dla `"Jan Kowalski"`, zwróci `"jankow11"`
public class Zad85 {
    public static void main(String[] args) {
        System.out.println(zwrocLogin("Przemek Kardas"));
        System.out.println(zwrocLogin("Bigus Dickus"));

    }
    private static String zwrocLogin (String name) {
        String [] array = name.split(" "); //split after every space and returns string array
        return (array[0].substring(0,3)             // concatenation of first 3 letters from array on first and second pos
                + array[1].substring(0,3)
                + (array[0] + array[1]).length()).toLowerCase();
    }
}
