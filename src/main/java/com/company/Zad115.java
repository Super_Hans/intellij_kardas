package com.company;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

//Utwórz metodę, która przyjmuje jeden parametr - maksymalną liczbę.
// Metoda powinna zwrócić mapę, gdzie kluczami będą kolejne liczby od `1`
// do podanej liczby (jako parametr). Wartościami tej mapy będzie informacja,
// przez jakie liczby jest ona podzielna.
//>
//```10 -> 1, 2, 5, 10
//11 -> 1, 11
//12 -> 1, 2, 3, 4, 6, 12
//13 -> 1, 13
//14 -> 1, 2, 7, 14
//...```
public class Zad115 {
    public static void main(String[] args) {
        niceShow(getMapWithDivisors(8));
    }

    private static Map<Integer, Set<Integer>> getMapWithDivisors(int max) {
        // set of Integers
        Map<Integer, Set<Integer>> map = new HashMap<>();
        // loop where we integrate set of divisors
        for (int i = 1; i <= max; i++) {
            Set<Integer> integerSet = setOfDivisors(i);
            map.put(i, integerSet);
        }

        return map;
    }

    private static Set<Integer> setOfDivisors(int numberToCheck) {
        Set<Integer> setDiv = new HashSet<>();
        for (int i = 1; i <= numberToCheck; i++) {
            if (numberToCheck % i == 0) {
                setDiv.add(i);
            }
        }
        return setDiv;
    }

    private static void niceShow(Map<Integer, Set<Integer>> map) {
        //checking values from map
        for (Map.Entry<Integer, Set<Integer>> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " -> " + entry.getValue());

        }
    }
}
