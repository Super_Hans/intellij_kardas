package com.company;

//Policz sumę cyfr przekazanej liczby przy wykorzystaniu *rekurencji*.
public class Zad141 {
    public static void main(String[] args) {
        System.out.println(sumOfNum(5000));

    }

    private static int sumOfNum(int number) {
        System.out.println("number = [" + number + "]");
        if (number == 0) {
            return 0;
        } else {
            return (number % 10) + sumOfNum(number / 10);
        }
    }
}
