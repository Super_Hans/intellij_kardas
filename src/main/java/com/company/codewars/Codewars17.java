package com.company;

import java.util.Arrays;

//Write a function that returns both the minimum and maximum number of the given list/array.
//Examples
//
//MinMax.minMax(new int[]{1,2,3,4,5}) == {1,5}
//MinMax.minMax(new int[]{2334454,5}) == {5, 2334454}
//MinMax.minMax(new int[]{1}) == {1, 1}
public class Codewars17 {
    public static void main(String[] args) {
        int[] array = new int[]{13, 2, 91, 4, -24};
        System.out.println(Arrays.toString(minMax(array)));
        System.out.println(Arrays.toString(miniMaxi(array)));
    }

    public static int[] minMax(int[] arr) {

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }if (arr[i] < min) {
                min = arr[i];
            }
        }
        return new int [] {min,max};
    }
    public static int [] miniMaxi (int[]arr) {
        Arrays.sort(arr);
        return new int[]{arr[0],arr[arr.length-1]};
    }
}