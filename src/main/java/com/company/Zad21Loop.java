package com.company;

public class Zad21Loop {
    public static void main(String[] args) {
        showMinor(5);
    }

    static void showMinor(int minor) {
        for (int i = minor; i >= 0; i--) {
            System.out.print(i + ", ");
        }
    }
}
