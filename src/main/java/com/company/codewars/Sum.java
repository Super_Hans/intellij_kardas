package com.company.codewars;

public class Sum {
    public static void main(String[] args) {
        System.out.println(getSum(-1, 3));
        System.out.println(getSum2(-1, 2));
        System.out.println(getSum3(-1, 3));
    }

    static int getSum(int a, int b) {
        int sum = 0;

        if (a == b) {
            return a;
        } else if (a < b) {
            for (int i = a; i <= b; i++) {
                sum += i;
            }
        } else if (a > b) {
            for (int i = b; i >= a; i++) {
                sum += i;
            }

        }
        return sum;
    }

    static int getSum2(int a, int b) {

        return (a + b) * (Math.abs(a - b) + 1) / 2;
    }

    static int getSum3(int a, int b) {
        int res = 0;
        for (int i = Math.min(a, b); i <= Math.max(a, b); i++) {
            res += i;
        }
        return a == b ? a : res;
    }
}
