package com.company;

import java.util.ArrayList;
import java.util.List;

//*ZADANIE #98*
//Utwórz metodę, która przyjmuje listę liczb, a następnie zwraca ich sumę.
public class Zad98 {
    public static void main(String[] args) {
        List<Integer> myList = new ArrayList<>();
        myList.add(12);
        myList.add(99);
        myList.add(69);
        myList.add(2);
        myList.add(13);
        myList.add(10);
        myList.add(3);
        myList.add(15);

        System.out.println(zwrocSumeZListy(myList));
        System.out.println(zwrocSumeZListy2(myList));
    }

    static int zwrocSumeZListy(List<Integer> list) {
        int sum = 0;
        for (Integer number : list) {
            sum += number;

        }
        return sum;
    }

    static int zwrocSumeZListy2(List<Integer> list) {
        int sum = 0;
        for (int pozycja = 0; pozycja < list.size(); pozycja++) {
            sum += list.get(pozycja);
        }
        return sum;
    }

}
