package com.company.zadanie151;

//Utwórz klasę `Person` (`Osoba`) posiadającą
//>*pola reprezentujące:*
//>* imie
//>* nazwisko
//>* wiek
//>* czyMezczyzna
//
//>*konstruktory*
//
//>*metody służące do:*
//> - zwrócenia imienia i nazwiska (tylko jeśli są ustawione!!)
//> - przedstawienia się (czyli zwraca `Witaj, jestem Adam Nowak, mam 20 lat i jestem mężczyzną`)
//> - sprawdzenia czy osoba jest pełnoletnia
//> - zmiany wieku
//> - zmiany imienia
//> - zwrócenia wieku do emerytury
public class Persona {

    private String name;
    private String surname;
    private int age;
    private boolean isMan;


    public Persona() {
    }

    public Persona(String name, String surname, int age, boolean isMan) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.isMan = isMan;

    }

    public boolean isAdult() {
        return age > 17;
    }

    public String getFullData() {
        if (surname == null && name == null) {
            return null;
        } else if (surname == null) {
            return name;
        } else if (name == null) {
            return surname;
        } else {
            return name + " " + surname;
        }
    }

    public int howManyYearsToPension() {
        if (age == 0) {
            return -1;
        }
        int calcAgeToPension = (isMan ? 67 : 65) - age;
        return calcAgeToPension > 0 ? calcAgeToPension : 0;
    }


    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public boolean isMan() {
        return isMan;
    }

    public void setName(String name) {
        if (name.matches("^[a-zA-Z]+$")) {
            this.name = name;
        }
    }

    public void setSurname(String surname) {
        if (surname.matches("^[a-zA-Z]+$")) {
            this.surname = surname;
        }
    }

    public void setAge(int age) {
        if (age > 0) {
            this.age = age;
        }
    }

    public void setMan(boolean man) {
        isMan = man;
    }

    public String setLogin() {
        if (name == null && surname == null) {
            return null;
        }

        String firstPart;
        String secPart;
        if (name != null && name.length() > 2) {
            firstPart = name.substring(0, 3).toLowerCase();
        } else if (name != null && name.length() < 3) {
            firstPart = name.toLowerCase() + "Z";
        } else {
            firstPart = "XXX";
        }

        if (surname != null && surname.length() > 2) {
            secPart = surname.substring(0, 3).toLowerCase();
        } else if (surname.length() < 3 && surname != null) {
            secPart = surname.toLowerCase() + "Z";
        } else {
            secPart = "YYY";
        }

        int nameLength;
        int surnameLength;
        if (name == null) {
            nameLength = 0;
        } else {
            nameLength = name.length();

        }
        if (surname == null) {
            surnameLength = 0;
        } else {
            surnameLength = surname.length();

        }

        int endIndexLogin = nameLength + surnameLength;

        return String.format("%s%s%s", firstPart, secPart, String.valueOf(endIndexLogin));

    }
}
