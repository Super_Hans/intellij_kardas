package com.company;

import java.util.Arrays;

//Utwórz metodę, która przyjmuje ciąg znaków
// zwraca informacje jak długie są ciągi znaków które stoją obok siebie
//> DLa `"aaabccccbb"` zwróci `[3, 1, 4, 2]`
//> DLa `"telefon"` zwróci `[1, 1, 1, 1, 1, 1, 1]`
//> DLa `"anna"` zwróci `[1, 2, 1]`
public class Zad93 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(retArrayWithSeries("aaabbbccccccccbb")));
        System.out.println(Arrays.toString(retArrayWithSeries("telephone")));


    }

    private static int[] retArrayWithSeries(String series) {
        String[] array = series.split("");
        String sameElement = array[0];
        int howManyTimes = 0;
        int[] newArray = new int[array.length];
        int positionNewArray = 0;

        for (String elements : array) {
            if (elements.equals(sameElement)) {
                howManyTimes++;
            } else {
                sameElement = elements;
                // use system outs to check if your code is doing exactly what you need
                newArray[positionNewArray] = howManyTimes;
                positionNewArray++;
                // zeroing number of how many times element appears
                howManyTimes = 1;

            }
        }
        newArray[positionNewArray] = howManyTimes;

        return cutArray(newArray);
    }

    private static int[] cutArray(int[] array) {
        int i = 0;
        for (; i < array.length; i++) {

            if (array[i] == 0) {
                break;
            }
        }
        int[] shorterarray = new int[i];
        for (int j = 0; j < shorterarray.length; j++) {
            shorterarray[j] = array[j];

        }
        return shorterarray;
    }
}
