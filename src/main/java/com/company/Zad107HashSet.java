package com.company;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

//Utwórz metodę, która przyjmuje dwa parametry - set (np. `HashSet`)
// oraz poszukiwaną liczbę (jako `double`.)
//
//Metoda ma zwrócić set elementów większych od podanego (drugiego) parametru.
public class Zad107HashSet {
    public static void main(String[] args) {
        Set<Integer> integerSet = new HashSet<>(Arrays.asList(22, -1, 2, 4, 6, 8, 12));
        System.out.println(getSetOfHigherElements(integerSet, 2.0));
        System.out.println(getSetOfHigherElements(integerSet, 5.0));

    }

    private static Set<Integer> getSetOfHigherElements(Set<Integer> firstSet, double number) {
        Set<Integer> result = new HashSet<>();
        for (Integer integer : firstSet) {
            if (integer > number) {
                result.add(integer);
            }
        }
        return result;
    }
}
