package com.company;

public class Codewars18 {
    public static void main(String[] args) {
        System.out.println(arithmetic(345,876,"add"));
        System.out.println(arithmetic(1345,876,"subtract"));
        System.out.println(arithmetic(145,6,"multiply"));
        System.out.println(arithmetic(245,5,"divide"));
    }

    public static int arithmetic(int a, int b, String operator) {
        switch (operator) {
            case "add":
                return a + b;
            case "subtract":
                return a - b;
            case "multiply":
                return a * b;
            case "divide":
                return a / b;
            default:
                return 0; //   throw new IllegalArgumentException("Invalid argument: " + operator);
        }
    }
}
