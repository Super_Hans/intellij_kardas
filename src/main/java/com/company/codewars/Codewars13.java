package com.company;

public class Codewars13 {
    public static void main(String[] args) {

        System.out.println(gagesSum(0, 2));
    }

    public static int gagesSum(int a, int b) {

        return (a + b) * (Math.abs(a - b) + 1) / 2;
    }

}

//Good luck!




