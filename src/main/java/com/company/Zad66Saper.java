package com.company;
//*Utwórz metodę, która wylosuję planszę do gry Saper z zaznaczonymi bombami
// (wartość `true` oznacza bombe, wartość `false` oznacza puste pole).
//
//Na potrzeby wizualizacji utwórz dodatkową metodę która będzie wyświetlać planszę gdzie bomby będą zaznaczone jako `*`, a puste pola np. `_`
//
//Kolejna metoda ma zwrócić wypełnioną planszę z numerami (gdzie `-1` oznacza bombę).
//
//>*Pierwsza metoda zwróci:*
//>
//```true false true
//false false true
//false false false```
//
//>*Druga metoda wyświetli:*
//>
// ```* _ *
// _ _ *
// _ _ _```
//
//>*Trzecia metoda zwróci:*
//>
// ```* 3 *
// 1 3 *
// 0 1 1```

import java.util.Random;

public class Zad66Saper {
    public static void main(String[] args) {
        boolean[][] planszaPodstawowa = wylosujPlansza1(5);
       // pokażTrue(planszaPodstawowa);

        int[][] cokolwiek = wypełnijLiczbami(planszaPodstawowa);
        wyswietlLiczbami(cokolwiek);
    }

    public static boolean[][] wylosujPlansza1(int wymiar) {
        boolean[][] plansza1 = new boolean[wymiar][wymiar]; //tablice boolean są defaultowo wypełniane wartością FALSE
        Random random = new Random();
        for (int proba = 0; proba < wymiar * wymiar * 0.25; proba++) {
            int wiersz = random.nextInt(wymiar);
            int kolumna = random.nextInt(wymiar);
            if (plansza1[wiersz][kolumna]) {
                proba--;
                //     System.out.println("Błędne pole " + wiersz + " " + kolumna);
            } else {
                plansza1[wiersz][kolumna] = true;
                //     System.out.println("Wylosowano pole " + wiersz + " " + kolumna);

            }
        }
        return plansza1;
    }

    public static void pokażTrue(boolean[][] plansza) { //nie używana
        for (boolean[] wiersz : plansza) {
            for (boolean elementWiersza : wiersz) {
                // System.out.print(elementWiersza +" \t ");
                System.out.print(elementWiersza ? "X" : "-");
                System.out.print("\t");
            }
            System.out.println();

        }
    }

    static int[][] wypełnijLiczbami(boolean[][] tab) {
        int[][] nowaTablica = new int[tab.length][tab.length];
        //podajemy zakres sprawdzenia pól wokół pola(-1) bo zaczynamy wiersz i kolumne wcześniej
        for (int wiersz = 0; wiersz < tab.length; wiersz++) {
            for (int kolumna = 0; kolumna < tab[0].length; kolumna++) {
                if (tab[wiersz][kolumna]) {
                    nowaTablica[wiersz][kolumna] = -1;
                } else {
                    nowaTablica[wiersz][kolumna] = liczbaTrafien(tab, wiersz, kolumna);

                }
            }

        }
        return nowaTablica;
    }

    static int liczbaTrafien(boolean[][] tab, int wiersz, int kolumna) {
        int licznik = 0;
        for (int i = wiersz - 1; i <= wiersz + 1; i++) {
            for (int j = kolumna - 1; j <= kolumna + 1; j++) {
                //sprawdzamy czy pod danymi współrzędnymi jest bomba

                if (j >= 0 && i >= 0 && j < tab.length && i < tab.length) {
                    //sprawdzamy czy na danej pozycji jest bomba - if yes -zwiekszamy liczbe bomb+1
                    if (tab[i][j]) { //==true
                        licznik++;
                    }
                }
            }
        }
        return licznik;
    }

    static void wyswietlLiczbami(int [][] tablica) {

        for (int [] wiersz : tablica) {
            for (int pole : wiersz) {
                if (pole == -1) {
                    System.out.print("*\t");
                } else {
                    System.out.print(pole + "\t");
                }

            }

            System.out.println();
        }
    }
}
