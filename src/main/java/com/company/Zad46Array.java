package com.company;

//**ZADANIE 46*
//Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
// Metoda ma usunąć z podanej tablicy element o wybranym indeksie i zwrócić nową tablicę. Zaimplementuj dodatkowo metodę służącą do wyświetlania tablicy.
//> Dla `([1, 2, 3, 4, 5],  2)`
//> zwróci `[1, 2, 4, 5]`
public class Zad46Array {
    public static void main(String[] args) {

        int[] tablica = {1, 2, 3, 4, 5};
        wyswietlTablice(tablica);
        wyswietlTablice(usunElementZTablicy(tablica, 2));
    }

    static int[] usunElementZTablicy(int[] tablicaWejsciowa, int indeksDousuniecia) {
        int[] nowaTablica = new int[tablicaWejsciowa.length - 1];
        for (int pozycja = 0; pozycja < indeksDousuniecia; pozycja++) { //ciągniemy pętle do indeksu
            nowaTablica[pozycja] = tablicaWejsciowa[pozycja];
        }
        for (int pozycja = indeksDousuniecia; pozycja < nowaTablica.length; pozycja++) {
            nowaTablica[pozycja] = tablicaWejsciowa[pozycja + 1];
        }

        return nowaTablica;
    }

    static void wyswietlTablice(int[] tablica) {
        for (int pozycja = 0; pozycja < tablica.length; pozycja++) {
            System.out.print(tablica[pozycja] + ", ");
        }
        System.out.println();
    }
}
