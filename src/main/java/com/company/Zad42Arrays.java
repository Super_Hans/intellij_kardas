package com.company;

import java.util.Arrays;

//**ZADANIE #42*
//Utwórz metodę, która jako parametr przyjmuje tablicę i *zwraca nową tablicę* z liczbami w odwrotnej kolejności.
public class Zad42Arrays {
    public static void main(String[] args) {
        int[] tab = {1,2,3,4,5,6,7,8,9,0};
        System.out.println(Arrays.toString(tab));
        System.out.println(Arrays.toString(tablicaOdwrocona(tab)));
    }
    static int [] tablicaOdwrocona (int [] tablica) {
        int [] nowaTablica = new int [tablica.length];
        int dlugoscTablicy = tablica.length -1 ;
        for (int i = 0; i < tablica.length ; i++) {
            nowaTablica[i] = tablica[dlugoscTablicy];
            dlugoscTablicy--;

        }
        return nowaTablica;
    }
}
