package com.company;
//**ZADANIE #48*
//Utwórz metodę, która przyjmuje trzy parametry - tablicę oraz początek i koniec przedziału.
// Metoda ma zwrócić nową tablicę, która będzie zawierać elementy z wybranego przedziału.
//> Dla `([1, 2, 3, 4, 5],  2,  4)`
//> zwróci `[3, 4, 5]

import java.util.Arrays;

public class Zad48Array {
    public static void main(String[] args) {

        System.out.println(
                Arrays.toString(
                        zwracanaTablica(new int[]{2, 3, 4, 5, 6, 7}, 2, 4)));
    }

    public static int[] zwracanaTablica(int[] tablica, int startPrzedzialu, int koniecPrzedzialu) {
        int tablicaZmniejszona[] = new int[koniecPrzedzialu - startPrzedzialu + 1];
        for (int pozycja = startPrzedzialu; pozycja <= koniecPrzedzialu; pozycja++) {
            tablicaZmniejszona[pozycja - startPrzedzialu] = tablica[pozycja];

        }
        return tablicaZmniejszona;
    }
}
