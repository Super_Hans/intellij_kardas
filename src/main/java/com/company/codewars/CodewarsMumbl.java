package com.company.codewars;
//This time no story, no theory.
// The examples below show you how to write function accum:
//
//Examples:
//
//Accumul.accum("abcd");    // "A-Bb-Ccc-Dddd"

public class CodewarsMumbl {
    private final static String SEPARATOR = "-";

    public static void main(String[] args) {
        System.out.println(accum("abCd"));

    }

    public static String accum(String phrase) {
        StringBuilder result = new StringBuilder();
        String[] arrayString = phrase.toLowerCase().split("");
        int counter = 0;

        for (String letter : arrayString) {
            result.append(letter.toUpperCase());

            for (int index = 0; index < counter; index++) {
                result.append(letter);
            }
            result.append(SEPARATOR);
            counter++;
        }
        //return result.replaceFirst(".$","");
        return result.substring(0,result.length()-1);
    }
}
