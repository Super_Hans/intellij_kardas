package com.company.zadanie79;

public class Rownanie { // konstruktor 3parametrowy
    private int a; // dostęp tylko z tej klasy
    private int b;
    private int c; //Shift + Alt - sets up multiple cursors

    public Rownanie(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    Rownanie() { //konstruktor bezparametrowy

    }
//metody setter zastępujemy wartościami, które otrzymamy
    public void setA(int a) {               // set nic nie zwraca ewentualnie get
        this.a = a;                         //settery nie
    }

    public void setB(int b) {
        this.b = b;
    }

    public void setC(int c) {
        this.c = c;
    }

    @Override
    public String toString() {    //metoda pokazujaca równanie
        return a + "^2 + " + b + "^3 + " + c + "^4 = ";
    }

    // metoda obliczająca równanie:
    double obliczRownanie() {
        // metoda typu static - wywoływana na klasie Math
        return (Math.pow(a, 2) + Math.pow(b, 3) + Math.pow(c, 4));
    }

    boolean czyWiekszaodParametr(int parametr) {
        //zwraca wartość logiczną czy wartość przekazana - parametr - jest większa od wartości zwracanej przez metodę

        return obliczRownanie() > parametr;
    }
}