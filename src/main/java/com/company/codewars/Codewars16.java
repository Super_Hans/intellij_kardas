package com.company;

import java.util.Arrays;

//*Write a method, that gets an array of integer-numbers and return an array of the averages
// of each integer-number and his follower, if there is one.
//
//Example:
//Input:  [ 1, 3, 5, 1, -10]
//Output:  [ 2, 4, 3, -4.5]
public class Codewars16 {
    public static void main(String[] args) {
        int[] numbers = new int[]{1,3,5,1  -10};
        System.out.println(Arrays.toString(averages(numbers)));
    }

    public static double[] averages(int[] numbers) {
        double[] arrayWithAverages = new double[numbers.length - 1];
        if (numbers == null || numbers.length <=1) return new double [0];
        for (int i = 0; i < numbers.length - 1; i++) {
            arrayWithAverages[i] = (numbers[i] + numbers[i+1]) / 2.0;
        }
        return arrayWithAverages;
    }
}
