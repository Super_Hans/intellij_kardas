package com.company.zadanie95;

public class CurrencyEx {
    private double sellEx;
    private double buyEx;
    private Currency currency;

    CurrencyEx(double sellEx, double buyEx, Currency currency) {
        this.sellEx = sellEx;
        this.buyEx = buyEx;
        this.currency = currency;
    }

    @Override
    public String toString() {
        return String.format("%s _ %s _ %s", sellEx, buyEx, currency);
    }

    void showProfit(double amount) {
        double exchangeRate = amount / sellEx;
        System.out.printf("For %.2f PLN you will buy %.2f %ss",
                amount,
                exchangeRate,
                currency.getDescription(),
                currency.getDescription());
    }

}
