package com.company.zadanie173;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Zad173 {
    public static void main(String[] args) {
        Student student = new Student(1001, "Martin");
        Student student2 = new Student(1001, "Martin");

        System.out.println("Student hashcode = " + student.hashCode());
        System.out.println("Student hashcode = " + student2.hashCode());
        System.out.println("Are Objects equals? -> " + student.equals(student2));

        List<Student> studentList = new ArrayList<>();
        studentList.add(student);
        studentList.add(student2);
        System.out.println("Size of list " + studentList.size());

        Set<Student> studentSet = new HashSet<>();
        studentSet.add(student);
        studentSet.add(student2);
        System.out.println("Size of set " + studentSet.size());

        System.out.println("Does list contains students? " +
                studentList.contains(new Student(1001, "Martin")));
        System.out.println("Does set contains students? " +
                studentSet.contains(new Student(1001, "Martin")));

        Student student1 = new Student(1002,"Matt");
        studentList.add(student1);
        studentSet.add(student1);
        System.out.println("Size with new student "+ studentList.size());
        System.out.println("Size with new student "+ studentSet.size());

        System.out.println("Student's list "+ studentList);
        System.out.println("Student's set "+ studentSet);
    }
}
