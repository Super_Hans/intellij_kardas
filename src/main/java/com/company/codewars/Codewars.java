package com.company;

public class Codewars {
    /*  public static void main(String[] args) {
          System.out.println(makeNegative(17));
      }

          public static int makeNegative ( final int x){
              return x > 0 ? -x : x;
          }
      }
      */
    public static void main(String[] args) {
        //*Your task is to create a function that does four basic mathematical operations.
        //
        //The function should take three arguments - operation(string/char), value1(number), value2(number).
        //The function should return result of numbers after applying the chosen operation.
        //basicOp('+', 4, 7)         // Output: 11
        //basicOp('-', 15, 18)       // Output: -3
        //basicOp('*', 5, 5)         // Output: 25
        //basicOp('/', 49, 7)        // Output: 7
        System.out.println(basicMath("-", 3, 43));
    }

    public static int basicMath(String op, int v1, int v2) {
        if (op == "+") {
            return v1 + v2;
        }
        if (op == "-") {
            return v1 - v2;
        }
        if (op == "*") {
            return v1 * v2;
        }
        if (op == "/") {
            return v1 / v2;
        }
        return 0;
    }

}
