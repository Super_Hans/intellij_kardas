package com.company;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

//Utwórz metodę która przyjmuje liczbę, a następnie tyle razy losuje liczbę (przy wykorzystaniu klasy `Random`).
// Metoda ma zwrócić mapę, która będzie przechowywać informację ile razy wystąpiła każda z liczb.
public class Zad113 {
    public static void main(String[] args) {
        System.out.println(getRandomMapHowMany(1910));

    }

    private static Map<Integer, Integer> getRandomMapHowMany(int random) {
        Random randNum = new Random();
        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < random; i++) {
            int randomNumber = randNum.nextInt(11);
            // int oldExec = map.get(randomNumber);
            // if (map.containsKey(randomNumber)) {
            // map.put(randomNumber,oldExec+1);
            // map.put(randomNumber,map.get(randomNumber)+1);
            // } else {
            // map.put(randomNumber,1);

            int numberOfExec = map.getOrDefault(randomNumber, 0);
            map.put(randomNumber, numberOfExec + 1);

        }
        return map;
    }
}
