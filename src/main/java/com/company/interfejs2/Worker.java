package com.company.interfejs2;

public class Worker extends Person {
    private Double salary;

    public Worker(String name, String surname, int age, Gender gender) {
        super(name, surname, age, gender);
    }



    @Override
    public String showDataFull() {
        if (salary != null) {
            return super.showDataFull() + "( salary = " + salary + " PLN )";
        }else {
            return super.showDataFull();
        }
    }
    public void setSalary(Double salary) {
        this.salary = salary;
    }
}
