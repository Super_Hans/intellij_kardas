package com.company.wzorceProjektowe6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Weather {
    private List<Observer> observerList = new ArrayList<>();
    private int currentTemp;

    void addNewObservers(Observer observer) {
        observerList.add(observer);
    }

    //overload
    void addNewObservers(Observer... observers) {
        observerList.addAll(Arrays.asList(observers));
    }

    void updateTemp(int newTemp) {
        currentTemp = newTemp;
        notifyAllObservers();
    }

    private void notifyAllObservers() {
//        for (Observer observer : observerList) {
//            observer.react(currentTemp);
//        }
        observerList.stream().forEach(observer -> observer.react(currentTemp));
    }
}
