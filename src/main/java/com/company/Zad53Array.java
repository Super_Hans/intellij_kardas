package com.company;

//**ZADANIE #53*
//Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca różnicę
// pomiędzy największym a najmniejszym elementem.
public class Zad53Array {
    public static void main(String[] args) {
int [] tablica = new int [] {3,5,7,99,13};
        System.out.println(zwrocLiczbeElementow(tablica));
    }

    public static int zwrocLiczbeElementow(int[] tablica) {
        int min = tablica[0];
        int max = tablica[0];

        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] > max) {
                max = tablica[i];

            }
            if (tablica[i] < min) {
                min = tablica[i];
            }
        }
        int diff = max - min;
        return diff;
    }

}
