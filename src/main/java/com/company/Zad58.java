package com.company;
//*ZADANIE #58*
//Utwórz metodę, która wykorzystuje *varargs*
// by przekazać do metody dowolną, większą od zera,
// liczbę elementów typu `String` i zwrócić jeden napis sklejony z nich.
public class Zad58 {
    public static void main(String[] args) {
        System.out.print(sklejonyNapis("Honda","robi","najlepsze","auta","^^"));

    }
    public static String sklejonyNapis (String... auta) {
        String nazwa = "";
        for (String s : auta) {
            nazwa += s + " ";
        }
return nazwa;
    }

}
