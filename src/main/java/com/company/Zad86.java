package com.company;

//*ZADANIE #86*
//Utwórz metodę, która przyjmuje dwa parametry - pierwszy datą (jako ciąg znaków)
// a drugi jest separatorem znaków. Metoda ma zwrócić rok z przekazanej daty.
//> Dla `("12/05/2018", "/")`, zwróci `"2018"`
//> Dla `("22.11.2007", ".")`, zwróci `"2007"`
public class Zad86 {
    public static void main(String[] args) {
        System.out.println(getYear("12/05/2018", "/"));
        System.out.println(getYear("12/05/2015"));
        System.out.println(getYear("12.05.1917", "\\."));
    }

    static String getYear(String date, String separator) {
        String[] array = date.split(separator);
        return array[array.length - 1];
    }

    static String getYear(String date) { //evoke method with only one parameter - we evoke method
        return getYear(date, "/");
    }
}
