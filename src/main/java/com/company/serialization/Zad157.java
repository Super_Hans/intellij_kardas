package com.company.serialization;

import java.io.*;

// package has to be the same
public class Zad157 {
    public static void main(String[] args) {
        Message message = new Message("New Message!!!", "All your base are belong to us");
        outputToFile(message);
        //Ctrl+Alt+V - remember
        Message readFile = readFile();
        System.out.println(readFile);


    }

    private static Message readFile() {
        try (
                FileInputStream fileInputStream = new FileInputStream("files/exercise157.txt");
                ObjectInputStream input = new ObjectInputStream(fileInputStream)
        ) {
            return (Message) input.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static void outputToFile(Message message) {
        // Try WITH RESOURCES (first in - last out) FILO
        try (FileOutputStream file = new FileOutputStream("files/exercise157.txt");
             ObjectOutputStream outputStream = new ObjectOutputStream(file)) {

            outputStream.writeObject(message);
            // not necessary with try
            // outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
