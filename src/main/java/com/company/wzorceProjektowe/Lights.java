package com.company.wzorceProjektowe;

class Lights {

    void lightsOn() {
        System.out.println("Lights have been switched on");
    }

    void lightsOff() {
        System.out.println("Lights have been switched off");
    }
}
