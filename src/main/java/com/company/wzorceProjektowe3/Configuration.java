package com.company.wzorceProjektowe3;

class Configuration {
    // singleton
    private int favNum;

    private static Configuration instance = null;
    // extremely important that constructor is private
    private Configuration() {
    }

    int getFavNum() {
        return favNum;
    }

    void setFavNum(int favNum) {
        this.favNum = favNum;
    }

    static Configuration getInstance() {
        if (instance == null) {
            instance = new Configuration();
            System.out.println("First time object created");
        }else {
            System.out.println("Field already created");
        }
        // instance is like shelf - object is always in the same place
        System.out.println(instance.hashCode());
        return instance;
    }
}
