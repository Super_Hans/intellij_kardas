package com.company;

public class Zad54 {
    public static void main(String[] args) {
        int[] tablica = new int[]{1, 2, 3, 10, 4, 6, 11, 2};
        System.out.println(zwrocSumeElementow(tablica));

    }

    public static double zwrocSumeElementow(int[] tablica) {
        int max = wartoscNajwieksza(tablica);
        double liczbaElementow = 0.0;
        int sumaElementow = 0;

        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] != max) {
                liczbaElementow++;
                sumaElementow += tablica[i]; //sumę zwieksza operator "+="
            }
        }
        return sumaElementow / liczbaElementow;
    }

    static int wartoscNajwieksza(int[] tablica) {
        int max = tablica[0];
        for (int element : tablica) {
            if (element > max) {
                max = element;
            }

            // alternatywa
            //  for (int i = 0; i < tablica.length; i++)
            //      int element = tablica[i];
            //}
        }
        return max;
    }
}
