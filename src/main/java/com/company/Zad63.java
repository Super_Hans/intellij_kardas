package com.company;

import java.util.Arrays;

//**ZADANIE #63*
//Utwórz metodę, która przyjmuje dwuwymiarową tablicę.
// Metoda ma zwracać tablicę która będzie zawierała sumy wszystkich kolumn.
public class Zad63 {
    public static void main(String[] args) {
        int[][] tablica = {
                {1, 2, 3},
                {7, 2, 1},
                {7, 2, 1},
                {7, 2, 1},
                {7, 2, 1},
                {8, -2, 3},
        };
        System.out.println(Arrays.toString(zwrocTabliceZSumami(tablica)));

    }

    public static int[] zwrocTabliceZSumami(int[][] tablica) {
        int[] sumaKolumn = new int[tablica[0].length];

        for (int wiersz = 0; wiersz < tablica.length; wiersz++) {
            for (int kolumna = 0; kolumna < tablica[0].length; kolumna++) {

                sumaKolumn[kolumna] += tablica[wiersz][kolumna];
            }
        }
        return sumaKolumn;
    }
}
