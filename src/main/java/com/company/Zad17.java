package com.company;
//*Utwórz metodę, do której przekazujesz wartość logiczną,
// a ona *wyświetli* napis `Tak`, w przypadku przekazania `
// true` oraz `Nie` w przypadku przekazania
public class Zad17 {

    public static void main(String[] args) {
        czyWarunekSpelniony();
    }

    static void czyWarunekSpelniony() {
        int i = -666;
        System.out.println(i > 20 ? "tak" : "nie");

        String message = i > 20 ? "tak" : "nie";
        System.out.println(message);
    }
}
