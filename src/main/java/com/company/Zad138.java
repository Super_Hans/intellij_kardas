package com.company;

import java.io.*;

//*ZADANIE #138*
//Utwórz metodę która przyjmuje dwa parametry, które są ścieżkami do plików.
// Metoda ma scalić pliki “zazębiając” linijki
public class Zad138 {
    public static void main(String[] args) {
        try {
            concatFiles("files/zadanie138.txt","files/zadanie138a.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void concatFiles(String path1, String path2) throws IOException {
        FileReader reader = new FileReader(path1);
        FileReader reader2 = new FileReader(path2);
        BufferedReader buffRead1 = new BufferedReader(reader);
        BufferedReader buffRead2 = new BufferedReader(reader2);
        BufferedWriter buffWrite = new BufferedWriter(new FileWriter("files/Finall38.txt"));

        while (true) {
            String lineFromFirst = buffRead1.readLine();
            String lineFromSec = buffRead2.readLine();
            if (lineFromFirst == null && lineFromSec == null) {
                break;
            }
            if (lineFromFirst != null) {
                buffWrite.write(lineFromFirst);
                buffWrite.newLine();

            }
            if (lineFromSec != null) {
                buffWrite.write(lineFromSec);
                buffWrite.newLine();

            }
        }
        buffWrite.close();

    }
}
