import com.company.zad150.Zad150;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TestZad150 {
    private Zad150 zadanko = new Zad150();

    @Test
    void ifArrayOK() {
        int expLength = 4;
        int[] expectedArray = {10, 11, 12, 13};
        int[] result = zadanko.showArrayFromGivenIndex(expLength);
        Assertions.assertEquals(expLength, result.length);
        Assertions.assertArrayEquals(expectedArray, result);
    }

    @Test
    void shouldReturnArrayEmptyWhenPassedZero() {
        int[] expectedArray = {};
        int[] result = zadanko.showArrayFromGivenIndex(0);
        Assertions.assertArrayEquals(expectedArray, result);
    }

    @Test
    void shouldReturnNegativeValues() {
        Assertions.assertThrows(NegativeArraySizeException.class,
                //lambda exp;functional programming
                () -> zadanko.showArrayFromGivenIndex(-3));
    }

    @Test
    void shouldReturnArrayWhenMaxValue () {
        //ToDo - fix after classes from JVM (Java Virtual Machine)
        int expLength = Integer.MAX_VALUE;
        Assertions.assertThrows(OutOfMemoryError.class,
                () -> zadanko.showArrayFromGivenIndex(expLength));
        int [] result = zadanko.showArrayFromGivenIndex(expLength);
        Assertions.assertEquals(expLength,result.length);
    }
}
