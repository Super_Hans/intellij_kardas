package com.company.zadanie179;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class City {
    @SerializedName("consolidated_weather")
    List<Day> days;

}

class Day {
    @SerializedName("the_temp")
    float temparture;

    @SerializedName("applicable_date")
    String date;
}
