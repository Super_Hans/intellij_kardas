package com.company;

public class Zad24 {
    public static void main(String[] args) {
        condition1(3);
        System.out.println();
        condi2(5);
    }

    static void condition1(int multiplying) {
        for (int i = 1; i <= multiplying; i++) {
            System.out.print(i * 10 + ", ");
        }
    }

    static void condi2(int multiplying) {
        int i = 1;
        while (i <= multiplying) {
            System.out.print(i * 10 + ", ");
            i++;
        }
    }
}
