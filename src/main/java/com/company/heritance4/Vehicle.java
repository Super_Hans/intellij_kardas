package com.company.heritance4;
// cannot make object from such class - this is just collection of types
abstract public class Vehicle {
    private String name;
    private int maxSpeed;
    private int passengerSites;

    Vehicle(String name, int maxSpeed, int passengerSites) {
        this.name = name;
        this.maxSpeed = maxSpeed;
        this.passengerSites = passengerSites;
    }
    abstract void showName () ;

}
