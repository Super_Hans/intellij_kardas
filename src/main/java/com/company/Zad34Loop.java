package com.company;

public class Zad34Loop {
    public static void main(String[] args) {
        showPrzedzial(1, 10, 3);
    }

    static void showPrzedzial(int start, int stop, int skok) {
        if (start >= stop) {
            System.out.println("Podano błedny przedział");
            return;
        } else if (stop - start <= skok) {
            System.out.println("Przedział jest za mały");
            return;
        }
        for (int i = start; i <= stop; i = i + skok) {
            System.out.println(i + ",");
        }

    }
}
