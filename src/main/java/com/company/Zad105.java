package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zad105 {
    public static void main(String[] args) {
        List<Integer> array = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        System.out.println(cutLists(array));

    }

    private static List<List<Integer>> cutLists(List<Integer> array) {
        List<Integer> oddList = new ArrayList<>();
        List<Integer> evenList = new ArrayList<>();

        for (Integer integer : array) {
            if (integer % 2 == 0) {
                evenList.add(integer);
            } else {
                oddList.add(integer);
            }
        }

        return new ArrayList<>(Arrays.asList(oddList, evenList));
    }
}
