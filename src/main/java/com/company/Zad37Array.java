package com.company;
// int [] t = new int [10]
//          = new int [] {3,5,8};
//          = {3,5,8};
//np. String [] imiona = new String [5];

import java.util.Arrays;
import java.util.stream.IntStream;

// int [][] plansza = new int [3] [6]
//**ZADANIE #37*
//Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca sumę wszystkich elementów
public class Zad37Array {
    public static void main(String[] args) {
        int[] array = new int[]{21, 2, 3, 8, 5};
        System.out.println(returnSum(array));
        System.out.println(zwrocSume(array));
        System.out.println(podajSrednia(array));
        System.out.println(find_average(array));
        System.out.println(getAverage(array));
    }

    static int returnSum(int[] array) {
        int suma = 0;
        for (int liczba : array) {               //iter
            suma += liczba;
        }
        return suma;
    }

    static int zwrocSume(int[] array) {
        int suma = 0;
        for (int liczba : array) {
            suma += liczba;
        }
        return suma;
    }

    static double podajSrednia(int[] array) {

        return zwrocSume(array) / (double) array.length; //rzutowanie z metody
    }
    public static double find_average(int[] array){
        return Arrays.stream(array).average().orElse(0); // ciekawa metoda z użyciem klas
    }
    public static int getAverage(int[] array){
        return (IntStream.of(array).sum())/array.length;
    }
}
