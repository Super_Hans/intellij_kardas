package com.company.wzorceProjektowe;

import java.util.Scanner;

// Facade
public class Zad159 {
    private final static int CODEIN = 1;
    private final static int CODEOUT = 2;

    public static void main(String[] args) {
        Facade panel = new Facade();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Options: ");
        System.out.println(CODEOUT + ": out of home");
        System.out.println(CODEIN + ": get back home");

        int d = scanner.nextInt();

        switch (d) {
            case CODEIN:
                panel.imIn();
                break;
            case CODEOUT:
                panel.imOut();
                break;
            default:
                System.out.println("Invalid option");
        }
    }

}
