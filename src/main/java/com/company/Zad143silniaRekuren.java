package com.company;

//Policz silnię dla podanej wartości przy wykorzystaniu *rekurencji*.
public class Zad143silniaRekuren {
    public static void main(String[] args) {
        System.out.println(calcSilnia(5));
    }

    private static int calcSilnia(int value) {
        System.out.println("value = " + value);
        if (value == 1) {
            return 1;
        } else {
            return value * calcSilnia(value - 1);
        }
    }
}
