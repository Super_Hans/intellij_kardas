package com.company;

import com.google.common.base.CaseFormat;
import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.io.Files;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Zad180Guava {
    public static void main(String[] args) {
//        method();
//        method1();
//        method2();
//        method3();
//        method4();
        method5();

    }

    private static void method5() {
        File file = new File("files/filesReading.txt");

        try {
            List<String> list = Files.readLines(file, Charsets.UTF_8);
            System.out.println(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void method4() {
        String nickname = "SexyGoy";
        String name = CaseFormat
                .UPPER_CAMEL
                .to(CaseFormat.LOWER_UNDERSCORE, nickname);

        System.out.println(nickname);
        System.out.println(name);
    }

    private static void method3() {
        BiMap<Integer, String> bimap = HashBiMap.create();

        //keys and values MUST be unique
        bimap.put(1, "Matt");
        bimap.put(2, "Martin");
        bimap.put(3, "Adam");
        bimap.put(4, "Mathew");
        bimap.put(5, "Tom");
        bimap.put(6, "Mattie");

        System.out.println(bimap);
    }

    private static void method2() {
        //Map<Integer,Set<String>>
        Multimap<Integer, String> multimap = HashMultimap.create();

        multimap.put(31, "January");
        multimap.put(28, "February");
        multimap.put(31, "March");
        multimap.put(30, "April");
        multimap.put(31, "May");
        multimap.put(30, "June");

        System.out.println(multimap);

        System.out.printf("Months with 30 days are: %s", multimap.get(30));
    }

    private static void method1() {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "Monday");
        map.put(2, "Tuesday");
        map.put(3, "Wednesday");
        map.put(4, "Thursday");
        map.put(5, "Friday");

        String joined = Joiner
                .on(", ")
                .withKeyValueSeparator("-->")
                .join(map);

        System.out.println(joined);
    }

    private static void method() {
        String[] names = {"Tomas", "Mattie", "Kalina", null, "Martin", null, "Johnny"};

        String joined = Joiner.on(", ")
                //this method skips nulls
                .skipNulls()
                .join(names);
        System.out.println(joined);

        String joined2 = Joiner
                .on(" | ")
                .useForNull("--NOPE--")
                .join(names);

        System.out.println(joined2);
    }
}
