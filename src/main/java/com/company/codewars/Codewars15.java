package com.company;

public class Codewars15 {
    public static void main(String[] args) {
        System.out.println(dontGiveMeFive(-225, -34));

    }

    public static int dontGiveMeFive(int start, int end) {
        int result = 0;
        for (int i = start; i <= end; i++) {
            result++;
            if (i % 10 == 5 || i % 10 == -5) {                                     //  if (!("" + i).contains("5")) result++; ???
                result--;
            }
        }
        return result;
    }
}
