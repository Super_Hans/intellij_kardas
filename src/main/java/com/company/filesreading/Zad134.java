package com.company.filesreading;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

//*ZADANIE #134*
//Utwórz metodę która przyjmuje jako parametr ścieżkę do pliku,
// a następnie wyświetla liczbę linii oraz liczbę znaków w pliku
public class Zad134 {
    public static void main(String[] args) {
        try {
            countLinesAndChars("files/filesReading.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void countLinesAndChars(String filePath) throws IOException {
        BufferedReader buff = new BufferedReader(new FileReader(filePath));
        String line = buff.readLine();
        int lineCounter = 0;
        int charCounter = 0;

        while (line != null) {
            lineCounter++;
            charCounter += line.length();
            line = buff.readLine();
        }
        System.out.printf("Number of lines in file is %s and number of characters is %s ",
                lineCounter,
                charCounter);

    }
}
