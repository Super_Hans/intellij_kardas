package com.company;
//*

public class Zad25 {
        public static void main(String[] args) {
            warunek1(0, 6, 2.5);
            System.out.println();
            warunek2(0, 6, 2.5);
        }

        static void warunek1(int start, int amount, double jump) {
            for (int step = start; step < amount; step++) {

                System.out.print(step * jump + ", ");

            }

        }

        static void warunek2(int start, int amount, double jump) {
            int step = start;
            while (step < amount) {
                System.out.print(step * jump + ", ");
                step++;
            }
        }
    }
