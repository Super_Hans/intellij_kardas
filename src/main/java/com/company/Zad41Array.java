package com.company;

import java.util.Arrays;

//**ZADANIE #41*
//Utwórz metodę, która zwraca tablicę o podanym rozmiarze wypełnioną kolejnymi wartościami zaczynając od `10`:
//> Dla `4` zwróci: `[10, 11, 12, 13]`
//>
//> Dla `8` zwróci: `[10, 11, 12, 13, 14, 15, 16, 17]`
public class Zad41Array {
    public static void main(String[] args) {
System.out.println(Arrays.toString(wypelnionaTablica(8)));
    }
    static int [] wypelnionaTablica (int rozmiar) {
        int [] tablica = new int [rozmiar];
        //int wartosc = 10;
        for (int pozycja = 0; pozycja < rozmiar; pozycja++) {
            tablica [pozycja] = pozycja +10;
           // wartosc++;
        }
        return tablica;
    }

}
