package com.company;

//*ZADANIE #80*
// Utwórz metodę, która przyjmuje liczbę w formie napisu,
// a następnie ma zwrócić (jako `String`) sumę wszystkich cyfr
// Dla `"536"`, zwróci `"14"` (bo 5 + 3 + 6 = 14)
// Dla `"12345"`, zwróci `"15"`
public class Zad80String {
    public static void main(String[] args) {
        System.out.println(zwrocSume("666"));
        System.out.println(zwrocSume("536"));
        System.out.println(zwrocSume("24566"));
        System.out.println(zwrocSume("12345"));
    }

    public static String zwrocSume(String liczba) {
        String[] naszaTablica = liczba.split("");
        int suma = 0;//tekst jest Klasą, np. chaAt wyciaga kolejne znaki, .length zwraca długość
        for (String cyfra : naszaTablica) {
            suma += Integer.parseInt(cyfra); // metoda static; typ klasowy - przechowuje metody; na typie prymitywnym nie wywołamy metod
        }         //endsWith ("kota") isEmpty (sprawdza czy było coś wpisane); split (dzieli po wybranym znaku)
        //toCharArray (napis zamieniony na tablice ['s','r','a']znaków
        //trim - ucina biale znaki na końcu
        return String.valueOf(suma); //wywołuje metodę statyczną na klasie;
    }

}
