package com.company.heritance;
//*ZADANIE #118*
//Utwórz klasę `Osoba`, która zawiera pola `imie`, `wiek`. Następnie utwórz klasę `Pracownik`,
// która zawiera dodatkowe pola `pensja`.
// Następnie utwórz klasę `Kierownik`,
// która rozszerza klasę `Pracownik` i posiada dodatkowe pole `premia`.
// Utwórz obieky wszystkich klas oraz przetestuj działanie
public class Manager extends Worker {
    private int bonus;


    public Manager(String name, String surname, int age, int salary) {
        super(name, surname, age, salary);

    }

    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    @Override
    public int showMonthlySalary(int monthCount) {
        return super.showMonthlySalary(monthCount) + monthCount*bonus;
    }

    @Override
    String getNameAndSurnameOfAPerson() {
        return super.getNameAndSurnameOfAPerson() + " and his bonus is " + bonus;
    }
}
