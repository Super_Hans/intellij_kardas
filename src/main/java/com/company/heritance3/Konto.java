package com.company.heritance3;

public class Konto {
    //works as package + for inheritance classes
    protected double stan;
    private String wlasciciel;
    private int numerKonta;
    private static int idKonta = 0;


    public Konto(String wlasciciel) {
        this.wlasciciel = wlasciciel;
        //user cc automatically increases
        numerKonta = idKonta++;
    }

    @Override
    public String toString() {
        // always use String.format cause it's nice
        return String.format
                ("Właścicielem konta (o numerze %s) jest - %s. Stan konta = %s",
                        numerKonta, wlasciciel, stan);
    }

    double pokazStanKonta() {
        return stan;
    }

    void wplataNaKonto(double wplata) {
        stan += wplata;
    }

    boolean wyplataZKonta(double wyplata) {
        if (stan >= wyplata) {
            stan -= wyplata;
            return true;
        }
        return false;
    }

    static void przelewanieSrodkow(Konto nadawca, Konto odbiorca, double kwotaPrzelana) {
        if (nadawca.wyplataZKonta(kwotaPrzelana)) {
            odbiorca.wplataNaKonto(kwotaPrzelana);
        }
    }

    public int getNumerKonta() {
        return numerKonta;
    }

}
