package com.company;
//*Utwórz metodę, która przyjmuje wysokość trójkąta Pascala
// a następnie tworzy odpowiednią strukturę danych i ją wypełnia
//*dla `6`  zwróci
//>`         [1]
//>`       [1, 1]
//>`      [1, 2, 1]
//>`     [1, 3, 3, 1]
//      [1 ,4 ,6 ,4, 1]
//>   [1, 5, 10, 10, 5, 1]
//> [1, 6, 15, 20, 15, 6, 1]

import java.util.Arrays;

public class Zad69Pascal {
    public static void main(String[] args) {
        wyswietlTrojkat(dejTrojkat(7));
    }

    public static int[][] dejTrojkat(int wysTroj) {
        int[][] trojkatPascala = new int[wysTroj][]; //nie można podać drugiej wartosci zostawiamy zatem puste
        for (int wiersz = 0; wiersz < wysTroj; wiersz++) {
            trojkatPascala[wiersz] = new int[wiersz + 1];//dodaje kolejną pozycje tymczasowo
            int liczba = 1; //potrzebujemy do wstawienia liczb, nie musimy nazywać nowej tablicy;

            for (int kolumna = 0; kolumna <= wiersz; kolumna++) { //wypełnia kolumny konkretnymi wartościami, konkretnej dług
                trojkatPascala[wiersz][kolumna] = liczba;
                liczba = liczba * (wiersz - kolumna) / (kolumna + 1);
            }
        }
        return trojkatPascala;
    }

    static void wyswietlTrojkat(int[][] trojkatPascala) {
        for (int[] wiersz : trojkatPascala) {
            System.out.println(Arrays.toString(wiersz));
        }
    }

}
