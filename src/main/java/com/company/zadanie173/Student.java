package com.company.zadanie173;

import java.util.Objects;
import java.util.Random;

public class Student {
    private int id;
    private String name;


    Student(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

//    @Override
//    public int hashCode() {
////         we use primal numbers i.e. 31, 7;
//        int hash = 31;
//        hash = 31 * hash + id;
//        hash = 31 * hash + (name == null ? 0 : name.hashCode());
//        return hash;
////        return  new Random().hashCode();
//        //same objects MUST have same hash codes!!!
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (obj == this) {
//            return true;
//        }
//        if (obj == null) {
//            return false;
//        }
//        if (obj instanceof Student) {
//            Student student = (Student) obj;
//            return student.id == this.id
//                    &&
//                    student.name.equals(this.name);
//        } else {
//            return false;
//        }
//    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
    // equals and Hashcode always together!!!
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;
        //
        if (getId() != student.getId()) return false;
        return getName() != null ? getName().equals(student.getName()) : student.getName() == null;
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        return result;
    }
}
