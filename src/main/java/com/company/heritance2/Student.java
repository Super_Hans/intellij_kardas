package com.company.heritance2;

import com.company.codewars.StudySubject;

import java.util.HashMap;
import java.util.Map;

public class Student extends Person {
    private University university;
    private int yearOfStudy;
    private Map <StudySubject, Integer> degrees = new HashMap<>();

    public Student(String name, int age, Sex sex,
                   University university, int yearOfStudy,
                   Map<StudySubject, Integer> degree) {
        super(name, age, sex);
        this.university = university;
        this.yearOfStudy = yearOfStudy;
    }


    void addDegree (StudySubject nameOfStudy, int degree) {
        degrees.put(nameOfStudy,degree);
    }

    @Override
    public String toString() {
        return super.toString() + " and on year " + yearOfStudy + " has such " + degrees;
    }
}
