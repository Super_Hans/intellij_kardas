package com.company.interfejs2;

public interface IntroduceYourself {
    String showDataFull ();
    String getFullName ();
    int yearsToPension ();
}
