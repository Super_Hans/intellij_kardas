package com.company.codewars;

import com.company.heritance2.Teacher;

public class StudySubject {
    private String nameOfStudies;
    private Teacher teacher;

    public StudySubject(String nameOfStudies, Teacher teacher) {
        this.nameOfStudies = nameOfStudies;
        this.teacher = teacher;
    }

    @Override
    public String toString() {
        return nameOfStudies;
    }
}
