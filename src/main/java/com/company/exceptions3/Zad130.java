package com.company.exceptions3;

//Utwórz metodę, która przyjmuje parametry typu `int` korzystając z mechanizmu `varargs`.
// W przypadku nie przekazania żadnego parametru,
// metoda powinna rzucić własny wyjątek (z komunikatem o powodzie).
// W przypadku podania parametrów, metoda powinna zwrócić ich sumę.
public class Zad130 {
    public static void main(String[] args) {
        // block try-catch to catch exceptions
        try {
            System.out.println(returnSum(5, 2, 34, 15));
            System.out.println(returnSum());
        } catch (MyException exception) {
            // we have access to getMessage via parent class Exception
            System.out.println(exception.getMessage());;
        }
    }
    // initializing parameters via varargs mechanism;
    private static int returnSum(int... numbers) throws MyException {
        int sum = 0;
        // condition
        if (numbers.length == 0) {
            // throw new object of class
            throw new MyException("Array is empty (no parameters to sum up)");
        }
        for (int number : numbers) {
            sum += number;

        }
        return sum;
    }
}
// self-made exception class which extends (inheritance) class Exception
class MyException extends Exception {
    MyException(String message) {
        //adding one parameter which is implemented to constructor of parent class
        super(message);
    }
}
