package com.company.wzorceProjektowe7;

class HtmlDocument extends Doc {

    HtmlDocument(DocFactory.DocumentType type, String content) {
        super(type, formatText(content));
    }
    private static String formatText (String txt){
        String format = String.format("<h1>%s</h2>", txt);
        String extraBreak=  format.replaceAll("\n","</br>");
        extraBreak = "<meta charset=\"utf-8\"/>"+extraBreak;
        return extraBreak;
    }
}
