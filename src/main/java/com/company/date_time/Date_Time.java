package com.company.date_time;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.UnsupportedTemporalTypeException;
import java.util.Scanner;
import java.util.Set;

import static java.time.LocalDate.*;

class Date_Time {
    public static void main(String[] args) {
        /*LOCAL TIME*/
//        method1();
//        method2();
//        method3();
//        method4();
//        method5();

        /*LOCAL DATE*/
//        method6();
//        method7();
//        method8();
//        method9();
//        method10();

        /*LOCAL DATE TIME*/
//        method11();
//        method12();

        /*TIME ZONE*/
//        method13();
//        method14();
//        method15();

        /*PERIOD & DURATION*/
//        method16();
//        method17();
        method18();
    }

    private static void method18() {
        String firstDateTime = "21.05.1981 19:15";
        String secDateTime = "01.12.2018 16:20";

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

        LocalDateTime firstLocalDate = LocalDateTime.parse(firstDateTime, dateTimeFormatter);
        LocalDateTime secLocalDate = LocalDateTime.parse(secDateTime, dateTimeFormatter);

        long years = ChronoUnit.YEARS.between(firstLocalDate, secLocalDate);
        long months = ChronoUnit.MONTHS.between(firstLocalDate, secLocalDate);
        long weeks = ChronoUnit.WEEKS.between(firstLocalDate, secLocalDate);
        long days = ChronoUnit.DAYS.between(firstLocalDate, secLocalDate);
        long hours = ChronoUnit.HOURS.between(firstLocalDate, secLocalDate);
        long minutes = ChronoUnit.MINUTES.between(firstLocalDate, secLocalDate);

        System.out.printf("Between %s and %s difference is:\n", firstDateTime, secDateTime);
        System.out.println(years + " years");
        System.out.println(months + " months");
        System.out.println(weeks + " weeks");
        System.out.println(days + " days");
        System.out.println(hours + " hours");
        System.out.println(minutes + " minutes");

    }

    private static void method17() {
        String firstDate = "21.05.1981";
        String secDate = "01.12.2018";

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        LocalDate firstLocalDate = LocalDate.parse(firstDate, dateTimeFormatter);
        LocalDate secLocalDate = LocalDate.parse(secDate, dateTimeFormatter);

        Period period = Period.between(firstLocalDate, secLocalDate);

        System.out.printf("Between %s and %s difference is:\n",
                firstDate, secDate);
        System.out.println(period.getDays() + " days");
        System.out.println(period.getMonths() + " months");
        System.out.println(period.getYears() + " years");
    }

    private static void method16() {
        String firstTime = "11:15";
        String secTime = "16:16";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        LocalTime firstLocalTime = LocalTime.parse(firstTime, formatter);
        LocalTime secLocalTime = LocalTime.parse(secTime, formatter);

        Duration duration = Duration.between(firstLocalTime, secLocalTime);

        System.out.printf("Between %s and %s the difference is:\n", firstLocalTime, secLocalTime);
        System.out.println(duration.toHours() + " hours");
        System.out.println(duration.toMinutes() + " minutes");
        System.out.println(duration.getSeconds() + " seconds");
    }

    private static void method15() {
        Set<String> availableZoneIds = ZoneId.getAvailableZoneIds();
        availableZoneIds.stream()
                .sorted()
                .forEach(System.out::println);
    }

    private static void method14() {
        LocalDateTime inputLocalDateTime = LocalDateTime.now();
        ZoneId inputZone = ZoneId.of("Europe/Warsaw");

        ZoneId outputZone = ZoneId.of("US/Hawaii");
        LocalDateTime outputLocalDateTime =
                inputLocalDateTime.atZone(inputZone)
                        .withZoneSameInstant(outputZone)
                        .toLocalDateTime();

        System.out.println(inputLocalDateTime);
        System.out.println(outputLocalDateTime);
    }

    private static void method13() {
        LocalDateTime inputDate = LocalDateTime.now();

        ZonedDateTime zonedDateTime = ZonedDateTime.of(inputDate, ZoneOffset.UTC);

        ZoneId zoneId = ZoneId.of("Europe/Sarajevo");

        ZonedDateTime outputZonedDateTime = zonedDateTime.withZoneSameInstant(zoneId);

        LocalDateTime dateTime = outputZonedDateTime.toLocalDateTime();
        System.out.println(inputDate);
        System.out.println(dateTime);

    }

    private static void method12() {
        String inputDate = "1.12.2018 15:03";

        DateTimeFormatter inputFormat =
                DateTimeFormatter.ofPattern("d.MM.yyyy HH:mm");
        LocalDateTime date = LocalDateTime.parse(inputDate, inputFormat);
        System.out.println(date);

        DateTimeFormatter outputFormat =
                DateTimeFormatter.ofPattern("dd LL yyy (D 'day of the year'), HH:mm");
        System.out.println(date.format(outputFormat));


    }

    private static void method11() {
        LocalDateTime localDateTime =
                LocalDateTime.of(2018, Month.DECEMBER, 1, 14, 40, 15);

        System.out.println(localDateTime.toString());

        DayOfWeek dayOfWeek = localDateTime.getDayOfWeek();
        System.out.println(dayOfWeek);
        System.out.println(dayOfWeek.getValue());

        // change of offset
        DayOfWeek plus = dayOfWeek.plus(1);
        System.out.println(plus);

        Month month = localDateTime.getMonth();
        System.out.println(month);
        System.out.println(month.getValue());

        int minuteOfDay = localDateTime.get(ChronoField.MINUTE_OF_DAY);
        System.out.println(minuteOfDay);

        long nanoOfDay = localDateTime.getLong(ChronoField.NANO_OF_DAY);
        System.out.println(nanoOfDay);


    }

    private static void method10() {
        String inputDate = "4/12/2018";

        DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("d/M/y");

        LocalDate date = parse(inputDate, inputFormat);
        System.out.println("ISO Date: " + date.format(DateTimeFormatter.ISO_DATE));
        System.out.println("Basic ISO date: " + date.format(DateTimeFormatter.BASIC_ISO_DATE));
        System.out.println("ISO ordinal date: " + date.format(DateTimeFormatter.ISO_ORDINAL_DATE));
        System.out.println("ISO WEEK date: " + date.format(DateTimeFormatter.ISO_WEEK_DATE));
        System.out.println("ISO local date: " + date.format(DateTimeFormatter.ISO_LOCAL_DATE));
    }

    private static void method9() {
        String inputDate = "1/12/2018";

        DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("d/MM/yyyy");
        LocalDate date = parse(inputDate, inputFormat);

        int day = date.get(ChronoField.DAY_OF_YEAR);

        System.out.println(day);

        DateTimeFormatter outputFormat = DateTimeFormatter.ofPattern("D");
        System.out.println(date.format(outputFormat));

        System.out.println(date.getDayOfYear());
    }

    private static void method8() {
        Scanner s = new Scanner(System.in);
        System.out.print("Please enter date format: ");
        String input = s.nextLine();

        try {
            LocalDate date = now();
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(input);
            System.out.println("Date: " + date.format(dateTimeFormatter));
        } catch (UnsupportedTemporalTypeException e) {
            System.out.println("Congrats! Wrong format...");
        } catch (IllegalArgumentException e) {
            System.out.println("Ooops! Too many letters");
        }

    }

    private static void method7() {
        LocalDate date = of(2018, 12, 1);

        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        System.out.println(date.format(format));

        DateTimeFormatter format2 = DateTimeFormatter.ofPattern("yyy");
        System.out.println(date.format(format2));

        //declined month
        DateTimeFormatter format3 = DateTimeFormatter.ofPattern("MMM");
        System.out.println(date.format(format3));

        DateTimeFormatter format4 = DateTimeFormatter.ofPattern("EEEE");
        System.out.println(date.format(format4));

        //no-declined month
        DateTimeFormatter format5 = DateTimeFormatter.ofPattern("LLLL");
        System.out.println(date.format(format5));

        DateTimeFormatter format6 = DateTimeFormatter.ofPattern("d MMMM yyyy (EEEE)");
        System.out.println(date.format(format6));

        System.out.println();
    }

    private static void method6() {
        LocalDate date = of(2018, Month.DECEMBER, 1);

        System.out.println(date.toString());

    }

    private static void method5() {
        String inputTime = "13:05:32";

        DateTimeFormatter format = DateTimeFormatter.ofPattern("HH:mm:ss");

        LocalTime time = LocalTime.parse(inputTime, format);

        DateTimeFormatter outputFormat = DateTimeFormatter.ofPattern("HH_mm_ss");

        String outputTime = time.format(outputFormat);

        System.out.println(outputTime);
    }

    private static void method4() {
        String currentTime = "12+51--47";

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH+mm--s");

        LocalTime time = LocalTime.parse(currentTime, dateTimeFormatter);

        System.out.println(time);

        int hour = time.get(ChronoField.HOUR_OF_DAY);
        int minute = time.get(ChronoField.MINUTE_OF_HOUR);

        System.out.printf("It is %s minutes after %s\n",
                minute, hour);
    }

    private static void method3() {
        LocalTime t1 = LocalTime.now();

        System.out.println(t1);
        System.out.println(t1.getNano());
    }

    private static void method1() {
        LocalTime time = LocalTime.of(12, 36);
        System.out.println(time.getHour());

        System.out.printf("Hour: %s, minute: %s, seconds: %s\n",
                time.getHour(),
                time.getMinute(),
                time.getSecond());

        System.out.println(time.toString());
    }

    private static void method2() {

        LocalTime time = LocalTime.parse("12:43:25");

        System.out.println(time.getSecond());
        System.out.println(time.getHour());
        System.out.println(time.getMinute());
        System.out.println(time.getNano());
    }
}

