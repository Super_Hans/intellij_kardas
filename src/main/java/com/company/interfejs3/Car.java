package com.company.interfejs3;

public class Car extends Vehicle {
    private int numOfWheels;
    private int numOfDoors;

    Car(String name, int numOfWheels, int numOfDoors) {
        super(name);
        this.numOfWheels = numOfWheels;
        this.numOfDoors = numOfDoors;
    }

    @Override
    public String toString() {
        return String.format(
                "Car %s has %s wheels and %s doors. %s \n%s",
                name, numOfWheels, numOfDoors,
                maxSpeed > 0 ? "Max speed is: " + maxSpeed + " km/h" : "",
                numberOfPassengers > 0 ? "Number of passenger sites: " + numberOfPassengers : "");
    }

}