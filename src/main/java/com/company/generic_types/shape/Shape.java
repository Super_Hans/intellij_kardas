package com.company.generic_types.shape;

public interface Shape {
    String getName();

    default void hello () {
        System.out.println("Allo! Ich bin shape\n");
        //static method could not be implemented
    }
}
