package com.company.heritance4;

public class Car extends Vehicle {
    private int numberOfWheels =4;
    private int numberOfDoors=5;

    public Car(String name, int maxSpeed, int passengerSites, int numberOfWheels, int numberOfDoors) {
        super(name, maxSpeed, passengerSites);
        this.numberOfWheels = numberOfWheels;
        this.numberOfDoors = numberOfDoors;
    }
    public Car(String name, int maxSpeed, int passengerSites) {
        super(name, maxSpeed, passengerSites);

    }
    // it is necessary in every inheritance class
    // you cannot make new object from this one
    // every class must have have it!!!
    @Override
    void showName() {

    }
}

