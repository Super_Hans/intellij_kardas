package com.company;

//**ZADANIE #61*
//Utwórz metodę, która przyjmuje conajmniej DWA parametry - boolean oraz dowolną ilość (większą od 0) liczb (typu `int`).
// Gdy pierwszy parametr ma wartość `true` metoda zwraca największą przekazaną liczbą, a gdy `false` najmniejszą
public class Zad61 {
    public static void main(String[] args) {
        System.out.println(zwrocNajwiekszaLiczbe(true,1,2,3,-7,5,9));
        System.out.println(zwrocNajwiekszaLiczbe(false,1,2,3,-7,5,9));
    }

    public static int zwrocNajwiekszaLiczbe(boolean czyWieksza, int... dowolnaIloscLiczb) {
           int max = dowolnaIloscLiczb[0];
           int min = dowolnaIloscLiczb[0];
        for (int elementZ : dowolnaIloscLiczb) {
            if (elementZ>max){
                max=elementZ;
            }
            if (elementZ<min) {
                min = elementZ;
            }
        }
        return czyWieksza ? max : min;
    }
}
