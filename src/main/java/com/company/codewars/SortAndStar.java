package com.company.codewars;

import java.util.Arrays;

public class SortAndStar {
    public static void main(String[] args) {
        String [] array = new String[]{"DcD","wDr","Coin"};
        System.out.println(twoSort(array));
    }

    static String twoSort(String[] s) {
        String [] letters = Arrays.stream(s)
                .sorted()
                .findFirst()
                .orElse("")
                .split("");
        //first parameter is what it will be BETWEEN
        return String.join("***",letters);
    }
}
