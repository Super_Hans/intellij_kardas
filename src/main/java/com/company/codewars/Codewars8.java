package com.company;

import java.util.Arrays;

public class Codewars8 {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(reverse(5)));
    }

    public static int[] reverse(int dlugTablicy) {
        int[] outputTable = new int[dlugTablicy];
        int licznik = dlugTablicy;
        for (int indeks = 0; indeks < dlugTablicy; indeks++) {
            outputTable[indeks] = licznik;
            licznik--;
        }
        return outputTable;
    }

}
