package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zad99ArrayList {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>(Arrays.asList(15, 7, -4, 8, 33));

        System.out.println(returnHigh(list));
        System.out.println(returnHigh2(list));
    }

    private static int returnHigh(List<Integer> list) {
        int highest = list.get(0);
        for (int position = 0; position < list.size(); position++) {
            if (list.get(position) > highest) {
                highest = list.get(position);
            }
        }
        return highest;
    }

    private static int returnHigh2(List<Integer> list) {
        int highest = list.get(0);
        for (Integer element : list) {
            if (element > highest) {
                highest = element;
            }
        }
        return highest;
    }
}
