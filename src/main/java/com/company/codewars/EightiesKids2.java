package com.company.codewars;
//..........
//..........
//..........
//.......X..
//..........
//..........
//
//The map will be given in the form of a string with \n separating new lines.
// The bottom left of the map is [0, 0]. X is ALF's spaceship.
//In this example:
//findSpaceship(map) => "[7, 2]"

import java.util.HashMap;
import java.util.Map;

public class EightiesKids2 {
    public static void main(String[] args) {
        System.out.println(findSpaceship("..........\n" +
                "..........\n" +
                "..........\n" +
                "..........\n" +
                ".......X..\n" +
                ".........."));

    }

    private static String findSpaceship(String map) {
        // This is where the magic happens
        String[] strings = map.split("\n");

        for (int i = 0; i < strings.length; i++) {
            if (strings[i].indexOf('X') >= 0) {
                return String.format("[%d, %d]", strings[i].indexOf('X'), strings.length - i - 1);
            }
        }
        return "Spaceship lost forever.";

    }
}
