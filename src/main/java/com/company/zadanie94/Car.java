package com.company.zadanie94;

public class Car {
    private String name;
    private int prodYear;
    private Color color;

    Car(String name, int prodYear, Color color) {
        this.name = name;
        this.prodYear = prodYear;
        this.color = color;
    }
    public String toString () {
        return String.format("%s (%s) Color: %s", name, prodYear, color);
    }
}
