package com.company.wzorceProjektowe4;

// Builder

public class User {
    //external class fields
    private String name;
    private String surname;
    private int age;
    private boolean isMan;

    private User(UserBuilder userBuilder) {
        //re-write values from builder field User
        name = userBuilder.name;
        surname = userBuilder.surname;
        age = userBuilder.age;
        isMan = userBuilder.isMan;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", isMan=" + (isMan ? "man" : "woman") +
                '}';
    }

    //static class for class - we can create object of class within class without creating new class
    static class UserBuilder {
        private String name = "no name";
        private String surname = "no surname";
        private int age = 0;
        private boolean isMan;

        UserBuilder setName(String name) {
            this.name = name;
            // returns object of this class
            return this;
        }

        UserBuilder setSurname(String surname) {
            this.surname = surname;
            return this;
        }

        UserBuilder setAge(int age) {
            this.age = age;
            return this;
        }

        UserBuilder setGender(boolean isMan) {
            this.isMan = isMan;
            return this;

        }

        User build() {
            return new User(this);
        }
    }
}
