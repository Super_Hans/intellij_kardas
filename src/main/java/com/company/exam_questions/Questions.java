package com.company.exam_questions;

public class Questions {
    public static void main(String[] args) {
        String[] arrayDegrees = {"+", "-", "-", "+", "+", "+", "+", "-", "-", "-",
                "+", "-", "+", "-", "+", "+", "-", "+", "-", "+", "-", "-", "+",
                "-", "+", "-", "-", "-", "+", "+", "-", "-", "+", "+", "-", "+",
                "+", "-", "-", "-", "+", "-", "-", "+", "+", "+", "+", "+", "-",
                "-", "+", "-", "+", "+", "+", "-", "+", "-", "-", "-", "+", "+",
                "-", "-", "-", "+", "-", "-", "+", "+", "+", "-", "+",
                "+", "+", "+", "+", "+"
        };


        System.out.println(howManyGoodAnswers(arrayDegrees));

    }

    static int howManyGoodAnswers(String[] array) {
        int degreesGood = 0;

        for (String s : array) {
            if (s.equals("+")) {
                degreesGood++;
            }
        }
        return degreesGood;
    }

}
