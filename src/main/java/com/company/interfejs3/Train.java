package com.company.interfejs3;

public class Train extends Vehicle {
    private int numOfWagons;
    private boolean ifBarWagon;
    // constructor name is after class name by default
    Train(String name, int numOfWagons, boolean ifBarWagon) {
        // calling constructor of upperclass
        super(name);
        this.numOfWagons = numOfWagons;
        this.ifBarWagon = ifBarWagon;
    }

    @Override
    public String toString() {
        return String.format("Train %s has %s wagons%s. %s \n%s",
                name,
                numOfWagons,
                ifBarWagon ? "(has bar wagon)" : ", but does not have bar wagon",
                maxSpeed > 0 ? "Max speed is: " + maxSpeed : "",
                numberOfPassengers > 0 ? "Passengers capacity " + numberOfPassengers : "");
    }
}
