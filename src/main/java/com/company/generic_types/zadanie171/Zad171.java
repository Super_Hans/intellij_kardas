package com.company.generic_types.zadanie171;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//*ZADANIE #171*
//Utwórz generyczną klasę, która przechowuje TRZY elementy
// (`imie`, `wiek`, `czyKobieta`).
// Zrób listę (kilku) tych elementów a następnie ją wyświetl.
public class Zad171 {
    public static void main(String[] args) {

        List<GenericClass<String, Integer, Boolean>> list =
                Arrays.asList(
                        new GenericClass<>("Marcin", 17, false),
                        new GenericClass<>("Kasia", 27, true),
                        new GenericClass<>("Tom", 25, false),
                        new GenericClass<>("Matt", 28, false)
                );
        list.forEach(consumer -> System.out.println(consumer));

        List<GenericClass<Integer, Integer, Integer>> list2 =
                Arrays.asList(
                        new GenericClass<>(12, 800, 33),
                        new GenericClass<>(22, -800, 3),
                        new GenericClass<>(2, 0, 13),
                        new GenericClass<>(124, 40, 17)
                );

        list2.forEach(System.out::println);
    }
}
