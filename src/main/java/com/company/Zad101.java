package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zad101 {
    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>(Arrays.asList("olej", "bierze", "Honda"));
        System.out.println(reverseString(stringList));
        System.out.println(reverseString2(stringList));

    }

    private static List<String> reverseString(List<String> list) {
        List<String> revertList = new ArrayList<>();

        for (String s : list) {
            revertList.add(0, s);

        }

        return revertList;
    }

    private static List<String> reverseString2(List<String> stringList) {
        List<String> revertList = new ArrayList<>();

        for (String element : stringList) {
            //it puts it on position 0 and then it puts another element,
            // first one will become last  - from left to one
            revertList.add(0, element);
        }

        return revertList;
    }

}
