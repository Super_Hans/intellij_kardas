package com.company;
//**ZADANIE #35*
//Utwórz metodę, która przyjmuje dwa parametry oraz *wyświetla* ciąg elementów.
// Pierwszy parametr metody określa wyświetlany element, a drugi parametr liczbę wystąpień,
//> Dla `9, 5` wyświetli: `9 99 999 9999 99999`
//>
//> dla `3, 3` wyświetli: `3 33 333`
//>
//> dla `8 4` wyświetli: `8 88 888 8888`
public class Zad35 {
    public static void main(String[] args) {
wyswietlCiag(9,5);
    }
    static void wyswietlCiag (int cyfra, int liczbawystapien) {
        for (int i = 0; i < liczbawystapien; i++) {
            for (int j = 0; j <= i ; j++) {
                System.out.print(cyfra);
            }
            System.out.printf(" ");
            }

        }
    }

