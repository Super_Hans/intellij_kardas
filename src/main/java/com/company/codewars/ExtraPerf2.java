package com.company.codewars;

import java.util.Arrays;
import java.util.stream.IntStream;

public class ExtraPerf2 {
    public static void main(String[] args) {
        //   int num1 = 35;
        //   System.out.println(Integer.toBinaryString(num1));
        System.out.println(Arrays.toString(extraPerfect(20)));
    }

    private static int[] extraPerfect(int number) {
        return IntStream.rangeClosed(1, number)
                .filter(ExtraPerf2::isBinaryStringCorrect)
                //              .filter(num -> {
//                    boolean result = isBinaryStringCorrect(num);
//                    System.out.printf("%s -> %s  %s\n ", num, Integer.toBinaryString(num), result);
//                    return result;
                //               })
                .toArray();

    }

    private static boolean isBinaryStringCorrect(int num) {
        final char startEnd = '1';
        String binaryString = Integer.toBinaryString(num);

        if (binaryString.charAt(0) == startEnd
                && binaryString.charAt(binaryString.length() - 1) == startEnd) {
            if (binaryString.length() < 3) {
                return true;
            } else {

                String substring = binaryString.substring(1, binaryString.length() - 1);
                return !substring.contains("1");
            }
        }
        return false;
    }
}
