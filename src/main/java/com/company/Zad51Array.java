package com.company;

import java.util.Arrays;

//**ZADANIE #51*
//Utwórz metodę, która przyjmuje dwie tablice.
// Metoda ma zwrócić tablicę z sumą kolumn podanych tablic.
// To znaczy element na pozycji `0` z pierwszej tablicy, dodajemy do elementu na pozycji `0` z drugiej tablicy.
//*Przyjęte założenia:*
//- Tablice są tych samych długości.
//- Wartości na danej pozycji mogą być większę niż 10
//
//>
//[1,  2,  3,  4]
//[5,  6,  7,  8]
//[6,  8, 10, 11]```
public class Zad51Array {
    public static void main(String[] args) {
        int[] tablica = new int[]{1, 2, 3, 4};
        int[] tablica2 = new int[]{5, 6, 7, 8};
        System.out.println(Arrays.toString(zwrocTabliceZSumami(tablica, tablica2)));

    }

    public static int[] zwrocTabliceZSumami(int[] tablica, int[] tablica2) {
        int[] tablicaZSumami = new int[tablica.length];
        for (int i = 0; i < tablica.length; i++) {
            tablicaZSumami[i] = tablica[i] + tablica2[i];

        }
        return tablicaZSumami;
    }
}
// #Zadanie 52 do DOMU