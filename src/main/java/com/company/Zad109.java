package com.company;
//Porównaj czasy `LinkedList<>` i `ArrayList<>`:
//- dodawania na końcu
//- dodawania na początku (index `0`)
//- wybierania spod indexu (po kolei)
//- usuwania spod indexu (po kolei) (edited)

import java.util.ArrayList;
import java.util.LinkedList;

public class Zad109 {
    private static final int NUM_OF_OPPS = 100_000;
    private static final int ELEMENT_TO_ADD = 999;
    public static void main(String[] args) {

        long timeStart = System.currentTimeMillis();
        ArrayList<Integer> arrayList = addingArrList();
        long timeEnd = System.currentTimeMillis();
        System.out.println("Concatenation ArrayList time: " + (timeEnd - timeStart));

        timeStart = System.currentTimeMillis();
        LinkedList<Integer> linkedList = addingLinkList();
        timeEnd = System.currentTimeMillis();
        System.out.println("Concatenation LinkedList time: " + (timeEnd - timeStart));

        timeStart = System.currentTimeMillis();
        chooseUnderPosition(arrayList);
        timeEnd = System.currentTimeMillis();
        System.out.println("Get element from arrlist time: " + (timeEnd - timeStart));

        timeStart = System.currentTimeMillis();
        chooseUnderPositionLL(linkedList);
        timeEnd = System.currentTimeMillis();
        System.out.println("Get element from linklist time: " + (timeEnd - timeStart));

        timeStart = System.currentTimeMillis();
        chooseUnderPositionForeachLL(linkedList);
        timeEnd = System.currentTimeMillis();
        System.out.println("Get element from linklist foreach time: " + (timeEnd - timeStart));

        timeStart = System.currentTimeMillis();
        chooseUnderPositionForeachAL(arrayList);
        timeEnd = System.currentTimeMillis();
        System.out.println("Get element from array list foreach time: " + (timeEnd - timeStart));

        timeStart = System.currentTimeMillis();
        removeFromPosZero(arrayList);
        timeEnd = System.currentTimeMillis();
        System.out.println("Remove from array list foreach time: " + (timeEnd - timeStart));

    }
    private static ArrayList <Integer> addingArrList() {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < NUM_OF_OPPS; i++) {
            //array list has to copy whole list
            list.add(0,ELEMENT_TO_ADD);
        }
        return list;
    }
    private static LinkedList<Integer> addingLinkList() {
        // Linked list know what is first;
        LinkedList<Integer> list = new LinkedList<>();
        for (int i = 0; i < NUM_OF_OPPS; i++) {
            //add new element to first position - no need to
            list.add(0,ELEMENT_TO_ADD);
        }
        return list;
    }
    private static int chooseUnderPosition(ArrayList<Integer> list) {
        int newNumber = 0;
        for (int i = 0; i < list.size() ; i++) {
            newNumber = list.get(i);

        }
        return newNumber;
    }
    private static int chooseUnderPositionLL(LinkedList<Integer> list) {
        int newNumber = 0;
        for (int i = 0; i < list.size(); i++) {
            newNumber = list.get(i);

        }
        return newNumber;
    }
    private static int chooseUnderPositionForeachLL(LinkedList<Integer> list) {
        int newNumber = 0;
        for (Integer integer : list) {
            newNumber = integer;
        }

        return newNumber;
    }
    private static int chooseUnderPositionForeachAL(ArrayList<Integer> list) {
        int newNumber = 0;
        for (Integer integer : list) {
            newNumber = integer;
        }
        return newNumber;
    }
    private static ArrayList<Integer> removeFromPosZero(ArrayList<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            list.remove(i);
        }
        return list;
    }
}
