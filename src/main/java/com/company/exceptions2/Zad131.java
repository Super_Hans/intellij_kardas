package com.company.exceptions2;

import java.util.Scanner;

//*ZADANIE #131*
//Utwórz metodę, która przyjmuje dwa parametry - napis (`String`) oraz liczbę (`int`)
// wcześniej odczytaną `Scanner`-em od użytkownika.
// Metoda ma wyświetlić podany napis, przekazaną liczbę razy.
// W przypadku gdy liczba wystąpień będzie mniejsza bądź równa ZERO
// powinien zostać rzucony własny wyjątek (dziedziczący po `RuntimeException`).

//signature of class
public class Zad131 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("How many times to execute: ");
        int howManyTimes = scan.nextInt();
        scan.nextLine();

        System.out.print("Add phrase: ");
        String phrase = scan.nextLine();

        try {
            showPhrase(phrase, howManyTimes);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static void showPhrase(String phrase, int howManyTimes) {
        if (howManyTimes <= 0) {
            throw new WrongExecNumberException(
                    "Given integer " + howManyTimes + " is not correct execution number");
        }
        for (int i = 0; i < howManyTimes; i++) {

            System.out.print(phrase + " ");
        }
    }
}
