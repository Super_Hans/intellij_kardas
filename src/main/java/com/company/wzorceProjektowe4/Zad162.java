package com.company.wzorceProjektowe4;

public class Zad162 {
    public static void main(String[] args) {
        User user1 = new User.UserBuilder()
                .setName("Karl")
                .setSurname("Marx")
                .setAge(68)
                .setGender(true)
                .build();

        User user2 = new User.UserBuilder()
                .setName("Włada")
                .setSurname("Bytomska")
                .setAge(33)
                .setGender(false)
                .build();

        System.out.println(user1);
        System.out.println(user2);
    }
}
