package com.company;

public class Zad10War {
    public static void main(String[] args) {
        System.out.println(moreThan(3));
        System.out.println(moreThan(24));

        System.out.println(lessThan(39));
        System.out.println(lessThan(13));

        int number = 12;
        boolean result = moreThan(number);
        if (result) {
            System.out.println("Liczba " + number + " jest większa od 20");
        }else {
            System.out.println("Liczba " + number + " jest mniejsza od 20");
        }

    }

    static boolean moreThan(int x) {
        if (x >= 20) {
            return true;
        } else
            return false;
    }

    static boolean lessThan(int x) {
        return x >= 20;
    }
}

