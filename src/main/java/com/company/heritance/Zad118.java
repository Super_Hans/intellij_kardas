package com.company.heritance;

public class Zad118 {
    public static void main(String[] args) {
        Person person = new Person("Przemek", "Kardas", 33);
        System.out.println(person.getNameAndSurnameOfAPerson());
        Person person2 = new Person("John", "Miller", 75);
        System.out.println(person2.getNameAndSurnameOfAPerson());

        Worker worker = new Worker("Zdzisław", "Kręcina", 79, 90000);
        System.out.println(worker.getNameAndSurnameOfAPerson());

        System.out.println(worker.showMonthlySalary(5));
        Manager manager = new Manager("John", "Dupchek", 55, 17000);
        System.out.println(manager.showMonthlySalary(3));
        manager.setBonus(1000);
        System.out.println(manager.showMonthlySalary(3));
        System.out.println(manager.getNameAndSurnameOfAPerson());
    }
}