package com.company.generic_types.shape;
/**Boxing class accepts only those classes that
co-relate with given class( implements interface or extends given class)
i.e. Shape*/
public class ShapeBox<TYPE extends Shape> {

    private TYPE shape;

    public ShapeBox(TYPE shape) {
        this.shape = shape;
    }

    public TYPE getShape() {
        return shape;
    }
    public String getNameOfShape(){
        return shape.getName();
    }
}
