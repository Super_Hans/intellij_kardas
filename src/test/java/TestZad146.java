import com.company.Zad146test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TestZad146 {

    @Test
    void shouldCheckIfTaxOK() {
      double result = Zad146test.amountAfterTax(100,23);
        Assertions.assertEquals(123.0,result);
    }

    @Test
    void shouldBeNegativeTax (){
        double result = Zad146test.amountAfterTax(100,-23);
        Assertions.assertEquals(100,result);
    }

    @Test
    void shouldBeMoreThanSumTax () {
        double result = Zad146test.amountAfterTax(100, 100);
        Assertions.assertEquals(200, result);
    }

    @Test
    void shouldBeMoreTaxMoreThan105() {
        double result = Zad146test.amountAfterTax(100, 105);
        Assertions.assertEquals(100, result);
    }

    @Test
    void shouldTaxBeEqualZero() {
        double result = Zad146test.amountAfterTax(100, 0);
        Assertions.assertEquals(100, result);
    }
}
