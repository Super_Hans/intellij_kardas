package com.company.wzorceProjektowe2;

import java.util.Scanner;

public class Zad160 {
    final private static int WITHDRAWN = 1;
    final private static int DEPOSIT = 2;

    public static void main(String[] args) {
        BankingMachine atm = new BankingMachine();
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter PIN:");
        String pin = scanner.nextLine();

        System.out.print("Enter amount of money: ");
        int money = scanner.nextInt();

        System.out.println("Withdraw: 1");
        System.out.println("Deposit: 2");

        int d = scanner.nextInt();

        switch (d) {
            case WITHDRAWN:
                atm.withdrawMoney(money, pin);
                break;
            case DEPOSIT:
                atm.inputMoney(money, pin);
                break;
            default:
                System.out.println("Invalid option");
        }
    }

}
