package com.company;

public class Zad16 {
    public static void main(String[] args) {
        System.out.println(przedzialParametrow(1,10,5));
        System.out.println(przedzialParametrow(4,5,17));
    }

    static boolean przedzialParametrow(int poczPar, int koniecPar, int liczbaDoSprawdzenia) {
        return liczbaDoSprawdzenia >= poczPar && koniecPar >= liczbaDoSprawdzenia;
    }
}
