package com.company.zadanie96;

class TelephoneNum {
    private String number;
    private Countrycode countrycode;

    TelephoneNum(String number, Countrycode countrycode) {
        this.number = number;
        this.countrycode = countrycode;
    }
    void showNumber () {
        System.out.println("("+ countrycode.asText()+
                ")"
                + number);
    }
}
