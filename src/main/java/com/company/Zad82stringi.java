package com.company;

//*ZADANIE #82*
//Utwórz metodę, która przyjmuje ciąg znaków (gdzie wyrazy rozdzielone są znakiem spacjami)
// a następnie zwraca najdłuższy z nich
//> Dla `"Ala ma kota."`, zwróci `"kota"` (gdyż jest najdłuższym wyrazem)
public class Zad82stringi {
    public static void main(String[] args) {
        String text = "Przemek ma psa";
        System.out.println(retLongestPhrase(text));
        System.out.println(retLongestPhrase2(text));
    }

    public static String retLongestPhrase(String zdanie) {
        int najdluzszyWyraz = 0;
        String najdluzsy = "";
        String[] tablica = zdanie.split(" "); // separatorem jest spacja

        for (String element : tablica) {
            if (element.length() > najdluzszyWyraz) {
                najdluzszyWyraz = element.length();  // podmienia wartości
                najdluzsy = element;                  // przypisujemy do elementu
            }
        }
        return najdluzsy;
    }
    public static String retLongestPhrase2(String zdanie) {

        String najdluzsy = "";
        String[] tablica = zdanie.split(" "); // separatorem jest spacja

        for (String element : tablica) {
            if (element.length() > najdluzsy.length()) {// podmienia wartości
                najdluzsy = element;                  // przypisujemy do elementu
            }
        }
        return najdluzsy;
    }
}
