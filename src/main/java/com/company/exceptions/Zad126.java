package com.company.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

//*ZADANIE #126*
//Utwórz metodę która przyjmuje tablicę, a następnie przy wykorzystaniu `Scannera` użytkownik może podać indeks parametru.
// W przypadku indeksu poza zakresem (tzn. gdy wystąpi wyjątek) zapytaj użytkownika o indeks ponownie.
public class Zad126 {
    public static void main(String[] args) {
        //avoidExcept();
        avoidException();
        // Homework! czy jak znów user poda błednie to też mi wyrzuci?
        // nie wiem, ale mam to zrobić np. "Znów podałeś zły index"

    }

    private static void avoidException() {
        int[] array = new int[]{1, 13, 56, 889, 22};
        Scanner s = new Scanner(System.in);

        while (true) {
            System.out.print("Podaj proszę indeks: ");
            try {
                int indeks = s.nextInt();
                System.out.printf(
                        "Na pozycji %s jest liczba %s\n",
                        indeks, array[indeks]);
                break;
                // as detailed as possible!!!

            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Podano zły indeks");
                // shows exception details
                e.printStackTrace();
            } catch (InputMismatchException e) {
                System.out.println("Nie podano liczby");
                // to clear Scanner
                s.nextLine();
            }
        }
    }

    private static void avoidExcept() {
        int[] array = new int[]{12, 666, 85, 3};
        Scanner scan = new Scanner(System.in);
        System.out.print("Podaj indeks: ");
        int index = scan.nextInt();
        try {
            System.out.println(array[index]);
        } catch (Exception e) {
            System.out.println("Zły indeks!");
        }
    }


}
