package com.company.codewars;

import java.util.Arrays;
import java.util.Collections;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Opstrings {
    public static void main(String[] args) {
        method1();
    }

    private static void method1() {
        //functional programming
        Function<Integer, Boolean> function = (n) -> n % 2 == 0;
        System.out.println(function.apply(10));
    }

    public static String vertMirror(String strng) {
        String[] arr = strng.split("\n");
        System.out.println(strng);
        return Arrays.stream(arr)
                .map(word -> new StringBuilder(word).reverse())
                .collect(Collectors.joining("\n"));
    }

    public static String horMirror(String s) {
        String[] arr = s.split("\n");
        Collections.reverse(Arrays.asList(arr));
        return String.join("\n", arr);

    }

    public static String oper(Function<String, String> operator, String s) {
        return operator.apply(s);
    }

}
