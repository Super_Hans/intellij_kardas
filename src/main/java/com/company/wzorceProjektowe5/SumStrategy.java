package com.company.wzorceProjektowe5;

public class SumStrategy implements Strategy {

    @Override
    public double calculate(double number1, double number2) {
        return number1+number2;
    }

    @Override
    public String nameOfOps() {
        return "adding";
    }
}
