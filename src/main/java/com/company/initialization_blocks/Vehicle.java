package com.company.initialization_blocks;

class Vehicle {
    {
        // block are exec first and before every constructor,
        // static blocks are executed ONLY ONCE!
        System.out.println("Initializing block of Vehicle2");
    }

    static {
        System.out.println("Initializing STATIC block of Vehicle");

    }

    {
        System.out.println("Initializing block of Vehicle");
    }

    Vehicle() {
        System.out.println("Vehicle constructor");
    }
}
