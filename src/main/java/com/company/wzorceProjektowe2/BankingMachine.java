package com.company.wzorceProjektowe2;

class BankingMachine {
    private PINverificator piNverificator = new PINverificator();
    private Account account = new Account();

    void withdrawMoney(int money, String pin) {
        if (piNverificator.isPINOK(pin)) {
            if (account.getBalance() > money) {
                System.out.println("Balance before: " + account.getBalance());
                account.withdrawnMoney(money);
                System.out.println("Balance after: " + account.getBalance());

            } else {
                System.out.println("No money on account");
            }
        } else {
            System.out.println("Wrong PIN");
        }

    }

    void inputMoney(int money, String pin) {
        if (piNverificator.isPINOK(pin)) {
            System.out.println("Balance before: " + account.getBalance());
            account.putMoney(money);
            System.out.println("Balance after: " + account.getBalance());

        } else {
            System.out.println("Wrong PIN");
        }
    }
}
