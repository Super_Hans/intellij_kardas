package com.company.wzorceProjektowe5;

public class DivideStrategy implements Strategy {
    @Override
    public double calculate(double number1, double number2) {
        if(number2!=0){

            return number1/number2;
        }
        return 0;
    }

    @Override
    public String nameOfOps() {
        return "dividing";
    }
}
