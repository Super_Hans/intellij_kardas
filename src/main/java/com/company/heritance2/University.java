package com.company.heritance2;

import java.util.ArrayList;
import java.util.List;

public class University {
    private String nameOfUni;
    private String address;
    private List<Student> listOfStudents = new ArrayList<>();
    private List<Teacher> listOfTeachers = new ArrayList<>();

    public University(String nameOfUni, String address) {
        this.nameOfUni = nameOfUni;
        this.address = address;
    }

    void addStudent(Student student) {
        listOfStudents.add(student);
    }

    void hireTeacher(Teacher teacher) {
        listOfTeachers.add(teacher);
    }
    void  showStudents (){
        //System.out.println(listOfStudents);
        System.out.println("In University " + nameOfUni + " students: ");
        for (Student students : listOfStudents) {
            System.out.println(students);
        }
    }
}
