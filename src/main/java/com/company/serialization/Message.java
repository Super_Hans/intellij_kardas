package com.company.serialization;

import java.io.Serializable;

public class Message implements Serializable {
    //when /transient/ key word is placed - field in class will not be serialized!!!
    transient private String title;
    // when field in class is not modified - it stays on base value(i.e. null)
    private String content;

    Message(String title, String content) {
        this.title = title;
        this.content = content;
    }

    @Override
    public String toString() {
        return "Message{" +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
