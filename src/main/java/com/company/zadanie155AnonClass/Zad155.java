package com.company.zadanie155AnonClass;

public class Zad155 {
    public static void main(String[] args) {
        //method1();
        method2();
    }
    private static void method1(){
        Person person = new Person("Karl","Marx",68);
        String info = person.introduceYourself();
        System.out.println(info);

    }
    private static void method2(){
        String info = new Person("Lukas","de Gaulle",35){
            @Override
            String introduceYourself() {
                return String.format("Tschuss! Ich bin %s %s und ich bin %s Jahre alt",
                        getName(),
                        getSurname(),
                        getAge());
            }
        }.introduceYourself();

        System.out.println(info);
    }
}
