package com.company.exceptions2;

class WrongExecNumberException extends RuntimeException {
    WrongExecNumberException(String message) {
        super(message);
    }
}
