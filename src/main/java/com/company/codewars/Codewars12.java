package com.company;

import static java.util.Arrays.stream;

public class Codewars12 {
    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3, 4};
        int[] arr2 = {1, 1, 1, 1};
        System.out.println(arrayPlusArray(arr1, arr2));
    }

    public static int arrayPlusArray(int[] arr1, int[] arr2) {

        return stream(arr1).sum() + stream(arr2).sum();
    }
}
