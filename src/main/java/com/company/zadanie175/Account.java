package com.company.zadanie175;

public class Account {
    private int number;
    private String owner;
    // modyficator volatile == only one thread has access to this class fields
    private volatile double balance;

    Account(int number, String owner, double balance) {
        this.number = number;
        this.owner = owner;
        this.balance = balance;
    }

    public int getNumber() {
        return number;
    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return String.format("Owner: %s number: %s Balance is: %s Thread ID is: %s",
                owner,
                number,
                balance,
                Thread.currentThread().getId());
    }

    synchronized void deposit(double amount) {
        balance += amount;
    }

    synchronized void withdraw(double amount) {
//
//        if (balance < amount) {
//            throw new IllegalArgumentException("Balance is too low");
//        }
        balance -= amount;
    }
}
