package com.company.generic_types.without_generic;

public class AppleBox {
    private Apple apple;

    public Apple getApple() {
        return apple;
    }

    public AppleBox(Apple apple) {
        this.apple = apple;
    }
}
