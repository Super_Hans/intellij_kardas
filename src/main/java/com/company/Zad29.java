package com.company;
//**ZADANIE #29*
//Utwórz metodę, do której przekazujesz dwa parametry następnie wyswietlasz wszystkie liczby z podanego przedziału podzielne przez 3 albo 5
//Dane wyświetl w formie:
//> dla `0, 13` wyświetli:
//>
// ```0  <-- 3, 5
// 1
// 2
// 3  <-- 3
// 4
// 5  <-- 5
// 6
// 7
// 9  <-- 3
//10  <-- 5
//11
//12  <-- 3
//13
//14
//15  <-- 3, 5```

public class Zad29 {
    public static void main(String[] args) {
        showNumbers(1, 15);
    }

    public static void showNumbers(int start, int stop) {
        for (int i = start; i <= stop; i++) {
            System.out.print(i);

            if (i % 3 == 0 && i % 5 == 0) {
                System.out.print("\t <-- 3,5");
            }else if (i % 3 == 0) {
                System.out.print("\t <--3 ");
            }else if (i % 5 == 0) {
                System.out.print("\t <--5");

            }
            System.out.println();
        }
    }
}
