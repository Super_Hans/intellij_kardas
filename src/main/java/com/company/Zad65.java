package com.company;

import java.util.Arrays;

//*Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
// Metoda ma “przesunąć” elementy w środku tablicy o tyle ile wynosi wartość drugiego elementu i zwrócić nową tablicę.
// Dla wartości dodatniej przesuwa w prawo, a dla ujemnej w lewo.
// Elementy z końca tablicy lądują na początku (i na odwrót) dla `([1,2,3,4,5,6],  2)`
//> należy zwrócić `[5,6,1,2,3,4]` (przesunięcie o dwa w prawo)
//> dla `([1,2,3,4,5,6],  -3)`
//> należy zwrócić `[4,5,6,1,2,3]` (przesunięcie o trzy w lewo)
public class Zad65 {
    public static void main(String[] args) {
        int [] array = new int [] {1,2,3,4};
        int parametr = 2;
        System.out.println(Arrays.toString(zwrocTabliceZPoprzesuwanymiElementami(array, parametr)));

    }
    public static int [] zwrocTabliceZPoprzesuwanymiElementami (int [] array, int parametr) {
        int[] tablicaPrzesunięta = new int[array.length];

        for (int i = array.length; i >= 0; i--) {
            if (parametr >= 0) {

                tablicaPrzesunięta[array.length-1] = tablicaPrzesunięta[parametr];


            }
            if(parametr<0) {
                tablicaPrzesunięta[i] = i - parametr;

            }

        }
        return tablicaPrzesunięta;
    }

}
