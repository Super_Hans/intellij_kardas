package com.company.zadanie148;

public class SumElements {
    public Integer howManyElementsToSumUp(int[] array, int number) {
        if (array == null) {
            return null;
        }
        int sum = 0;
        int count = 0;
        for (int i : array) {
            sum += i;
            count++;
            if (sum > number) {
                return count;
            }
        }
        return null;
    }
}
