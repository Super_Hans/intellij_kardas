package com.company.datastreams;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Zad158 {
    private static List<Person> people = Arrays.asList(
            new Person("Marcus", "Miller", 23),
            new Person("Wojtek", "Dupa", 29),
            new Person("Kumar", "L", 25),
            new Person("Kamila", "Pupek", 28),
            new Person("Danuta", "Giez", 73)
    );

    public static void main(String[] args) {
//        method1();
//        method2();
//        method3();
//        method4();
//        method5();
//        method6();
//        method7();
//        method8();
//        method9();
//        method10();
//        method11();
//        method12();
//        method13();
//        method14();
//        method15();
//        method16();
//        method17();
//        method18();
//        method19();
//        method20();
        method21();

    }

    static void method1() {
        IntStream
                //does not include last number
                .range(1, 10)
                .forEach(number -> {
                    System.out.println(number);
                });
    }

    static void method2() {
        IntStream
                //rangeClosed includes last integer
                .rangeClosed(1, 10)
                .forEach(System.out::print);
    }

    private static void method3() {
        Random random = new Random();
        random
                .ints(-10, 20)
                .limit(5)
                .forEach(System.out::println);
    }

    private static void method4() {
        List<Integer> list = new Random()
                .ints(-10, 10)
                .limit(10)
                //changes basic types to Class types
                .boxed()
                //collect elements to correct structures
                .collect(Collectors.toList());

        System.out.println(list);
    }

    private static void method5() {
        int[] array = {231, -3, 15, 82};
        Arrays
                .stream(array)
                .forEach(number ->
                        System.out.print(number + ","));

    }

    private static void method6() {
        int[] array = {3, 45, 34, 67};
        Arrays
                .stream(array)
                .map(number -> number * 2)
                .forEach(number -> System.out.print(number + ","));
    }

    private static void method7() {
        IntStream.range(1, 25)
                .filter(number -> number % 3 == 0)
                .forEach(System.out::println);
    }

    private static void method8() {
        int[] array = {33, 90, 5, 78, 11};
        double average = Arrays.stream(array)
                .filter(number -> number % 3 == 0)
                .average()
                .orElse(0);
        System.out.println(average);
    }

    private static void method9() {
        IntStream.rangeClosed(20, 30)
                .filter(number -> number % 2 == 0)
                .average()
                .ifPresent(number ->
                        System.out.println(number));
    }

    private static void method10() {
        IntStream.iterate(1, number -> number + 2)
                .limit(8)
                .forEach(number ->
                        System.out.println(number));
    }

    private static void method11() {
        List<Integer> list = IntStream.rangeClosed(1, 100)
                .filter(number -> Math.sqrt(number) % 1 == 0)
                .boxed()
                .collect(Collectors.toList());
        System.out.println(list);
    }

    private static void method12() {
        List<Integer> list = Arrays.asList(1, 4, 5, 6, 8, 9, 12);
        IntSummaryStatistics stats = list.stream().mapToInt(number -> number).summaryStatistics();
        System.out.println("Max : " + stats.getMax());
        System.out.println("Average : " + stats.getAverage());
        System.out.println("Sum : " + stats.getSum());
        System.out.println("Min : " + stats.getMin());
        System.out.println("Number of elements : " + stats.getCount());
    }

    private static void method13() {
        List<String> list = Arrays.asList("Kapitan", "Bomba", "", "G***O", "", "Bazyl");
        List<String> collection = list.stream()
                .filter(word -> !word.isEmpty())
                .filter(word -> word.startsWith("B") || word.startsWith("G"))
                //.map(word->word.toUpperCase())
                .map(String::toUpperCase)
                .collect(Collectors.toList());
        System.out.println(collection);
    }

    private static void method14() {
        List<String> list = Arrays.asList("Kapitan", "Bomba", "", "G***O", "", "Bazyl", "kołowrotek");
        List<Integer> wordsLengths = list.stream()
                .filter(word -> !word.isEmpty())
                //.map(word->word.length())
                .map(String::length)
                //here starts integers
                .collect(Collectors.toList());

        System.out.println(wordsLengths);
    }

    private static void method15() {
        List<String> list = Arrays.asList("A2", "C4", "B15", "M4", "S7");
        List<Integer> listOfParameters = list.stream()
                .filter(word -> word.length() == 2)
                .map(word -> word.substring(1))
//                .map(word->{)
//                .map(word->Integer.parseInt(word))
                .map(liczba -> {
                    try {
                        return Integer.parseInt(liczba);
                    } catch (NumberFormatException e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        System.out.println(listOfParameters);

    }

    private static void method16() {
        Stream.of("M2", "B9", "C4", "F16", "G8")
                .map(word -> word.substring(1))
                //.mapToInt(word->Integer.parseInt(word))
                .mapToInt(Integer::parseInt)
                .max()
                .ifPresent(System.out::println);
    }

    private static void method17() {
        Stream.of("M2", "B9", "C4", "F16", "G8")
                .filter(word -> {
                    System.out.println("Now filtering " + word);
                    return true;
                }).map(word -> {
            System.out.println("Now mapping " + word);
            return word;
        })
                .forEach(word -> System.out.println("ForEach " + word));
    }

    private static void method18() {
        List<String> list = Arrays.asList("M52", "B91", "C44", "F16", "G8");
        boolean result = list.stream()
                .anyMatch(word -> word.length() % 2 == 0);
        System.out.println(result);

        List<Integer> listNum = Arrays.asList(30, 54, 12, 6, -10);
        boolean result2 = listNum.stream()
                .allMatch(number -> number % 2 == 0);

        System.out.println(result2);
    }

    private static void method19() {
        List<Integer> listOfAges = people.stream()
                .map(person -> person.getAge())
                .sorted()
                .collect(Collectors.toList());

        System.out.println(listOfAges);
    }

    private static void method20() {
        Map<Integer, List<Person>> map = people.stream()
                .collect(Collectors.groupingBy(person -> person.getAge()));

        System.out.println(map);
    }

    private static void method21() {
        String concatNames = people.stream()
                .map(person -> person.getName())
                .collect(Collectors.joining(","));

        System.out.println(concatNames);
    }

}
