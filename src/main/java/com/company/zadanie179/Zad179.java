package com.company.zadanie179;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.List;
import java.util.Scanner;

public class Zad179 {
    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://www.metaweather.com/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private static WeatherAPI api = retrofit.create(WeatherAPI.class);

    public static void main(String[] args) {
        // now crating scanner with name of the city
        Scanner s = new Scanner(System.in);
        System.out.println("Enter name of the City: ");
        String cityQuery = s.nextLine();


        Call<List<CitySearch>> call = api.searchCities(cityQuery);
        call.enqueue(getSearchCallback());
    }

    private static Callback<List<CitySearch>> getSearchCallback() {
        return new Callback<List<CitySearch>>() {
            @Override
            public void onResponse(Call<List<CitySearch>> call, Response<List<CitySearch>> response) {
                if (response.isSuccessful()) {
                    parseSearchData(response.body());
                }
            }


            @Override
            public void onFailure(Call<List<CitySearch>> call, Throwable throwable) {
                throwable.printStackTrace();
            }
        };
    }

    private static void parseSearchData(List<CitySearch> body) {

//        final int[] counter = {0};
//        body.forEach(city -> {
//            System.out.println(counter[0]++ + city.title);
//        });

        int counter = 1;
        for (CitySearch search : body) {
            System.out.println(counter++ + ". " + search.title);
        }

        Scanner s = new Scanner(System.in);
        System.out.println("Choose position number of the City: ");
        int select = s.nextInt();
        String cityId = body.get(select - 1).id;
        getCity(cityId);

    }

    private static void getCity(String cityId) {
        Call<City> cityCall = api.getCity(cityId);
        cityCall.enqueue(getCityCallback());
    }

    private static Callback<City> getCityCallback() {
        return new Callback<City>() {
            @Override
            public void onResponse(Call<City> call, Response<City> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    for (Day day : response.body().days) {
                        System.out.println(day.date + " " + day.temparture);
                    }
                }
                System.exit(0);

            }

            @Override
            public void onFailure(Call<City> call, Throwable throwable) {
                throwable.printStackTrace();
            }
        };
    }
}
