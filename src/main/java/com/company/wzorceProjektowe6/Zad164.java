package com.company.wzorceProjektowe6;

import java.util.Scanner;

public class Zad164 {
    public static void main(String[] args) {
        //create new instance of Weather class
        Weather weatherSystem = new Weather();

        Observer observer1 = new Observer("John", 0, -20);
        Observer observer2 = new Observer("Kate", 10, 0);
        Observer observer3 = new Observer("David", 20, 11);
        Observer observer4 = new Observer("Joanne", 27, 15);

        weatherSystem.addNewObservers(observer1, observer2, observer3, observer4);

        Scanner scanner = new Scanner(System.in);
        for (; ; ) {
            System.out.println("Set temperature: ");
            int setTemp = scanner.nextInt();
            weatherSystem.updateTemp(setTemp);
        }

    }
}
