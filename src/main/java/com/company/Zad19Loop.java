package com.company;

//*Utwórz metodę, w której pętlą (typu `FOR`) wyświetlisz wszystkie liczby od `0` do `18` (włącznie).
//>`0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18`

public class Zad19Loop {
    public static void main(String[] args) {
        showNum();
    }
    static void showNum () {
        for (int i = 0; i <= 18; i++) {
            System.out.print(i + ",");
        }
    }
}
