package com.company;

//Utwórz metodę która przyjmuje niepustą liste lub tablicę a następnie w *nieskończonej* pętli je wyświetla (zaczynając od pozycji `0`).
// Pętla ma zostać przerwana po wyjściu poza zakres listy (tzn. po przekroczeniu jej rozmiaru).
public class Zad127 {
    public static void main(String[] args) {
    int [] array = new int [] {5,6,7,8,9,10,11,12,7};
    showArray(array);
    }

    private static void showArray(int[] array) {
        int index = 0;
        while (true) {
            try {
                System.out.println(array[index++]);
            } catch (ArrayIndexOutOfBoundsException e) {
                //e.printStackTrace();
                System.out.printf("End of array reached at position %s", index);
                break;
            }
        }
    }
}
