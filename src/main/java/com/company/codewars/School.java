package com.company.codewars;

import java.util.Arrays;

public class School {
    public static void main(String[] args) {
        int[]marks={2,4,5,3,3,3,2,5,5};
        System.out.println(getAverage(marks));

    }
    public static int getAverage(int[] marks){
        return Arrays.stream(marks).sum()/marks.length;
    }
}
