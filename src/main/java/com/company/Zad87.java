package com.company;

// Utwórz metodę, która przyjmuje dwa parametry - pierwszy jest ciągiem znaków (typu `String`),
// a drugi jest poszukiwaną literą (jako `char`).
// Metoda ma zwrócić informację ile razy występuje podana litera (jako `int`).
public class Zad87 {
    public static void main(String[] args) {
        System.out.println(howManyTimes("Szerszeń jest super osą", 's'));
        System.out.println(howManyTimes("This shit is SERIOUS", 'i'));

    }

    private static int howManyTimes(String series, char letter) {
        int numOfDoubles = 0;
        // how many times it will occur
        for (int position = 0; position < series.length(); position++) {
            if (series.toLowerCase().charAt(position) == letter) {
                //incrementation
                numOfDoubles++;
            }
        }

        return numOfDoubles;
    }
}
