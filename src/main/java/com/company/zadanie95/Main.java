package com.company.zadanie95;
// Utwórz typ wyliczeniowy `TypWaluty` zawierająca opcje `USD`, `EUR`, `PLN` wraz z opisem (`Dolar`, `Euro`, `Polski Złoty`).
// Utwórz klasę `Waluta`, gdzie jednym z pól będzie ten typ (oraz pola `kursSprzedazy`, `kursKupna`).
public class Main {
    public static void main(String[] args) {
        CurrencyEx curr = new CurrencyEx(3.77,3.69,Currency.EUR);
        System.out.println(curr);
        curr.showProfit(1000);
        System.out.println();
        System.out.println(Currency.EUR.getDescription());
        System.out.print(Currency.EUR.getCountry());
    }
}
