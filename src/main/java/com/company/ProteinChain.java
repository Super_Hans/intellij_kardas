package com.company;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ProteinChain {
    public static void main(String[] args) {
        System.out.println(czyMoznaZamienic("ABCDEF", "ACBDMF"));
        System.out.println(changePossible("ABCDEF", "FEDCBA"));
    }

    private static boolean czyMoznaZamienic(String chain1, String chain2) {
        if (chain1.length() != chain2.length()) {
            return false;
        }
        // declaration of collection; if add to the enc - ArrayList should be used
        List<Integer> listPozycji = new LinkedList<>();
        //decl of two new char arrays
        char[] chainArr1 = chain1.toCharArray();
        char[] chainArr2 = chain2.toCharArray();
        //
        for (int i = 0; i < chainArr1.length; i++) {
            if (chainArr1[i] != chainArr2[i]) {

                listPozycji.add(i);
                if (listPozycji.size() > 2) {
                    return false;
                }
            }
        }
        if (listPozycji.size() == 1) {
            return false;
        }
        // initialize new char
        char bufor = chainArr2[listPozycji.get(0)];
        // we change values of arrays at certain positions
        chainArr2[listPozycji.get(0)] = chainArr2[listPozycji.get(1)];
        chainArr2[listPozycji.get(1)] = bufor;

        return Arrays.equals(chainArr1, chainArr2);
    }

    private static boolean changePossible(String chainA, String chainB) {
        char[] a = chainA.toCharArray();
        char[] b = chainB.toCharArray();
        Arrays.sort(a);
        Arrays.sort(b);
        return Arrays.equals(a, b);
    }
}
