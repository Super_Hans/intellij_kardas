package com.company.codewars;

public class Codewars23 {
    public static void main(String[] args) {
        System.out.println(numberOfDivisors(5000));
    }

    public static long numberOfDivisors(int n) {
        // TO DO please write your code below this comment
        long liczbaDzielnikow = 0;
        for (int i = 1; i <= n; i++) {
            if (n % i == 0) {
                liczbaDzielnikow++;
            }
        }

        return liczbaDzielnikow;
    }

}
