package com.company.codewars;


public class Codewars29Jaden {
    public static void main(String[] args) {
        System.out.println(toJadenCase(null));
    }

    static String toJadenCase(String phrase) {
        /**FIRST check if this is null and THEN check, if has value at all!!! */
        if (phrase == null || phrase.isEmpty()) {

            return null;
        }
        //use StringBuilder to create new String
        StringBuilder result = new StringBuilder();
        String[] splitString = phrase.split(" ");
        for (String target : splitString) {
            result.append(Character.toUpperCase(target.charAt(0))).append(target.substring(1)).append(" ");
        }
        return String.valueOf(result).trim();
    }

}
