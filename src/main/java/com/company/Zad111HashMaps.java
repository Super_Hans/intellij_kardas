package com.company;

import com.sun.javafx.collections.MappingChange;

import java.util.HashMap;
import java.util.Map;

//Utwórz metodę, która przyjmuje jeden parametr - mapę w postaci `String : String`.
// Metoda ma wyświetlić zawartość przekazanej mapy (w postaci `"PL" -> "Polska"`)
public class Zad111HashMaps {
    public static void main(String[] args) {
        showMap(getMap());
        showMap2(getMap());

    }

    private static Map<String, String> getMap() {
        Map<String, String> mappe = new HashMap<>();
        mappe.put("PL", "Poland");
        mappe.put("DE", "Germany");
        mappe.put("IE", "Ireland");

        return mappe;
    }

    private static void showMap(Map<String, String> mappe) {
        for (String keys : mappe.keySet()) {
            System.out.println(keys + " : " + mappe.get(keys));
        }
    }

    private static void showMap2(Map<String, String> mappe) {
        // getting set of pairs via entrySet
        for (Map.Entry<String, String> stringEntry : mappe.entrySet()) {
            System.out.println(stringEntry.getKey() + " -> " + stringEntry.getValue());
        }
    }
    /*private static void showMap3 (Map<String,String>mappe) {
        for (String value : mappe.values()) {
            You cannot really get pairs from values
        }*/

}
