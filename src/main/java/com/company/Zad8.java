package com.company;

/*Utwórz metodę, która zwróci trzecią potęgę przekazywanej liczby.
>Dla `2` zwróci `8` (bo `2^3 = 2 * 2 * 2 = 8`)
>
>a dla `3` zwróci `27` (bo `3^3 = 3 * 3 * 3 = 27`)
*/
public class Zad8 {
    public static void main(String[] args) {
        System.out.println(power(2));
        System.out.println(power(3));
    }

    static int power(int x) {
        System.out.println(x);
        return x * x * x;
    }
}
