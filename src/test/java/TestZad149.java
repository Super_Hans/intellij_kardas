import com.company.zadanie149.Zadanie149;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TestZad149 {
    private Zadanie149 zadanie149 = new Zadanie149();

    @Test
    void checkIfNumInRange () {
        boolean result = zadanie149.doesNumIsInRange(100,200,150);
        Assertions.assertTrue(result);
    }

    @Test
    void checkIfStartInRange () {
        boolean result = zadanie149.doesNumIsInRange(100,200,50);
        Assertions.assertFalse(result);
    }
    @Test
    void checkIfStopInRange () {
        boolean result = zadanie149.doesNumIsInRange(100,200,250);
        Assertions.assertFalse(result);
    }
    @Test
    void checkIfRangeOK () {
        boolean result = zadanie149.doesNumIsInRange(220, 200, 210);
        Assertions.assertFalse(result);
    }

    @Test
    void checkIfStartSameAsStop () {
        boolean result = zadanie149.doesNumIsInRange(200,200,250);
        Assertions.assertFalse(result);
    }
    @Test
    void checkIfAllTheSame () {
        boolean result = zadanie149.doesNumIsInRange(200,200,200);
        Assertions.assertTrue(result);
    }
}
