package com.company.heritance2;

public class Teacher extends Person {
    private MasterDegree degree;

    public Teacher(String name, int age, Sex sex, MasterDegree degree) {
        super(name, age, sex);
    }
    enum MasterDegree {
        DR, PROF, MGR
    }
}
