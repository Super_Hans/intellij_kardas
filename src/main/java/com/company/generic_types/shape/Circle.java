package com.company.generic_types.shape;

public class Circle implements Shape {
    @Override
    public String getName() {
        return "Circle";
    }
}
