package com.company.codewars;

public class Bumps {
    public static void main(String[] args) {
        String road = "n___nnn__n_n_n_n_n___n_nnn_nnn_nnnn_n_n";
        System.out.println(bumpsSolution(road));
        String road2 = "n___nn__n__n_n_n_n_______________n_n_n";
        System.out.println(bumps(road2));
    }

    static String bumps(final String road) {
        String[] roadArray = road.split("");
        int bumpCounter = 0;

        for (String bump : roadArray) {
            if (bump.equals("n")) {
                bumpCounter++;
            }
        }

        return bumpCounter > 15 ? "Car Dead" : "Woohoo!";
    }

    static String bumpsSolution(final String road) {
        return road.chars().filter(ch -> ch =='n').count()>15? "Car Dead" : "Woohoo!";
    }
}
