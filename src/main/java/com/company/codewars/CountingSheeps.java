package com.company.codewars;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.lang.String.*;

public class CountingSheeps {
    public static void main(String[] args) {
        System.out.println(countingSheep(4));

    }

    static String countingSheep(int num) {
        return IntStream
                .rangeClosed(1, num)
                .mapToObj(numer -> valueOf(numer) + " sheep...")
                .collect(Collectors.joining());
    }

}
