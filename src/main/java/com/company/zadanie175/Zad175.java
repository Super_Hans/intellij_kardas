package com.company.zadanie175;

public class Zad175 {
    public static void main(String[] args) {
       method1();
    }

    private static void method1() {
        Account account = new Account(11101,"Mateuszek",2300);
        final double howManyTimesWithdraw = 1000;
        final double amount = 30;

        Thread deposittee = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i <howManyTimesWithdraw ; i++) {
                    account.deposit(amount);
                    System.out.println(account);
                }
            }
        });

        Thread withdrawee = new Thread((new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < howManyTimesWithdraw; i++) {
                    account.withdraw(amount);
                    System.out.println(account);
                }

            }
        }));

        System.out.println(account);

        deposittee.start();
        withdrawee.start();

        try {
            deposittee.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            withdrawee.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(account);

    }
}
