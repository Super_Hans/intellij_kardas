package com.company.heritance;

public class Worker extends Person {
    private int salary;


    Worker(String name, String surname, int age, int salary) {
        super(name, surname, age);
        this.salary = salary;
    }

    @Override
    // we override two methods, basis method is override by toString
    String getNameAndSurnameOfAPerson() {
        return super.getNameAndSurnameOfAPerson() + " his salary is " + salary + " PLN";
    }

    @Override
    public String toString() {
        return getNameAndSurnameOfAPerson() + " his salary is " + salary;
    }

    public int showMonthlySalary (int monthCount) {
        return monthCount*salary;
    }
}
