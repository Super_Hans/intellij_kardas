package com.company;
/*Utwórz metodę, która przyjmuje wartość produktu oraz procent podatku jaki należy naliczyć, a następnie zwraca kwotę po opodatkowaniu.
> Dla przekazanych parametrów `100, 23`,
>
>powinno zwrócić `123`
*/

public class Zad9 {
    public static void main(String[] args) {
        double price = value(100,23);
        System.out.println(price);

        double price2 = value(12,23);
        System.out.println(price2);

        double price3 = value(399,23);
        System.out.println(price3);
    }
    static double value (int x, int tax){

        return x + x * (tax * 0.01);
    }
}
