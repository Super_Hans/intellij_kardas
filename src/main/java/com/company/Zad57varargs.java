package com.company;
//**ZADANIE #57*
//Utwórz metodę, która wykorzystuje mechanizm *varargs* by przekazać do metody dowolną,
// większą od zera, liczbę elementów typu `int` i zwrócić ich sumę.
// int METODA (String "imie", int... liczby) {
// int sume = liczby[0]   np. METODA (1,2,-11,9,0)
public class Zad57varargs {
    public static void main(String[] args) {
        System.out.println(returnSum(1,2,-11,5,-3,17));
    }
    public static int returnSum (int... numbers){ //tylko 1 taki parametr i musi być na końcu!!!
        int sume = 0;
      //  for (int i = 0; i < numbers.length ; i++) {
      //      sume += numbers[i];
        for (int number : numbers) {
            sume += number;
        }
        return sume;
    }
}
