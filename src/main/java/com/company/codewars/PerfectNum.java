package com.company.codewars;

import java.util.Arrays;
import java.util.stream.IntStream;

public class PerfectNum {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(extraPerfect(7)));
        System.out.println(Arrays.toString(extraPerfect(15)));
        System.out.println(Arrays.toString(extraPerfect(58)));

    }

    private static int[] extraPerfect(int number) {
        return IntStream.rangeClosed(1, number)
                .filter(num -> num % 2 == 1)
                .toArray();
    }
}
