package com.company.codewars;

public class StringUtils {
    public static void main(String[] args) {
        System.out.println(toAlternativeString("Happy Happy JoY JoY"));

    }

    static String toAlternativeString(String string) {
        char[] chars = string.toCharArray();

        for (int ch = 0; ch < chars.length; ch++) {
            if (Character.isUpperCase(chars[ch])) {
                chars[ch] = Character.toLowerCase(chars[ch]);
            } else if (Character.isLowerCase(chars[ch])) {
                chars[ch] = Character.toUpperCase(chars[ch]);
            }
        }
        return new String(chars);
    }
}
