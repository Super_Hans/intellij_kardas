package com.company.filesreading5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

//*ZADANIE #137*
//Utwórz metodę która odczytuje zawartości ze Scannera (do momentu podania pustej linii) i zapisuje wartości do pliku
public class Zad137 {
    public static void main(String[] args) {
        try {
            inputToFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void inputToFile() throws IOException {
        Scanner scan = new Scanner(System.in);
        FileWriter writer = new FileWriter("files/zadanko137.txt");
        BufferedWriter buffWriter = new BufferedWriter(writer);

        while (true) {
            System.out.println("Enter something please ");
            String line = scan.nextLine();
            if (line.isEmpty()) {
                break;
            } else {
                buffWriter.write(line);
                buffWriter.newLine();
                // alternative: buffWriter.write("\n")
            }
        }
        // put elements in the file - fast uploading
        buffWriter.flush();
        //closure of writing
        buffWriter.close();
        System.out.println("Writing ended");
    }
}
