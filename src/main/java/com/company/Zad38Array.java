package com.company;

import java.util.Arrays;

//**ZADANIE #38*
//Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca *NAJWIĘKSZĄ* liczbę
public class Zad38Array {
    public static void main(String[] args) {
        double [] arr = new double [] {13.25, 12.0, 7.50};
        int [] arr2 = new int [] {13,25, 12,0, 7, -50};
        System.out.println(zwrocMax(arr));
        System.out.println(zwrocMax2(arr));
        System.out.println(min(arr2));
        System.out.println(max(arr2));
    }
    public static double zwrocMax (double [] array) {
        double max = array[0];
        for (double element : array) {
            if (element > max) {
                max = element;
            }
        }
        return max;
    }
    public static double zwrocMax2 (double [] array) {
        double max = array [0];
        for (int i = 1; i < array.length ; i++) {
            if (array[i]> max) {
                max = array[i];

            }

        }
        return max;
    }
    public static int min(int[] list) {
        return Arrays.stream(list).min().getAsInt();  //getter
    }

    public static int max(int[] list) {
        return Arrays.stream(list).max().getAsInt();  // metoda z Codewars solutions
    }

}
