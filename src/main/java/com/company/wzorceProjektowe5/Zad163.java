package com.company.wzorceProjektowe5;

public class Zad163 {
    public static void main(String[] args) {
    Calculator calculator = new Calculator();
    calculator.setCurrentStrategy(new SumStrategy());
        System.out.println(calculator.calculate(10, 20));

        calculator.setCurrentStrategy(new DeductStrategy());
        System.out.println(calculator.calculate(367,200));

        /** nice use of singleton */
        calculator.setCurrentStrategy(MultiplyStrategy.getInstance());
        System.out.println(calculator.calculate(20,43));

        calculator.setCurrentStrategy(new DivideStrategy());
        System.out.println(calculator.calculate(21,6));
        System.out.println(calculator.calculate(789,13));
    }
}
