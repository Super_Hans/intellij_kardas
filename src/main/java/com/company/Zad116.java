package com.company;

import java.util.HashMap;
import java.util.Map;

public class Zad116 {
    public static void main(String[] args) {
        Map<String, Integer> mappa = new HashMap<>();
        mappa.put("PL", 966);
        mappa.put("MAZ", 966);
        mappa.put("DL", 966);
        mappa.put("GER", 1090);
        mappa.put("VA", 450);
        mappa.put("RUM", 1309);
        mappa.put("SLO", 966);
        System.out.println(howManyTimes(mappa, 966));

    }

    private static int howManyTimes(Map<String, Integer> map, int number) {
        int valueCount = 0;
        for (Integer value : map.values()) {
            if (value == number) {
                valueCount++;

            }
        }
        return valueCount;
    }

}
