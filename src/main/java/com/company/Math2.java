package com.company;

public class Math2 {
    public static void main(String[] args) {
        int a = 99;
        int b = 2;
        int c = 37;

        a = ++b * 2;
        c = a++ + b;
        b = ++a + a-- - c++ - --c;

        System.out.println(c);
// a == 7
// b == 8 + 8 - 9 - 9 = -2
// c == 9
    }
}
