package com.company;

/* *ZADANIE #36*
 Utwórz metodę, do której przekazujesz dwa parametry następnie wyświetlasz wszystkie liczby z podanego przedziału *I ICH DZIELNIKI*

 Dane wyświetl w formie:
 > dla `0, 6` wyświetli:
 >
 ```1  <--  1
 2  <--  1, 2,
 3  <--  1, 3,
 4  <--  1, 2, 4,
 5  <--  1, 5,
 6  <--  1, 2, 3, 6```*/
public class Zad36 {
    public static void main(String[] args) {
        przekazParametry(0, 6);
    }

    static void przekazParametry(int pParametr, int drParametr) {
        for (int liczba = pParametr; liczba <= drParametr; liczba++) {
            System.out.print(liczba + " <-- ");
            for (int dzielnik = 1; dzielnik <= liczba; dzielnik++) {
                if (liczba % dzielnik == 0) {
                    System.out.print(dzielnik + " ");

                }
            }
            System.out.println();

        }
    }
}
