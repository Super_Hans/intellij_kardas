package com.company.codewars;
//s="aaabbbbhaijjjm"
//error_printer(s) => "0/14"
//s="aaaxbbbbyyhwawiwjjjwwm"
//error_printer(s) => "8/22"
public class CodewarsPrinterErr {
    public static void main(String[] args) {
        System.out.println(printerError("aaaaaaaaaaaaaabccccccccdz"));

    }
    public static String printerError(String s) {
        String[] array = s.split("");
        int errCount = 0;
        for (int i = 0; i < array.length; i++) {
            if (s.charAt(i) > 'm') {
                ++errCount;
            }
        }
        return String.valueOf(errCount)
                +"/"
                + String.valueOf(s.length());
    }
}
