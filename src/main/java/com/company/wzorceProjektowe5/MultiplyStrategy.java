package com.company.wzorceProjektowe5;

public class MultiplyStrategy implements Strategy {
    private static MultiplyStrategy instance = null;

    private MultiplyStrategy() {
    }

    static MultiplyStrategy getInstance() {
        if (instance == null) {
            instance = new MultiplyStrategy();
        }
        return instance;
    }

    @Override
    public double calculate(double number1, double number2) {
        return number1 * number2;
    }

    @Override
    public String nameOfOps() {
        return "multiplying";
    }
}
