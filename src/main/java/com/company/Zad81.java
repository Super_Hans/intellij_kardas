package com.company;

//*ZADANIE #81*
//Utwórz metodę, która przyjmuje zdanie
// (np. `"Ala ma kota."`, `"Dziś jest sobota"`),
// a następnie ma zwrócić sumę długości *wszystkich* wyrazów.
public class Zad81 {
    public static void main(String[] args) {
        System.out.println(sumaDlugWyrazow("Przemek ma psa"));
        System.out.println(sumaDlugWyrazow2("Przemek ma psa"));
    }

    public static int sumaDlugWyrazow(String zdanie) {
        int suma = 0;
        String[] tablica = zdanie.split(" ");
        for (String wyraz : tablica) {
            suma += wyraz.length();
        }
        return suma;
    }

    public static int sumaDlugWyrazow2(String zdanie) {

        return zdanie.replace(" ", "").length();
        //można tak to zrobić - Spamiętaj! usunęliśmy spacje i zwracamy dlugosc zdania bez spacji
    }
}
