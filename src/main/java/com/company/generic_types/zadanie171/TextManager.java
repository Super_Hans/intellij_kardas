package com.company.generic_types.zadanie171;

public class TextManager implements AutoCloseable {
// restriction! AutoCloseable must be implemented
    TextManager() {
        System.out.println("====Beginning of line====");
    }

    @Override
    public void close() throws Exception {
        System.out.println("====End of line==========");
    }
    void show (String text) {
        System.out.println(text);
    }
}
