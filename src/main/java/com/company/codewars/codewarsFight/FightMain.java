package com.company.codewars.codewarsFight;

public class FightMain {
    public static void main(String[] args) {
        Fighter nabojka1 = new Fighter("Jerry_RTS", 1910, 3);
        Fighter nabojka2 = new Fighter("Harry_L", 705, 5);
        System.out.println(declareWinner(nabojka1, nabojka2, "Jerry_RTS"));

    }

    static String declareWinner(Fighter nabojka1, Fighter nabojka2, String firstAttack) {
        Fighter first;
        Fighter second;

        if (firstAttack.equals(nabojka1.name)) {
            first = nabojka1;
            second = nabojka2;

        } else {
            first = nabojka2;
            second = nabojka1;
        }
        while (true) {
            second.health -= first.damagePerAttack;
            if (second.health <= 0) {
                return first.name;
            }
            first.health -= second.damagePerAttack;
            if (first.health <= 0) {
                return second.name;
//
//            }
//        }
//    }
//        while (first.health > 0 && second.health > 0) {
//            second.health -= first.damagePerAttack;
//            first.health -= second.damagePerAttack;
//        }
//        return second.health > 0 ? first.name : second.name;
//    }
            }
        }
    }
}