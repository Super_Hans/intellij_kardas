package com.company.generic_types.simple_generics;
// Another thing that we can put into class signature < >
//T - type
//K - key
//V - value
public class FruitBox <TYPE> {

    private TYPE fruit;

    public FruitBox(TYPE fruit){
        this.fruit = fruit;
    }

    public TYPE getFruit(){
        return fruit;
    }

    public void setFruit(TYPE fruit) {
        this.fruit = fruit;
    }
}
