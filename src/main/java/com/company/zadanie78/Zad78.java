package com.company.zadanie78;

//*Utwórz klasę `Rectangle` (`Prostokąt`) posiadającą
//>*pola reprezentujące:*
//>* dłuższy bok
//>* krótszy bok
//>
//>*konstruktory oraz metody służące do:*
//>* policzenia obwodu
//>* policzenia pola powierzchni
//>* porównania czy pola powierzchni dwóch przekazanych prostokątów są takie same (zwracająca wartość `boolean`)
//>
//>*Utwórz tablicę zawierającą 5 obiektów tej klasy i w pętli wyświetl zdania w formie*
//>`10 x 20 = 200`
public class Zad78 {
    public static void main(String[] args) {
        Rectangle prostokatNowy = new Rectangle(6, 2);
        System.out.println(prostokatNowy.policzObwod());
        System.out.println(prostokatNowy.policzPole());
        System.out.println(prostokatNowy.getKrótszyBok());
        // prostokatNowy.setDłuższyBok(20);
        //System.out.println(prostokatNowy.getDłuższyBok());

        Rectangle prostokat2 = new Rectangle(4, 2);
        System.out.println(Rectangle.czyPolaTeSame(prostokatNowy, prostokat2));
    }
}
