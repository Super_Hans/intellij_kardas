package com.company.homework3;

public class SquareRoots {
    public static void main(String[] args) {
        int x = 6000, y = 7000;
        System.out.println(findNumberOfSqrRoots(x, y));
    }

    static int findNumberOfSqrRoots(int A, int B) {
        int rootCounter = 0;

        for (double i = A; i <= B; i++) {
            for (double j = 1; j * j <= i; j++) {
                if (j * j == i) {
                    rootCounter++;

                }
            }
        }

        return rootCounter;
    }
}
