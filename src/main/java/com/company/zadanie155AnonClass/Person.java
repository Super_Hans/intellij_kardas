package com.company.zadanie155AnonClass;

public class Person {
    private String name;
    private String surname;
    private int age;

    public Person(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    String introduceYourself(){
        return String.format("Hi! I'm %s %s and I'm %s years old",
                name,
                surname,
                age);
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }
}
