package com.company;
//Utwórz metodę, która wczytuje liczby od użytkownika do momentu podania `-1`
// a następnie zwraca ich listę
// (np. wykorzystując implementację `ArrayList`)

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Zadanie101 {
    private static final int END = -1;

    public static void main(String[] args) {
        List<Integer> list = returnList();
        System.out.println(listAverage(list));

    }

    static List<Integer> returnList() {
        List<Integer> listNew = new ArrayList<>();
        // static value
        Scanner s = new Scanner(System.in);
        int number;
        while (true) {
            System.out.print("Give number: ");
            /* s. is object of Class Scanner */
            number = s.nextInt();
            if (number == END) {
                break;
            }
            listNew.add(number);
        }
        return listNew;
    }

    static double listAverage(List<Integer> list) {
        double sum = 0.0;
        for (Integer integer : list) {
            sum += integer;

        }
        return sum / list.size();
    }
}