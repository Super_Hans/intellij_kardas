package com.company.interfejs2;

public class Person implements IntroduceYourself {
    private String name;
    private String surname;
    int age;
    private Gender gender;

    public Person(String name, String surname, int age, Gender gender) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.gender = gender;
    }

    @Override
    public String showDataFull() {
        return String.format("This is  %s %s, %s years old %s", name, surname, age, gender);
    }

    @Override
    public String getFullName() {
        return String.format("My name is %s", name);
    }

    @Override
    public int yearsToPension() {
        return (Gender.MAN == gender ? 67 : 65) - age;
    }

    enum Gender {
        MAN, WOMAN
    }

    @Override
    public String toString() {
        return showDataFull();
    }
}
