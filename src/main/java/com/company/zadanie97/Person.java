package com.company.zadanie97;

public class Person {
    private String name;
    private String surname;
    private int age;
    private Gender gender;

    public Person(String name, String surname, int age, Gender gender) {
        //violet = fields in class
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name '" + name + '\'' +
                ", surname '" + surname + '\'' +
                ", age " + age +
                ", gender " + gender +
                '}';
    }
}



