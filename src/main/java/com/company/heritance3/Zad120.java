package com.company.heritance3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//*ZADANIE #120*
//Utwórz klasę `Konto`, która zawiera pola `stan`, `numer`, `wlasciciel`
// oraz metody umożliwiające wpłatę, wypłatę, przelewanie środków, sprawdzenie stastus.
//
//Utwórz klasę `KontoOszczednosciowe`, które dodatkowo zawiera pole `procentOszczednosci`.
//Utworz klasę `KontoFirmowe`, które dodatkowo zawiera pole `oplataZaTransakcje`.
public class Zad120 {
    public static void main(String[] args) {
        Konto konto = new Konto("Mariusz");
        Konto konto1 = new Konto("Zofia");
        Konto konto2 = new Konto("Zachary");
        System.out.println(konto1);
        System.out.println(konto2);

        konto.wplataNaKonto(40.0);
        System.out.println(konto);

        boolean rezultat = konto.wyplataZKonta(50);
        if (rezultat) {
            System.out.println("Transakcja udana");
        } else {
            System.out.println("Transakcja nieudana");
        }
        System.out.println(konto);

        Konto.przelewanieSrodkow(konto, konto1, 60);
        System.out.println(konto);
        System.out.println(konto1);

        KontoFirmowe kontoFirmowe = new KontoFirmowe("Mariusz", 0.2);
        kontoFirmowe.wplataNaKonto(20);
        System.out.println(kontoFirmowe);
        kontoFirmowe.wyplataZKonta(10);
        System.out.println(kontoFirmowe);

        Konto.przelewanieSrodkow(kontoFirmowe, konto, 5);
        System.out.println(kontoFirmowe);

        List<Konto> listaKont = Arrays.asList(
                konto, konto1, konto2, kontoFirmowe
        );
        for (Konto k : listaKont) {
            //instance of -> check if this Object of this class
            if (k instanceof KontoFirmowe) {
                System.out.print("Konto Firmowe ");
            } else {
                System.out.print("Konto ");
            }
            System.out.println("(" + k.getNumerKonta() + ")" + k.pokazStanKonta());
        }
    }
}
