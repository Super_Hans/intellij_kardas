package com.company.zad154;

public class Computer {
    void start() {
        preparation();
        System.out.println("Computer is working");
    }

    protected void preparation() {
        System.out.println("Loading system...");
    }
}
