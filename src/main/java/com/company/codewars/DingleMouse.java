package com.company.codewars;

public class DingleMouse {
    //creating inner class
    private static class Point<FIRST, SECOND> {
        FIRST x;
        SECOND y;

        Point(FIRST x, SECOND y) {
            this.x = x;
            this.y = y;
        }
    }

    private final static char[][] keyboard = {
            {'a', 'b', 'c', 'd', 'e', '1', '2', '3'},
            {'f', 'g', 'h', 'i', 'j', '4', '5', '6'},
            {'k', 'l', 'm', 'n', 'o', '7', '8', '9'},
            {'p', 'q', 'r', 's', 't', '.', '@', '0'},
            {'u', 'v', 'w', 'x', 'y', 'z', '_', '/'}};

    public static void main(String[] args) {
        System.out.println(tvRemote("your"));
        System.out.println(tvRemote("work"));
        System.out.println(tvRemote("code_wars"));

    }

    private static int tvRemote(final String word) {
        // we have to accept must-be-done clicks;
        int counter = word.length();
        // decl-init of generic classes
        Point<Integer, Integer> startingCoOrdinates = new Point<>(0, 0);
        Point<Integer, Integer> finalCoOrdinates;

        for (int i = 0; i < word.length(); i++) {
            finalCoOrdinates = coordinatesOfLetter(word.charAt(i));
//            System.out.println(startingCoOrdinates+ " --> " + finalCoOrdinates);
//            System.out.println(coordinatesOfLetter(word.charAt(i)));
            assert finalCoOrdinates != null;
            counter += differenceBetweenCoordinates(startingCoOrdinates,
                    finalCoOrdinates);
            startingCoOrdinates = finalCoOrdinates;

        }

        return counter;
    }

    private static Point<Integer, Integer> coordinatesOfLetter(char letter) {
        for (int high = 0; high < keyboard.length; high++) {
            for (int length = 0; length < keyboard[high].length; length++) {
                if (keyboard[high][length] == letter) {
                    //returns Object of our newly created class
                    return new Point<>(high, length);
                }
            }
        }
        return null;
    }

    private static int differenceBetweenCoordinates(Point<Integer, Integer> begin,
                                                    Point<Integer, Integer> end) {
        int height = Math.abs(begin.x - end.x);
        int length = Math.abs(begin.y - end.y);

        return height + length;
    }

}
