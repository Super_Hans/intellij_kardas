package com.company.codewars;

import java.util.Arrays;
import java.util.stream.Collectors;

public class SPinWars {
    public static void main(String[] args) {
        System.out.println(spinWords("Another one bites the dust"));
    }

    public static String spinWords(String sentence) {
        String separator = " ";
        String[] sentenceArr = sentence.split(separator);

        return Arrays.stream(sentenceArr)
                .map(word -> {
                    if (word.length() > 4) {
                        return new StringBuilder(word).reverse().toString();
//                        String newWord = "";
//                        for (char s : word.toCharArray()) {
//                            newWord = s + newWord;
//
//                        }
//                        return newWord;
//                    } else {
//                        return word;
                    }
                    return word;
                }).collect(Collectors.joining(separator));
    }
}
