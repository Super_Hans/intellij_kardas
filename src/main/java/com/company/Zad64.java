package com.company;

import java.util.Arrays;

//**ZADANIE #64 - EXTRA*
//Utwórz metodę, która przyjmuje tablicę liczb i zwraca rozmiar tablicy po usunięciu duplikatów
//> Dla `[-7, 8, 8, 0, 2, 7, 2]` (ma 7 elementów)
//> zwróci `5` (bo  tablica bez duplikatów to `[-7, 8, 0, 2, 7]`)
public class Zad64 {
    public static void main(String[] args) {
        int[]tablica = new int []{1,1,1,2,1,1,1};
        System.out.println(zwrocBezDup(tablica));
    }
    public static int zwrocBezDup (int [] tablica) {
        int koncowaDlugosc = tablica.length;

        for (int i = 0; i < koncowaDlugosc; i++) {
            for (int j = i + 1; j < koncowaDlugosc; j++) {
                if (tablica[i] == tablica[j]) {
                    int przesunWLewo = j;
                    for (int k = j+1; k < koncowaDlugosc; k++, przesunWLewo++) {
                        tablica[przesunWLewo] = tablica[k];
                    }
                    koncowaDlugosc--;
                    j--;
                }
            }
        }

      //  int[] whitelist = new int[koncowaDlugosc];
        // for(int i = 0; i < koncowaDlugosc; i++){
            //whitelist[i] = tablica[i];
       // }
        return koncowaDlugosc;
    }
}
