package com.company.zadanie174;

public class ThreadOfUs extends Thread {
    @Override
    public void run() {
        System.out.printf("Thread is starting with %s: ",
                ThreadOfUs.currentThread().getId());
        for (int i = 0; i < 20; i++) {
            System.out.printf("%s. " +
                    " ID " +
                    Thread.currentThread().getId(), i);

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.printf("Thread has finished with %s\n",
                ThreadOfUs.currentThread().getId());
    }
}
