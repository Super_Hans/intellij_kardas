package com.company;

import java.util.Arrays;

//**ZADANIE #44*
//Utwórz metodę, która przyjmuje trzy parametry - tablicę oraz dwie liczby.
// Metoda ma zwrócić nową tablicę do której na wybranej pozycji
// (podanej jako drugi parametr) wstawi nowy element (podany jako trzeci parametr).
//> Dla `([1, 2, 3, 4, 5], 2, 77)`
//> powinno zwrócić `[1, 2, 77, 3, 4, 5]`
public class Zad44Array {
    public static void main(String[] args) {
        int tablicaNowa [] = {1,2,3,6,9};
        System.out.println(Arrays.toString(
                wstawElementNaPozycje(tablicaNowa, 2, 68)));
    }
    static int [] wstawElementNaPozycje (int [] tabliczka, int pozycja, int nowyElement ) {
        int [] tablicaWyjscia = new int [tabliczka.length + 1];

        for (int index = 0; index < pozycja ; index++) {
            tablicaWyjscia[index] = tabliczka[index];
        }

        tablicaWyjscia[pozycja] = nowyElement;

        for (int i = pozycja +1; i < tablicaWyjscia.length  ; i++) {
            tablicaWyjscia[i] = tabliczka[i-1];
        }
        return tablicaWyjscia;
    }
}
