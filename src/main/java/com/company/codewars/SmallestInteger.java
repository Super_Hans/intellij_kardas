package com.company.codewars;

import java.util.Arrays;

public class SmallestInteger {
    public static void main(String[] args) {
        int[] array = {1, 2, -3, 4, -5};
        System.out.println(findSmallestInt(array));
        System.out.println(findSmallestInt2(array));
    }

    static int findSmallestInt(int[] args) {
        int min = args[0];
        for (int arg : args) {
            if (arg < min) {
                min = arg;
            }
        }
        return min;
    }

    static int findSmallestInt2(int[] args) {
        if(args!=null) {
            return Arrays.stream(args).min().getAsInt();
        }else {
            return 0;
        }
    }
}
