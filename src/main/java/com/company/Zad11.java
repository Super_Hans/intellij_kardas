package com.company;

public class Zad11 {
    public static void main(String[] args) {
        System.out.println (welcome("Maciej",88, false));

    }

    static String welcome(String name, int age, boolean isMan) {
        String wynik = "Cześć! Jestem " + name + " mam " + age + " lat i";
        if (isMan) {
            wynik += " jestem mężczyzną";
        } else {
            wynik += " jestem kobietą";
        }

        return wynik;
    }
}
