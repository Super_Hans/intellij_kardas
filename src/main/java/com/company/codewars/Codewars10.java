package com.company;

import java.util.Arrays;

public class Codewars10 {
    public static void main(String[] args) {
        Boolean [] arrayOfSheeps = new Boolean []{true,  true,  true,  false,
                true,  true,  true,  null ,
                true,  false, true,  false,
                true,  false, false, true ,
                true,  true,  true,  true ,
                false, false, true,  true};
        System.out.println(countSheeps(arrayOfSheeps));
    }

    public static int countSheeps(Boolean[] arrayOfSheeps) {
        int counter = 0;
        for (int sheep = 0; sheep < arrayOfSheeps.length ; sheep++) {
            if (arrayOfSheeps[sheep]!=null && arrayOfSheeps[sheep] == true ) {
                counter++;
            }
        }
        return counter;
    }
}