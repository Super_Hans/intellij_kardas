package com.company;

public class Zad13 {
    public static void main(String[] args) {
        System.out.println(retHigh(789, 4, 93));
        System.out.println(retHigh(34, 409, 87));
        System.out.println(retHigh(12, 59, 93));
    }

    static int retHigh(int x, int y, int z) {
        if (x > y && x > z) {
            return x;
        } else if (y > x && y > z) {
            return y;
        } else {
            return z;
        }
    }
}
