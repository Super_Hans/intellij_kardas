package com.company;

import java.util.Arrays;

//**ZADANIE #52 - DO DOMU*
//Utwórz metodę, która przyjmuje dwie tablice.
//Metoda ma zwrócić tablicę z sumą kolumn podanych tablic.
// To znaczy element na pozycji `0` z pierwszej tablicy, dodajemy do elementu na pozycji `0` z drugiej tablicy.
//*Przyjęte założenia:*
//- Tablice mogą być różnych długości!
//- Liczby sumujemy jak w “dodawaniu pisemnym”,
// tzn. gdy wartość jest większa od `9` to `1` “idzie dalej”.
//>
//[1,  2,  3,  4]
//   [ 6,  7,  8]
//[1,  9,  1,  2]
public class Zad52Dom {
    public static void main(String[] args) {
        int[] taka = {1, 2, 3, 4};
        int[] owaka = {6, 7, 8};
        int[] insza = {1, 2, 3, 8};
        int[] czwartsza = {2, 5, 7, 6, 3};
        int[] rezultat = zwrocTabliceZsumowana(taka, owaka,
                insza, czwartsza);
        System.out.println(Arrays.toString(rezultat));

        int[] kolejnaTablica = usunPustePozycje(rezultat);
        System.out.println(Arrays.toString(kolejnaTablica));
    }

    static int[] usunPustePozycje(int[] tablica) {
        int licznik = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] == 0) {
                licznik++;
            } else {
                break;
            }
        }
        int[] kolejnaTablica = new int[tablica.length - licznik];
        for (int index = 0; index < kolejnaTablica.length; index++) {
            kolejnaTablica[index] = tablica[index + licznik];
        }
        return kolejnaTablica;
    }

    public static int[] zwrocTabliceZsumowana(int[] taka, int[] owaka,
                                              int[] insza, int[] czwartsza) {
        int[] tablicaIndeksowa = {taka.length - 1, owaka.length - 1, insza.length - 1, czwartsza.length - 1};
        int[] tablicaWynikowa = new int[10];
        for (int index = tablicaWynikowa.length - 1; index >= 0; index--) {
            int sume = tablicaWynikowa[index];
            if (tablicaIndeksowa[0] >= 0) {
                sume += taka[tablicaIndeksowa[0]];
                tablicaIndeksowa[0]--;
            }
            if (tablicaIndeksowa[1] >= 0) {
                sume += owaka[tablicaIndeksowa[1]];
                tablicaIndeksowa[1]--;
            }
            if (tablicaIndeksowa[2] >= 0) {
                sume += insza[tablicaIndeksowa[2]];
                tablicaIndeksowa[2]--;
            }
            if (tablicaIndeksowa[3] >= 0) {
                sume += czwartsza[tablicaIndeksowa[3]];
                tablicaIndeksowa[3]--;
            }
            if (sume > 9) {
                tablicaWynikowa[index] = sume % 10;
                tablicaWynikowa[index - 1] = sume / 10;
            } else {
                tablicaWynikowa[index] = sume;
            }
        }
        return tablicaWynikowa;

    }

}