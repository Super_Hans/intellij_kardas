package com.company;

//Utwórz metodę, która przyjmuje wartość produktu oraz procent podatku jaki należy naliczyć,
// a następnie zwraca kwotę po opodatkowaniu.
//
//> Dla przekazanych parametrów `100, 23`,
//>powinno zwrócić `123`
public class Zad146test {
    public static void main(String[] args) {
        System.out.println(amountAfterTax(100,23));
    }

    public static double amountAfterTax(double sum, double tax) {
        if (tax < 0 || tax > 100){
            return sum;
        }

        return sum * (tax / 100.0) + sum;
    }
}
