package com.company;

public class Zad145 {
    public static void main(String[] args) {
        //method1();
        //method2();
        //method3();
        //method4();
        //method5();
        method6();
    }

    private static void method1() {
        // 0,7 not applicable for float
        for (float i = 0; i < 1; i += 0.1f) {
            System.out.printf("%.2f and %s \n", i, i);
        }
    }

    private static void method2() {
        for (double i = 0; i < 1; i += 0.1d) {
            System.out.printf("%.2f and %s \n", i, i);
        }
    }

    private static void method3() {
//        0,80 and 0.7999999999999999
//        0,90 and 0.8999999999999999
//        1,00 and 0.9999999999999999
        System.out.println(1d == 0.9999999999999999);
        System.out.println(0.99f == 0.9999999999999999);
        System.out.println(0.70d == 0.70000005);
        System.out.println(0.70f == 0.70000005);
    }

    private static void method4() {
        for (float i = 10f; i > 0; i -= 0.1f) {
            System.out.println(i);
        }
    }
    private static void method5() {
        double sum = 0;
        for (int i = 0; i < 100000000 ; i++) {
            sum+=0.1;

        }
        System.out.println(sum);
    }
    private static void method6() {
        float x = 0.1f;
        double y = 0.1d;

        System.out.printf("Float is %.50f \n",x);
        System.out.printf("Double is %.50f \n",y);
        // problematic float -
        System.out.println(0.1f == 0.10000000149011612000000000000000000000000000000000
        );
    }

}
