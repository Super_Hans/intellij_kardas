package com.company.zad150;

//*ZADANIE #150*
//Utwórz metodę, która zwraca tablicę o podanym rozmiarze wypełnioną kolejnymi wartościami zaczynając od `10`:
//> Dla `4` zwróci: `[10, 11, 12, 13]`
//>
//> Dla `8` zwróci: `[10, 11, 12, 13, 14, 15, 16, 17]`
public class Zad150 {
    public int[] showArrayFromGivenIndex(int num) {
        int[] newArr = new int[num];
        for (int i = 0; i < num; i++) {
            newArr[i] = 10 + i;
        }
        return newArr;
    }
}
