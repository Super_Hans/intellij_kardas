package com.company.obiektowe;

public class Zad77OOP {
    public static void main(String[] args) {
         Osoba x1 = new Osoba("Paweł", "Bednarz");
        System.out.println(x1);
        Osoba y1 = new Osoba("Anna", "Marczak", 32, false);
        System.out.println(y1);
        y1.przedstawSię();
        if (y1.czyPelnoleni()) {
            System.out.println("Osoba jest pełnoletnia");

        } else {
            System.out.println("Osoba jest niepełnoletnia");
        }
    }
}
