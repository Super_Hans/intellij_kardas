package com.company.wzorceProjektowe7;

class DocFactory {

    Doc createDoc(String text, DocumentType type) {
        Doc document = null;

        switch (type) {
            case TXT:
                document = new TxtDocument(type, text);
                break;
            case HTML:
                document = new HtmlDocument(type, text);
                break;

        }
        return document;
    }

    enum DocumentType {
        TXT("txt"),
        HTML("html");

        private String extension;

        DocumentType(String extension) {
            this.extension = extension;
        }

        String getExtension() {
            return extension;
        }

    }
}
