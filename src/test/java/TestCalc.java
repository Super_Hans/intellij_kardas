import com.company.kalkulator.Calculator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TestCalc {
    Calculator calc = new Calculator();

    @Test
    void shouldSumOK() {
        Integer result = calc.summation(3, 8);
        Assertions.assertEquals(Integer.valueOf(11), result);

    }

    @Test
    void shouldFirstNumIsNull() {
        Integer result = calc.summation(null, 8);
        Assertions.assertEquals(Integer.valueOf(8), result);

    }

    @Test
    void shouldSecNumIsNull() {
        Integer result = calc.summation(3, null);
        Assertions.assertEquals(Integer.valueOf(3), result);

    }

    @Test
    void shouldBothAreNull() {
        Integer result = calc.summation(null, null);
        Assertions.assertNull(result);

    }

    @Test
    void shouldBothNumsAreMax() {
        Integer result = calc.summation(Integer.MAX_VALUE, Integer.MAX_VALUE);
        Assertions.assertEquals(Integer.valueOf(-2), result);
    }

    @Test
    void shouldCalcCalc() {
        Integer result = calc.sumOfManyNums(2, 4, 6, 8);
        Assertions.assertEquals(Integer.valueOf(20), result);
    }

    @Test
    void shouldCalcCalcWhenVarArgsEmpty() {
        Integer result = calc.sumOfManyNums(null);
        Assertions.assertNull(null);
    }

    @Test
    void shouldCalcCalcWhenArrayEmpty() {
        Integer result = calc.sumOfManyNums();
        Assertions.assertEquals(Integer.valueOf(0), result);
    }

    @Test
    void shouldDeductWork() {
        Integer result = calc.deducting(4, 39850);
        Assertions.assertEquals(Integer.valueOf(-39846), result);
    }

    @Test
    void shouldDeductWorkWithOneNull() {
        Integer result = calc.deducting(null, 3);
        Assertions.assertEquals(Integer.valueOf(-3), result);
    }

    @Test
    void shouldDeductWorkWithSecNull() {
        Integer result = calc.deducting(4, null);
        Assertions.assertEquals(Integer.valueOf(4), result);
    }

    @Test
    void shouldDeductWorkWithNull() {
        Integer result = calc.deducting(null, null);
        Assertions.assertNull(null);
    }
}
