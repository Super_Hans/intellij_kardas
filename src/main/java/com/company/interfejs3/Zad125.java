package com.company.interfejs3;

import java.util.ArrayList;
import java.util.List;

//*ZADANIE #125*
//Utwórz klasę `Pojazd` (z polami `nazwa`) a następnie dziedziczące klasy klasy
//- `Samochod` (z polami `liczbaKol` i `liczbaDrzwi`)
//- `Pociag` (z polami `liczbaWagonow`, `czyMaWagonBarowy`)
//
//Utwórz interfejs `CharakterystykaPojazdu`, zawierający metody `maxPredkosc()` oraz `liczbaPasazerow()`.
public class Zad125 {
    public static void main(String[] args) {


        Car car = new Car("Honda", 4, 2);
        Train train = new Train("Janusz", 10, false);

        car.showParameters(200, 4);
        System.out.println(car);

        train.showParameters(120, 250);
        System.out.println(train);
        // list of Objects that can implement interface
        List<VehicleSpecs> vehicleSpecsList = new ArrayList<>();
        vehicleSpecsList.add(car);vehicleSpecsList.add(train);

        for (VehicleSpecs specs : vehicleSpecsList) {
            System.out.println(specs.maxSpeed());
        }

    }
}
