package com.company;

//*Utwórz metodę, w której *wyświetlisz co drugą* liczbę z przedziału `0` do `14`
// (krańcowy zakres przedziału ma być drugim parametrem metody)
//> Dla `0, 4` wyświetli `0, 2, 4`
//>
//> Dla `6, 14` wyświetli `6, 8, 10, 12, 14`
//
public class Zad23Loop {
    public static void main(String[] args) {
        coDrugaLiczba(2, 32);
        System.out.println();
        war4(23,32);
    }

    static void coDrugaLiczba(int start, int stop) {
        for (int i = start; i <= stop; i = i + 2) {
            System.out.print(i + ",");
        }
    }

    static void war4(int start, int stop) {
        int i = start;
        do {
            System.out.print(i + ", ");
            i  += 2;
        }while (i <= stop);
    }
}
