package com.company.heritance3;

// calling constructor of Class Konto
public class KontoFirmowe extends Konto {
    private double oplataTransakcyjna;

    public KontoFirmowe(String wlasciciel, double oplataTransakcyjna) {
        super(wlasciciel);
        this.oplataTransakcyjna = oplataTransakcyjna;
    }

    @Override
    boolean wyplataZKonta(double wyplata) {
        double sumaPoOplacie = wyplata + wyplata * oplataTransakcyjna;
        if (stan >= sumaPoOplacie) {
            stan -= sumaPoOplacie;
            return true;
        }
        return false;
    }

}
