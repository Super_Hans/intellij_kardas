package com.company.zadanie96;

public enum Countrycode {
    POLAND(48), GERMANY(49), RUSSIA(7);
    private final int code;

    //from Integer to int --> UNBOXING, from int to INTEGER = autoboxing
    Countrycode(int code) {
        this.code = code;
    }

    String asText() {
        return "+" + code;
    }

    int asNum() {
        return code;
    }
}
