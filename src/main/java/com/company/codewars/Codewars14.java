package com.company;

public class Codewars14 {
    public static void main(String[] args) {
        int [] array = new int [] {32,51,8,-28};
        System.out.println(oddOrEven(array));
    }
    public static String oddOrEven (int[] array) {
        int sum = 0;// your code
        for (int i : array) {
            sum+= i;
        }
        if (sum%2 == 0) {
            return "even";
        }
        else
        {
            return "odd";
        }
    }
    //*return stream(array).sum() % 2 == 0 ? "even" : "odd";
    //    }  best practice!
}
