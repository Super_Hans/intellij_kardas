package com.company.interfejs;

public interface Calculations {
    double calcArea ();
    double calcVolume ();
    double calcCirc ();
}
