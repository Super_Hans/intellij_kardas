import com.company.zadanie148.SumElements;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class TestZad148 {
    private SumElements sumElements = new SumElements();

    @Test
    void shouldRangeOK (){
        int[] array = new int []{1,2,3,4,5,6};
        Integer result = sumElements.howManyElementsToSumUp(array,9);
        Assertions.assertEquals(Integer.valueOf(4),result);
    }

    @Test
    void shouldRangeOKIfNumHigher (){
        int[] array = new int []{1,2,3,4,5,6};
        Integer result = sumElements.howManyElementsToSumUp(array,99);
        Assertions.assertNull(null);
    }
    @Test
    void shouldRangeOKIfArrayEmpty(){
        int[] array = new int []{};
        Integer result = sumElements.howManyElementsToSumUp(array,9);
        Assertions.assertNull(null);
    }

    @Test
    void shouldRangeOKIfFirstElementFine(){
        int[] array = new int []{5,6,7,8};
        Integer result = sumElements.howManyElementsToSumUp(array,4);
        Assertions.assertEquals(Integer.valueOf(1),result);
    }

    @Test
    void shouldRangeOKIfArrayNull(){

        Integer result = sumElements.howManyElementsToSumUp(null,9);
        Assertions.assertNull(result);
    }
    @Test
    void shouldRangeOKIfNumNegative (){
        int[] array = new int []{-1,-2,-3,-4,-5,-6};
        Integer result = sumElements.howManyElementsToSumUp(array,9);
        Assertions.assertNull(result);
    }
}
