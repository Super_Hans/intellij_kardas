package com.company.generic_types.zadanie171;

import com.sun.xml.internal.org.jvnet.fastinfoset.FastInfosetException;

import javax.xml.stream.FactoryConfigurationError;

public class Zad172_try_w_res {
    public static void main(String[] args) {
        try (TextManager text = new TextManager()) {
            text.show("Sunday");
            text.show("Marcin is smart");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class A {
        void method() throws FastInfosetException {
        }

        class B extends A {
            @Override
            void method() throws FactoryConfigurationError {
                try {
                    super.method();
                } catch (FastInfosetException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
