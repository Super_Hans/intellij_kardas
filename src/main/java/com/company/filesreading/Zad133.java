package com.company.filesreading;

import java.io.*;

//Utwórz metodę która przyjmuje jako parametr ścieżkę do pliku,
// a następnie wyświetla zawartość tego pliku na konsolę.
// Zrealizuj zadanie odczytując dane “linia po linii”
public class Zad133 {
    public static void main(String[] args) {
        try {
            readFile("files/filesReading.txt");
        } catch (IOException e) {
            System.out.println("Wrong file and/or file path");
        }

    }
    private static void readFile (String filePath) throws IOException {
        FileReader file = new FileReader(filePath);
        BufferedReader reader = new BufferedReader(file);
        String getLine = reader.readLine();

        while (getLine != null) {
            System.out.println(getLine);
            getLine = reader.readLine();

        }
    }
}
