package com.company.homework;

import java.util.*;

//*ZADANIE #115  -  PRACA DOMOWA*
// Utwórz metodę, która przyjmuje jeden parametr - mapę w postaci `String : List<Integer>`.
// Metoda ma zwrócić napis.
// W mapie przekazanej jako parametr, kluczami są litery
// a jako wartościami - *pozycje* na jakich mają wystąpić w zwracanym napisie.
@SuppressWarnings("MismatchedReadAndWriteOfArray")
public class Zad115 {
    public static void main(String[] args) {

        Map<String, List<Integer>> stringListMap = new HashMap<>();
        List<Integer> integerList = new ArrayList<>();
        List<Integer> integerList1 = new ArrayList<>();
        List<Integer> integerList2 = new ArrayList<>();
        integerList.add(0);
        integerList1.add(1);
        integerList2.add(2);

        stringListMap.put("d", integerList);
        stringListMap.put("o", integerList1);
        stringListMap.put("g", integerList2);
        System.out.println(getWord(stringListMap));

        Map<String, List<Integer>> nowaMappa = new HashMap<>();
        nowaMappa.put("K", Arrays.asList(0,4));
        nowaMappa.put("A", Arrays.asList(1,3));
        nowaMappa.put("J", Arrays.asList(2));
        System.out.println(getWord(nowaMappa));

    }
    private static String getWord (Map<String, List<Integer>> map) {
        int arrLength = 0;
        for (List<Integer> listValue : map.values()) {
            arrLength += listValue.size();
        }
        String [] stringArr = new String[arrLength];
        for (Map.Entry<String, List<Integer>> entry : map.entrySet()) {
            for (Integer position : entry.getValue()) {
                stringArr[position] = entry.getKey();
            }
        }
        // scala elementy znakiem ==> .join
        // wrzuca separator miedzy elementy
        return String.join("",stringArr);
    }
}














 //   private static String getWord(Map<String, List<Integer>> map) {
//        StringBuilder resultPhrase = new StringBuilder();
////
////        Set<String> stringSet = map.keySet();
////        String[] strings = new String[stringSet.size()];
////        int count = 0;
////
////        for (List<Integer> value : map.values()) {
////            strings[count] = String.valueOf(value);
////            count++;
////        }
////
////            for (String x : map.keySet()) {
////                strings[v] = x;
////            }
////
////        return resultPhrase.append(strings.toString();
////        }
   // }

