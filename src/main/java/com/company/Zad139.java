package com.company;

import javafx.util.Pair;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//*ZADANIE #139*
//Utwórz metodę która pobiera od użytkownika login i hasło,
// a następnie sprawdza czy podane dane są prawidłowe (weryfikując to z plikiem,
// w którym wartości rozdzielone są spacją)
//```admin admin123
//   mojLogin 123456```
public class Zad139 {
    public static void main(String[] args) {
        try {
            List<Pair<String, String>> list = readFile("files/zadanie139.txt");
            System.out.println(doesGivenDataCorrect(list));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static List<Pair<String, String>> readFile(String filePath) throws IOException {
        List<Pair<String, String>> list = new ArrayList<>();
        FileReader reader = new FileReader(filePath);
        BufferedReader buffRead = new BufferedReader(reader);
        String line = buffRead.readLine();

        while (line != null) {
            String[] arr = line.split(" ");
            if (arr.length == 2) {
                Pair<String, String> pair = new Pair<>(arr[0], arr[1]);
                list.add(pair);
            }
            line = buffRead.readLine();
        }

        return list;
    }

    private static boolean doesGivenDataCorrect(List<Pair<String, String>> list) {
        Scanner scan = new Scanner(System.in);
        boolean isCorrect = false;

        for (int howManyTimesEntered = 0; howManyTimesEntered < 3; howManyTimesEntered++) {
            System.out.println("Enter login ");
            String login = scan.nextLine();
            System.out.println("Enter password ");
            String pass = scan.nextLine();
            isCorrect = checking(list, login, pass);
            if (isCorrect) {
                break;
            }
            System.out.println("Enter correct pass and login");

        }

        return isCorrect;
    }

    private static boolean checking(List<Pair<String, String>> list, String login, String pass) {
        for (Pair<String, String> stringPair : list) {
            if (stringPair.getKey().equals(login)
                    &&
                    stringPair.getValue().equals(pass)) {
                return true;
            }

        }
        return false;
    }

}
