package com.company;
//*Utwórz metodę, do której przekazujesz liczbę, która jest numerem miesiąca,
//a program powinien *zwrócić nazwę* tego dnia.
//Wykorzystaj instrukcję warunkową *switch*
//> Dla `1` zwróci `styczeń`
//>
//> Dla `2` zwróci `luty`
//>
//>`...`
//>
//> Dla `7` zwróci `lipiec`

public class Zad18Switch {

    public static void main(String[] args) {
//        System.out.println(nazwaMiesiaca(7));
        System.out.println(jakiMiesiac(5));
    }

    static String nazwaMiesiaca(int numerMiesiaca) {
        switch (numerMiesiaca) {
            case 1:
                return "styczeń";
            case 2:
                return "luty";
            case 3:
                return "marzec";

            default:
                return "błędny numer miesiąca";
        }
    }
    static String jakiMiesiac (int miesiac) {
        String message = "Błędny numer miesiąca";
        switch (miesiac) {
            case 4:
                message = "kwiecień";
                break;
            case 5:
                message = "maj";
                break;
            case 6:
                message = "czerwiec";
                break;

        }
        return message;
    }
}
