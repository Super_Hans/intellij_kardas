package com.company;
//*Utwórz metodę, która przyjmuje *jeden* parametr (który jest liczbą wierszy i kolumn) oraz
// wyświetla tabliczkę mnożenia (znaki możesz oddzielać znakiem tabulacji `\t`).
//> Dla `3` wyświetli:
//>
//```1   2   3
//2   4   6
//3   6   9```

public class Zad30 {
    public static void main(String[] args) {
//liczbaKolumnIWierszy(4);
        whileTabMnozenia(4);
   /* }
    static void liczbaKolumnIWierszy (int liczbaKW) {
        for (int i = 1; i <= liczbaKW ; i++) {
            for (int j = 1; j <= liczbaKW ; j++) {
                System.out.print(i*j + "\t");
            }
            System.out.println();
        }
    }
}*/
    }

    static void whileTabMnozenia(int wymiar) {
        int i = 1;
        int j = 1;
        while (i <= wymiar) {
            while (j <= wymiar) {
                System.out.print(i * j + "\t");
                j++;
            }

            j = 1;
            i++;

            System.out.println();
        }
    }
}
