package com.company;

public class Zad26 {
    public static void main(String[] args) {
        int wynik = sumOfElements(2);
        System.out.println(wynik);
        System.out.println(sumOfElements(8));
    }

    static int sumOfElements(int parameter) {
        int sum = 0;
        for (int x = 1; x <= parameter; x++) {
            sum += x;
        }
        return sum;
    }
}
