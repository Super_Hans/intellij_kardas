package com.company;

//**ZADANIE #56*
//Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
// Metoda ma zwrócić *ile elementów* (idąc po kolei od *PRAWEJ*)
// należy zsumować by przekroczyć podany (jako drugi) parametr
//> dla ([1,2,3,4,5,6],  9) należy zwrócić `2`
public class Zad56 {
    public static void main(String[] args) {
        int[] tablica = new int[]{1, 2, 3, 4, 5, 6};
        System.out.println(zwrocIloscElementow(tablica, 16));

    }

    public static int zwrocIloscElementow(int[] tablica, int sumaMinimalna) {
        int suma = 0;
        for (int i = tablica.length - 1; i >= 0; i--) {
            suma += tablica[i];
            if (suma >= sumaMinimalna) {
                return tablica.length - i;
            }
        }
        return 0;
    }
}
