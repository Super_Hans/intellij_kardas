package com.company.codewars;

import java.util.Arrays;

public class Petals {
    public static void main(String[] args) {
        System.out.println(howMuchILoveYou(45));
    }

    private static String howMuchILoveYou(int nb_petals) {

        return Arrays.asList("not at all",
                "I love you",
                "a little",
                "a lot",
                "passionately",
                "madly")
                .get(nb_petals % 6);
    }
}
