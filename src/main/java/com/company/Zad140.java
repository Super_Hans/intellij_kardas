package com.company;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Array;
import java.util.Arrays;

//*ZADANIE #140*
//Utwórz metodę która przyjmuje tablicę liczb i szuka największej z nich.
// W pliku tekstowym ma być parametr od którego zależy czy cokolwiek będzie wyświetlane na konsoli czy nie.
//
//Jeśli *tak*, metoda powinna *wyświetlić* zawartość całej tablicy oraz *wyświetlić* znalezioną liczbę*.
//```Największa liczba to:  15
//Zbiór: [1, 4, 2, -9, 1, 15]```
//
//Jeśli *nie*, na konsoli nie powinien pojawić się żaden napis.
public class Zad140 {
    public static void main(String[] args) {
        int[] array = new int[]{1, 4, 2, -9, 1, 15};
        if (ifShowInfo()) {
            System.out.println(showHighest(array));
            System.out.println(Arrays.toString(array));
        }

    }

    private static int showHighest(int[] array) {
        int max = array[0];

        for (int i : array) {
            if (max < i) {
                max = i;
            }
        }
        return max;
    }

    private static boolean ifShowInfo() {
        try {
            // check if condition is true
            FileReader reader = new FileReader("files/zadanie140.txt");
            BufferedReader buffReader = new BufferedReader(reader);
            String line = buffReader.readLine();
            return !line.isEmpty();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
