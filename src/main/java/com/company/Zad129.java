package com.company;

import java.util.InputMismatchException;
import java.util.Scanner;

//*ZADANIE #129*
//Utwórz metodę która wczytuje liczbę (czyli typ `int`) od użytkownika a następnie ją wyświetla.
// W przypadku podania innej wartości (np. litery), wyświetl użytkownikowi komunikat i pobierz informację ponownie.
// Zaimplementuj to na dwa sposoby (pierwszy - obsługa wyjątku przez metodę pobierającą,
// drugi - metoda rzucająca wyjątek)
public class Zad129 {
    public static void main(String[] args) {
        // executing exception inside method
        System.out.println(getInteger());
        // method throws exception!
        try {
            System.out.println(getInteger2());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int getInteger() {
        Scanner scan = new Scanner(System.in);
        // use Ctrl+Alt+T bitte
        int scanInt;
        try {
            System.out.print("Enter number: ");
            scanInt = scan.nextInt();
            //catch (Exception e) {
        } catch (InputMismatchException e) {
            e.printStackTrace();
            scanInt = 0;
        }
        return scanInt;

    }
    // using this method different way
    private static int getInteger2() throws Exception {
        Scanner scan = new Scanner(System.in);

        return scan.nextInt();
    }
}
