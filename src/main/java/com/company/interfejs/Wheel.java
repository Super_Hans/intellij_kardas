package com.company.interfejs;

public class Wheel extends Shape {
    private int radius;

    public Wheel(int radius) {
        this.radius = radius;
    }

    @Override
    String showInformationAboutPerson() {
        return String.format("I am wheel and my radius is %s cm",
                radius);
    }

    @Override
    public double calcArea() {
        return Math.PI * Math.pow(radius,2);
    }

    @Override
    public double calcVolume() {
        return 0;
    }

    @Override
    public double calcCirc() {
        return 2*Math.PI*radius;
    }
}
