package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

//Utwórz metodę, która przyjmuje listę (`List`)
// np. `ArrayList` elementów typu `String` a następnie zwraca listę w odwróconej kolejności.
public class Zad100ArrayList {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>(Arrays.asList(
                "Honda", "bierze", "olej"
        ));
        System.out.println(revertList(list));

    }
    private static List<String> revertList(List<String> stringList) {
        Collections.reverse(stringList);
        return stringList;

    }
}
