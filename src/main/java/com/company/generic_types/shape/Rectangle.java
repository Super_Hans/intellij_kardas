package com.company.generic_types.shape;

public class Rectangle implements Shape {

    @Override
    public String getName() {
        return "Rectangle";
    }
}
