package com.company;

public class Zad28 {
    public static void main(String[] args) {
        power(3, 100);
    }

    public static void power(int liczba, int limes) {
        for (int i = 0; ; i++) {
            double result = Math.pow(liczba, i);
            if (result >= limes) {
                break;
            }
            System.out.println(liczba + "^" + i + " -> " + result);
        }
    }
}
