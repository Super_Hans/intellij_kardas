package com.company.interfejs;

public class Square extends Rectangle {
    Square(int width) {
        super(width, width);
    }

    @Override
    String showInformationAboutPerson() {
        return String.format("I am square with %s cm side",
                width);
    }
}
