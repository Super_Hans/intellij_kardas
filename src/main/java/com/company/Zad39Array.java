package com.company;
//**ZADANIE #39*
//Utwórz metodę, która przyjmuje dwa parametry - tablicę (wartości `short`) oraz liczbę.
// Metoda ma zwrócić informację (jako wartość logiczna) czy dana liczba znajduje się w tablicy.

public class Zad39Array {
    public static void main(String[] args) {
        short array[] = new short[]{3, 5, 8};
        short variab = 9;
        boolean result = czyZnajdujeSieWTabl(array, variab);
        System.out.println(result ? "Liczba znajduje sie w tablicy"
                : "Liczba nie znajduje się w tablicy");
    }

    public static boolean czyZnajdujeSieWTabl(short[] array, short num) {
        for (short number : array) {
            if (number == num) {
                return true;
            }
        }
        return false;
    }
}
