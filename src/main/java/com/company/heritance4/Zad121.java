package com.company.heritance4;
//*ZADANIE #121*
//Utwórz klasę `Pojazd` (z polami `nazwa`, `maxPredkosc` i `liczbaPasazerow`) a następnie klasy dziedziczące:
//- `Samochod` (z polami `liczbaKol` i `liczbaDrzwi`)
//- `Pociag` (z polami `liczbaWagonow` i `czyMaWagonBarowy`)
public class Zad121 {
    public static void main(String[] args) {
        Car car = new Car("Honda",200,4);
        Train train = new Train("Intercity",80,280, 6);
//        Vehicle' is abstract; cannot be instantiated
//        Vehicle vehicle = new Vehicle("Boat",10,120);
    }
}
