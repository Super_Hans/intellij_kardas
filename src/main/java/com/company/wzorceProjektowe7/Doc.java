package com.company.wzorceProjektowe7;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static com.company.wzorceProjektowe7.DocFactory.*;

class Doc {
    private DocumentType type;
    private String content;

    Doc(DocumentType type, String content) {
        this.type = type;
        this.content = content;
    }

    void safeFile(String fileName) {
        String output = String.format("files/%s.%s",
                fileName,
                type.getExtension());
        // catch on with file
        File file = new File(output);
        //try with resources
        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.write(content);
        } catch (IOException exc) {
            exc.printStackTrace();
        }

    }

    String getContent() {
        return content;
    }
}
