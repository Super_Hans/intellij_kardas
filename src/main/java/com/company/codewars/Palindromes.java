package com.company.codewars;

public class Palindromes {
    public static void main(String[] args) {
        System.out.println(longestPalindrome("aab"));
        System.out.println(longestPalindrome("a"));
        System.out.println(longestPalindrome("aa"));
        System.out.println(longestPalindrome(""));
        System.out.println(longestPalindrome("123454321"));

    }
    public static int longestPalindrome(final String s) {

        if (s.isEmpty()){
            return 0;
        }
        int counter = 1;
        String [] strArr = s.split("");

        for (int i = 0; i <strArr.length ; i++) {
            if (strArr[i].equals(strArr[strArr.length - 1])) {
                counter++;
            }
        }
        return counter;
    }
}
