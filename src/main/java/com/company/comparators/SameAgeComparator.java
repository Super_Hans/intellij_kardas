package com.company.comparators;

import java.util.Comparator;

public class SameAgeComparator implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        Worker worker1 = (Worker) o1;
        Worker worker2 = (Worker) o2;

        int nameCompare = worker1.getName().compareTo(worker2.getName());

        if (nameCompare == 0) {
            int ageCompare = Integer.compare(worker1.getAge(), worker2.getAge());

            if (ageCompare == 0) {
                return Integer.compare(worker1.getSalary(), worker2.getSalary());
            }

            return ageCompare;
        }

        return nameCompare;
    }
}
