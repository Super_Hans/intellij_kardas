package com.company.generic_types.shape;

public class Square extends Rectangle implements Shape {

    @Override
    public String getName() {
        return "Square";
    }
}
