package com.company;

public class Codewars7 {
    public static void main(String[] args) {
        System.out.println(countingSheep(6));
    }
        public static String countingSheep ( int num){
            String result = "";
            for (int i = 1; i <= num; i++) {
                result += i + " sheep...";
            }
            return result;
        }
    }
