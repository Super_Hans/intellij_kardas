package com.company.comparators;

import java.util.Comparator;

public class NameComparator implements Comparator {
    @Override
    public int compare(Object worker1, Object worker2) {
        //casts from one class and tries to set new object
        Worker w1 = (Worker)worker1;
        Worker w2 = (Worker)worker2;

        return w1.getName().compareTo(w2.getName());
    }
}
