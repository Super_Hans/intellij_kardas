package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//Utwórz metodę, która przyjmuje jeden parametr - napis (typu `String`).
// Metoda ma zwrócić mapę w postaci `String : List<Integer>`,
// w której jako klucze mają być litery (z napisu wejściowego)
// a jako wartości - *pozycje* na jakich występują one w napisie wejściowym.
public class Zad114 {
    public static void main(String[] args) {
        System.out.println(getMapFromLettPos("verydullWord"));
        System.out.println(getMapFromLettPos("dog"));

    }

    private static Map<String, List<Integer>> getMapFromLettPos(String word) {
        Map<String, List<Integer>> mappa = new HashMap<>();
        // create an array from String given
        String[] stringArr = word.split("");
        int value = 0;

        for (String letters : stringArr) {
//            List<Integer> yetAnotherList = new ArrayList<>();
            List<Integer> yetAnotherList;
            if (mappa.containsKey(letters)) {
                yetAnotherList = mappa.get(letters);
            } else {
                yetAnotherList = new ArrayList<>();
            }
            yetAnotherList.add(value);
            mappa.put(letters, yetAnotherList);
            value++;
        }
        return mappa;
    }
}
