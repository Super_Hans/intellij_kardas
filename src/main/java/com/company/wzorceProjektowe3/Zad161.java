package com.company.wzorceProjektowe3;

public class Zad161 {
    public static void main(String[] args) {
        Configuration config = Configuration.getInstance();
        config.setFavNum(8);

        method1();

        config.setFavNum(67);
        method1();
    }
    // in completely different method can be called
    static private void method1 (){
        Configuration config = Configuration.getInstance();
        System.out.println(config.getFavNum());
    }
}
