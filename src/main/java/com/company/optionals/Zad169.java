package com.company.optionals;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Zad169 {
    public static void main(String[] args) {
//        method1();
//        method2();
//        method3();
        method4();
    }

    private static void method4() {
        List<Integer> arrayList = Arrays.asList(1, 4, 8, 14, 18, 121, 4, 54, 12, 2, 39);

        Optional<Integer> first = arrayList.stream()
                .filter(number -> number > 100)
                .findFirst();

        Integer second = arrayList.stream()
                .filter(number->number>100)
                .findFirst()
                .orElse(0);

        if (first.isPresent()) {
            System.out.println("Yes, found number: " + first.get());
        } else {
            System.out.println("Does not found matching number");
        }
    }

    private static void method3() {
        Optional<Double> result = divisionSafe(4, 2);
        System.out.println(result.isPresent());
        double resultD = result.orElse(0D);
        System.out.println(resultD);

        //optional type to avoid null
        Optional<Double> result2 = divisionSafe(7, 0);
        System.out.println(result2.isPresent());
        double secondResult = result2.orElse(0D);
        System.out.println(secondResult);

    }

    private static Optional<Double> divisionSafe(double first, double sec) {

        if (sec == 0) {
            return Optional.empty();
        } else {
            return Optional.of(first / sec);
        }
    }

    private static void method2() {
        Car car = new Car();
        // optionals excludes `null` from equation
        Optional<Integer> age = car.getAge();

        // always assign value
        int realAge = age.orElse(-1);

        // check if value is different from null "is present" inside
        if (age.isPresent()) {
            int real = age.get();
        }
    }

    private static void method1() {
        Car car = new Car();
        System.out.println(car.getName());

        if (car.getName() != null) {
            String carName = car.getName().toUpperCase();
        }
    }
}
