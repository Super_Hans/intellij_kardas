package com.company.filesreading4;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

//Przygotuj plik .csv (gdzie wartości oddzielone są przecinkami
// - `,`), który będzie zawierał reprezentację obiektów klasy `Osoba`.
// Metoda ma odczytać plik i zwrócić listę obiektów klasy `Osoba`.
//```Jan,Nowak,12,false
//Anna,Kowalska,25,true
//Kuba,Janowski,66,false
//Karolina,Nowa,88,true```
public class Zad135 {
    private static final String SEPARATOR = ",";

    public static void main(String[] args) {
        try {
            List<Person> listOfPpl = getListOfPpl("files/zadanie135.txt");
            for (Person person : listOfPpl) {
                System.out.println(person);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<Person> getListOfPpl(String filePath) throws IOException {
        FileReader fileReader = new FileReader(filePath);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = bufferedReader.readLine();
        List<Person> listOfPeople = new LinkedList<>();

        while (line != null) {
            if (!line.isEmpty()) {
                Person person = getPersonFromLine(line);
                listOfPeople.add(person);
            }
            line = bufferedReader.readLine();
        }
        return listOfPeople;
    }

    private static Person getPersonFromLine(String line) {
        String[] lineArr = line.split(SEPARATOR);

        return new Person(
                lineArr[0],
                lineArr[1],
                Integer.valueOf(lineArr[2]),
                Boolean.valueOf(lineArr[3])
        );
    }

}
