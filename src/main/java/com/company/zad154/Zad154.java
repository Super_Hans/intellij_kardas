package com.company.zad154;

public class Zad154 {

    public static void main(String[] args) {
       // method1();
       // method2();
       // method3();
        method4();
    }
    private static void method1(){
        Computer computer = new Computer();
        computer.start();
    }

    private static void method2(){
        NewComputer newComputer = new NewComputer();
        newComputer.start();
    }

    private static void method3(){
        //creation of anonymous class - completely new - build on a fly;
        //sometimes we need something for just 1 case
        new Computer() {
            @Override
            protected void preparation() {
                super.preparation();
                System.out.println("Anonymous loading sequence");
            }
        }.start();
    }
    private static void method4(){
        // Anonymous class; method is getting data but change only one
        //inheritance in one place only - changes method
        Computer computer2 = new Computer(){
            @Override
            protected void preparation() {
                super.preparation();
                System.out.println("Computer hacked");
            }

            @Override
            void start() {
                super.start();
                System.out.println("LOL");
            }
        };

        computer2.start();

    }
}
