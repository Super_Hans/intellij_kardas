package com.company.wzorceProjektowe;

import java.util.Arrays;
import java.util.List;

class Facade {
    private final int TEMP_OUT = 18;
    private final int TEMP_IN = 22;
    private List<Windows> windows = Arrays.asList(
            new Windows(),
            new Windows(),
            new Windows()

    );
    private Lights light = new Lights();
    private Alarm alarm = new Alarm();
    private Heating heating = new Heating();

    void imOut() {
        windows.stream().forEach(window -> window.close());
        alarm.enable();
        light.lightsOff();
        heating.setTemp(TEMP_OUT);
    }

    void imIn() {
        windows.forEach(Windows::open);
        alarm.disable();
        light.lightsOn();
        heating.setTemp(TEMP_IN);
    }
}
