package com.company.interfejs;

import java.util.ArrayList;
import java.util.List;

//Utwórz klasę `Ksztalt` oraz klasy dziedziczące po niej:
//        - klasę `Prostokat` (z polami `dlugosc`, `szerokosc`),
//        - klasę `Kwadrat` (z polem `dlugosc`),
//        - klasę `Koło` (z polem `promien`).
//        Zablokuj możliwość tworzenia obiektów klasy `Ksztalt` oraz wymuś,
// by klasy dziedziczące musiały zaimplementować metodę `wyswietlInformacjeOSobie()`
public class Zad123 {
    public static void main(String[] args) {
        Wheel w = new Wheel(5);
        //Shape w = new Wheel(5); in this Object is implemented
        Rectangle r = new Rectangle(5,2);
        Square s= new Square(4);
        //System.out.println(r.showInformationAboutPerson());

        List<Shape> listOfShapes = new ArrayList<>();
        listOfShapes.add(w);
        listOfShapes.add(s);
        listOfShapes.add(r);

        for (Shape listOfShape : listOfShapes) {
            System.out.println(listOfShape.showInformationAboutPerson());
            System.out.printf ("Area of shape is %.2f cm2 \n",
                    listOfShape.calcArea());
        }
    }
}
