package com.company.zadanie174;

public class Zad174 {
    public static void main(String[] args) {
//        method1();
//        method2();
//        method3();
//        method4();
        method5();
    }

    private static void method5(){
        System.out.println("Main thread = " +
                Thread.currentThread().getId());
        ThreadOfUs thread = new ThreadOfUs();
        ThreadOfUs thread2 = new ThreadOfUs();
        thread.start();
        thread2.start();
        try {
            thread.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("End of work " +
                Thread.currentThread().getId());
    }

    private static void method4() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                for (int start = 0; start < 20 ; start++) {
                    System.out.printf("%s. ", start);

                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }

    private static void method3 () {
        Thread thread1 = new Thread() {
            @Override
            public void run() {
                long id = Thread.currentThread().getId();
                for (int i = 0; i < 20; i++) {
                    System.out.printf("%s. (id = %s)\n", i, id);
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        };
        Thread thread2 = new Thread() {
            @Override
            public void run() {
                long id = Thread.currentThread().getId();
                for (int i = 0; i < 20; i++) {
                    System.out.printf("%s. _______________>(id = %s)\n", i, id);
                    try {
                        Thread.sleep(800);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

        };
        thread1.start();
        thread2.start();
    }

        private static void method2 () {
            //creation of anonymous class
            Thread thread1 = new Thread(() ->
                    System.out.println("ID of new thread: " + Thread.currentThread().getId()));
            thread1.start();
        }

        private static void method1 () {
            System.out.println("ID of current thread: " + Thread.currentThread().getId());
        }

    }
