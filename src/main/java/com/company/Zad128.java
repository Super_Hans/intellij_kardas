package com.company;

//Utwórz metodę która wykonuję operację dzielenia dwóch liczb przekazywanych jako parametry.
// W przypadku podzielenia przez `0` metoda powinna zwrócić `null`.
public class Zad128 {
    public static void main(String[] args) {
        System.out.println(dividing(3450, 15));
        System.out.println(dividing(11, 0));
        System.out.println(dividing(11, 3) == null);
    }

    private static Integer dividing(int a, int b) {
        try {
            return a / b;
        } catch (ArithmeticException e) {
            e.printStackTrace();
            return null;
        }
    }
}
