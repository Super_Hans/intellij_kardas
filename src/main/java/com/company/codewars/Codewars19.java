package com.company;

import static java.lang.Math.pow;

public class Codewars19 {
    public static void main(String[] args) {
        System.out.println(maxRot(56789));
    }

    public static long maxRot(long n) {
        //  long cyfry = 5;
        //   return (long) ((n % 10) * pow(10, cyfry - 1) + (n / 10)); moje wyliczenie - daje cyfrę z końca na poczatek

        String val = String.valueOf(n); //converted to string so we can use substring
        String temp = "";
        long max = n;

        for (int i = 1; i < val.length(); i++) {
            val = temp.substring(0, i - 1) + val.substring(i) + val.substring(i - 1, i); //rotation
            max = Math.max(max, Long.parseLong(val)); //get maximum as program operates
            temp = val; //temporary storage of previous state of string

        }
        return max;
    }
}

