package com.company.initialization_blocks;

class Car extends Vehicle {
    //variables always on top
    final static Integer NUM_OF_WHEELS = 4;

    {
        //initializing block of Car object
        System.out.println("Initializing block of Car");
    }
    {
        System.out.println("Initializing block of second Car");

    }
    static  {
               System.out.println("Initializing STATIC block of Car");

    }

    Car() {
        System.out.println("Car's constructor");
    }

}
