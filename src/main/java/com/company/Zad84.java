package com.company;

//Utwórz metodę, która przyjmuje wyraz, a następnie zwraca środkowy element.
// W przypadku nieparzystej długości ma zostać zwrócony wyraz długości `1`, a dla parzystych długości `2`
//> Dla `"Ala"`, zwróci `"l"`
//> Dla `"Anna"`, zwróci `"nn"`
public class Zad84 {
    public static void main(String[] args) {
        //System.out.println(getMiddleElement("HONDA"));
       // System.out.println(getMiddleElement("PRELUDEV"));
        System.out.println(getMidElement("HONDA"));
        System.out.println(getMidElement("PRELUDEV"));
    }

    // method returns string from string parameter
    private static String getMiddleElement(String phrase) {
        // splits word for characters; it is evoked on object(phrase)!!!
        String[] array = phrase.split("");
        int midPosition = phrase.length() / 2; // if we use var more than once - we take it here so it is faster to calculate

        if (phrase.length() % 2 == 0) {  //check if word is even
            return array[midPosition - 1] + array[midPosition]; // returns middle position and merge it with middle position on
        } else {                            // word is odd
            return array[midPosition];
        }
    }

    private static String getMidElement(String word) {
        int midPosition = word.length() / 2; // divide length of the word by half

        return word.substring(   //returns from evoked method substring; final position is midPos
                word.length() % 2 == 0 ? midPosition-1 : midPosition // correct position
                ,
                midPosition + 1);
    }
}

