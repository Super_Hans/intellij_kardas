package com.company.zadanie78;

public class Rectangle {
    // dwa pola priviet - można sie do nich odwołać tylko w tej klasie */
    private int dłuższyBok;
    private int krótszyBok;

    /**konstruktor obiektowy do tworzenia prostokątu - możemy do użyć w dowolny miejscu w projekcie dzieki mod. dost "public"*/
    public Rectangle(int dłuższyBok, int krótszyBok) { // przekazujemy do niego dwa parametry;
        this.dłuższyBok = dłuższyBok; //this. odwołuje się do OBECNEGO obiektu - szuka tej nazwy w polach tej klasy(i.e.dłuższyBok)
        this.krótszyBok = krótszyBok;
    }

    //metody
    int policzObwod() {
        return 2 * krótszyBok + 2 * dłuższyBok;
    }

    int policzPole() {
        return getDłuższyBok() * getKrótszyBok(); //gdy damy get możemy wywołać także w metodach
    }
    //To DO -
    static boolean czyPolaTeSame(Rectangle r1, Rectangle r2) { /**porównamy pola dwoch prostokatow*/
        return r1.policzPole() == r2.policzPole();
    }
    /**2gettery i settery */

    public int getKrótszyBok() {
        return krótszyBok;
    }

    public int getDłuższyBok() {
        return dłuższyBok;
    }

    public void setDłuższyBok(int dłuższyBok) {
        this.dłuższyBok = dłuższyBok;
    }


}
