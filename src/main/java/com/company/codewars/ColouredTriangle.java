package com.company.codewars;


public class ColouredTriangle {
    public static void main(String[] args) {
        System.out.println(colouredTriangle("BBRG"));

    }

    static char colouredTriangle(final String row) {
        char[] tab = row.toCharArray();
        char[] tab2 = tab;

        for (int j = 0; j < row.length(); j++) {
            tab2 = new char[tab.length - 1];
            if (tab.length == 1) {
                return tab[0];
            }
            for (int i = 0; i < tab.length - 1; i++) {
                if (tab[i] == tab[i + 1]) {
                    tab2[i] = tab[i];
                } else {
                    tab2[i] = "RGB".replaceAll(String.valueOf(tab[i]), "")
                            .replace(String.valueOf(tab[i + 1]), "")
                            .charAt(0);
                }
            }
            tab = tab2;
        }
        return tab2[0];
    }
}
