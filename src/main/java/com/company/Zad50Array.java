package com.company;
//**ZADANIE #50*
//Utwórz metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
// Metoda ma zwrócić informację ile razy w tablicy występuje liczba podana jako drugi parametr.

import java.util.Arrays;

//Utwórz drugą metodę, która przyjmuje dwa parametry - tablicę oraz liczbę.
// Metoda ma usunąć wszystkie elementy równe podanemu parametrowi
// (tzn. gdy podamy `11` to ma usunąć z tablicy wszystkie `11`-ki)
//> Dla `([1, 2, 2, 4, 5],  2)`
//> zwróci `[1, 4, 5]`
public class Zad50Array {
    public static void main(String[] args) {
        int[] tablica = new int[]{1, 2, 2, 4, 5, 2, 2, 2, 5, 6};
        System.out.println(ileRazyWTablicy(tablica, 2));
        System.out.println(
                Arrays.toString(
                        usunPodanaLiczbe(tablica, 2)));
    }

    public static int ileRazyWTablicy(int[] tablica, int drugiParametr) {
        int wystepowanie = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (drugiParametr == tablica[i]) {
                wystepowanie++;
            }
        }
        return wystepowanie;
    }

    public static int[] usunPodanaLiczbe(int[] tablica, int usuwanyParametr) {
        int wystepowanie = ileRazyWTablicy(tablica, usuwanyParametr);
        int[] tablicaNowa = new int[tablica.length - wystepowanie];
        int indeksTablicyNowej = 0;

        for (int i = 0; i < tablica.length; i++) {
            if (usuwanyParametr != tablica[i]) {
                tablicaNowa[indeksTablicyNowej] = tablica[i];
                indeksTablicyNowej++;
            }
        }
        return tablicaNowa;
    }

}
