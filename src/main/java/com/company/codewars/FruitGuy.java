package com.company.codewars;

import java.util.Arrays;

public class FruitGuy {
    public static void main(String[] args) {
        String[] basket = {"apple", "rottenTomato", "tomato", "apple"};
        System.out.println(Arrays.toString(removeRotten(basket)));

    }

    static String[] removeRotten(String[] fruitBasket) {
        String[] resultBasket = new String[fruitBasket.length];

        if (fruitBasket != null) {
            //   return new String[0];


            for (int i = 0; i < fruitBasket.length; i++) {
                resultBasket[i] = fruitBasket[i].replaceAll("rotten", "").toLowerCase();

            }

        }
        return resultBasket;
    }
}
