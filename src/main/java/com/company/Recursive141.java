package com.company;
//     Wyświetl 5 razy napis `Hello World` przy wykorzystaniu *rekurencji*.
public class Recursive141 {
    private static int count = 0;
    public static void main(String[] args) {
        showPhrase();

    }
    private static void showPhrase () {
        count++;
        if(count<=5){
            System.out.println("Hello World! " + count);
            showPhrase();
        }
    }
}
