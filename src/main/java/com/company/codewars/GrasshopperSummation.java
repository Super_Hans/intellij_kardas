package com.company.codewars;

import java.util.stream.IntStream;

public class GrasshopperSummation {
    public static void main(String[] args) {
        int n1= 3;
        int n2 = 8;
        System.out.println(summation(n1));
        System.out.println(summation(n2));

    }
    public static int summation(int n) {

        return IntStream.rangeClosed(1,n)
                .sum();
    }
}
