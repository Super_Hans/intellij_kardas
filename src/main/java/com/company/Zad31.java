package com.company;

public class Zad31 {
    public static void main(String[] args) {
        showChoinka(8);
    }

    static void showChoinka(int liczbaWierszy) {
        for (int i = 1; i <= liczbaWierszy; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j);
            }
            System.out.println();
        }
    }
}
