package com.company;

public class Math3 {
    public static void main(String[] args) {
        int a = 3;
        int b = 2;
        int c = a++ + ++b; //3+3
        System.out.println(c);
        int d = --a + --b + c--; //3 + 2 + 6=11
        System.out.println(d);
        int e = a + ++b + ++c + d--; //3+3+6+11=23
        System.out.println(e);
        int f = --a + b-- + --c - d++; //2+3+5-10=0
        System.out.println(f);
        int sum = a + b + c + d + e + f;
        // ==43
        System.out.println(sum);
    }
}
