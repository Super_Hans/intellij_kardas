package com.company.heritance2;

public class Person {
    private String name;
    private int age;
    private Sex sex;

    Person(String name, int age, Sex sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    @Override
    public String toString() {
        return String.format("Name: %s, Age: %s, Sex: %s",name,age,sex);
    }

    enum Sex {
        MAN, WOMAN, CHILD
    }
}
