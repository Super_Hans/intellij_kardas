package com.company.homework3;

public class CastleSolution {
    public static void main(String[] args) {
    int[] array ={3,2,1};
        System.out.println(howManyCastlesToBuild(array));
    }
    private static int howManyCastlesToBuild(int[] A){
        int hill = A[0];
        int castleCount = 0;

        for (int i = 1; i < A.length ; i++) {
            if(A[i]>hill ) {
                hill = A[i];
                castleCount++;
            }
            if(A[i]<hill){
                hill=A[i];
            }
        }

        return castleCount;
    }
}
