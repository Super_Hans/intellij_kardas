package com.company;
//*Utwórz metodę, która wyświetli
// wszystkie liczby całkowite z przedziału `10` a `20`.

public class Zad22Loop {
    public static void main(String[] args) {
        showFull();
        showPrzedzial();
        showLiczbyDoWhile();
    }

    static void showFull() {
        for (int number = 10; number < 21; number++) {
            System.out.print(number + ",");
        }
        System.out.println();
    }

    static void showPrzedzial() {
        int i = 10;
        while (i <= 20) {
            System.out.print(i + ",");
            i++;
        }
        System.out.println();
    }

    static void showLiczbyDoWhile() {
        int i = 10;
        do {
            System.out.print(i + ",");
            i++;
        } while (i <= 20);
    }
}
