package com.company.codewars;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//Given a dictionary/hash/object of languages and your respective test results,
// return the list of languages where your test score is at least 60, in descending order of the results.
//
//Note: There will be no duplicate values.
public class MyLang {
    public static void main(String[] args) {

    }

    public static List<String> myLanguages(final Map<String, Integer> results) {
        return results
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue() >= 60)
                .sorted((value1, value2) -> value2.getValue()-value1.getValue())
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

}
