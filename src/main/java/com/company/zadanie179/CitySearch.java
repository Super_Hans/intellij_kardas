package com.company.zadanie179;

import com.google.gson.annotations.SerializedName;
import lombok.ToString;
@ToString

class CitySearch {
    /**
     * https://www.metaweather.com/api/location/search/?query=new
     */
    //pre-mapping for titles
    @SerializedName("title")
    String title;

    @SerializedName("location_type")
    String location;

    @SerializedName("woeid")
    String id;

    @SerializedName("latt_long")
    String lattLong;

}
