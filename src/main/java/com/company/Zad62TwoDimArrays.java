package com.company;

import java.util.Random;

//**ZADANIE #62*
//Utwórz metodę, która przyjmuje dwa parametry - długość i szerokość tablicy, a następnie zwraca nowo utworzoną,
// dwuwymiarową tablicę wypełnioną losowymi wartościami. Utwórz drugą metodę do wyświetlania zwróconej tablicy.
// int [] [] Plansza = new int [3][2];
// [[0,0],[0,0],[0,0]]  np. t [1][0]
public class Zad62TwoDimArrays {
    public static void main(String[] args) {
        int[][] inszaTablica = zwrocTabliceWypelniona(4, 2);
        wyswietlTablice(inszaTablica);

    }

    static int[][] zwrocTabliceWypelniona(int dlug, int szer) {
        int[][] nowaTablica = new int[dlug][szer];

        Random random = new Random();
        for (int ndex = 0; ndex < dlug; ndex++) {
            for (int i = 0; i < szer; i++) {
                nowaTablica[ndex][i] = random.nextInt(50) -50;
            }

        }

        return nowaTablica;
    }

    static void wyswietlTablice(int[][] tablica) {
        for (int i = 0; i < tablica.length; i++) {
            for (int j = 0; j < tablica[0].length; j++) {
                System.out.print(tablica[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
