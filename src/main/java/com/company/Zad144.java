package com.company;

import org.jetbrains.annotations.NotNull;

//Cy palindrome
public class Zad144 {
    public static void main(String[] args) {
        System.out.println(ifPalindromeo("Kajak"));
        System.out.println(ifPalindrom("Kajak"));

    }
    private static boolean ifPalindrom (@NotNull String phrase) {
        return  ifPalindromeo(phrase.toLowerCase());
    }

    private static boolean ifPalindromeo(String phrase) {
        if (phrase.length() == 0 || phrase.length() == 1) {
            return true;
        } else if (phrase.charAt(0) == phrase.charAt(phrase.length() - 1)) {
            return ifPalindromeo(phrase.substring(1, phrase.length() - 1));
        } else {
            return false;
        }
    }
}
