package com.company;

/**
 * ZADANIE #15*
 * Utwórz metodę, do której przekazujesz dwa parametry
 * a ona zwróci informację (w formie wartości logicznej) czy *obydwie* są parzyste.
 * Oraz metodę, która zwróci informację czy *chociaż jeden z nich* jest większy od zera. (edited)
 */

public class Zad15 {
    public static void main(String[] args) {
        int x=10;
        int y=33;

        System.out.println(czyObieParzyste(x, y));
        System.out.println(czyObieParzyste(17, 228));
        System.out.println(czyObieParzyste(12, 118));
        System.out.println(czyObieParzyste(13, 99));

        System.out.println(wiekszyOdZera(-399, 987));
        System.out.println(wiekszyOdZera(x, -y));
        System.out.println(wiekszyOdZera(-9, -997));
    }

    static boolean czyObieParzyste(int pierwsza, int druga) {
        return pierwsza % 2 == 0 && druga % 2 == 0;
    }

    static boolean wiekszyOdZera(int pierwsza, int druga) {
        return pierwsza > 0 || druga > 0;

    }
}

