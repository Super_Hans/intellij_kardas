package com.company;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

//Utwórz metodę, która wczytuje liczby od użytkownika do momentu podania wartości `-1`
// następnie zwraca mapę wartości, gdzie kluczem jest liczba a wartością jest liczba jej wystąpień.
public class Zad112 {
    public static void main(String[] args) {
        System.out.println(getMapOfValues());
    }

    private static Map<Integer, Integer> getMapOfValues() {
        // Map with key with value integer and Value of integer
        Map<Integer, Integer> integerMap = new HashMap<>();
        Scanner input = new Scanner(System.in);

        while (true) {
            System.out.println("Enter number: ");
            int key = input.nextInt();
            if (key == -1) {
                break;
            }
            if (integerMap.containsKey(key)) {
                // check if map contains key and take value of the key
                int oldKeyExec = integerMap.get(key);
                // we change old key with new key with value increased by 1
                integerMap.put(key, oldKeyExec + 1);
            } else {
                // if map does not have other same values = return 1
                integerMap.put(key, 1);
            }
        }
        return integerMap;
    }
}
