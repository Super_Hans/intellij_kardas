package com.company.generic_types.methods;

import com.company.generic_types.shape.Shape;
import com.company.generic_types.without_generic.Orange;

public class ReturnMethods {

    //<> always before returned type!
    public static <TYPE> void method5(TYPE element) {
        System.out.println("Entered " + element
                .getClass()
                .getSimpleName());

    }

    public static <TYPE> TYPE method6(TYPE element) {
        return element;
    }

    public static <ABC extends Shape> ABC method7(ABC shape) {
        shape.hello();
        return shape;
    }

    public static <A> A method8(A first, A second) {
        System.out.println("To method entered variables of type: ");
        System.out.print(first.getClass().getSimpleName());
        System.out.print(" and ");
        System.out.print(second.getClass().getSimpleName());

        return first;
    }

    public static <T extends Shape, K extends Orange> boolean method9(
            T first, K second
    ) {
        return first.equals(second);
    }
}
