package com.company.filesreading;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

//Utwórz metodę która przyjmuje jako parametr ścieżkę do pliku,
// a następnie wyświetla zawartość tego pliku na konsolę.
// Zrealizuj zadanie odczytując dane “znak po znaku”.
public class Zad132 {
    public static void main(String[] args) {
        try {
            getFile("files/filesReading.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void getFile(String filePath) throws IOException {
        FileInputStream input = new FileInputStream(filePath);
        int character = input.read();

        while (character != -1) {
            System.out.print((char) character);
            character = input.read();

        }
    }
}
