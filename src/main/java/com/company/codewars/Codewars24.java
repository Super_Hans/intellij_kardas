package com.company.codewars;

public class Codewars24 {
    public static void main(String[] args) {
        System.out.println(sumTriangularNumbers(6));
    }

    public static int sumTriangularNumbers(int n) {
        int sumTriangular = 0;
        if (n < 0) {
            return 0;
        } else {
            for (int i = 1; i <= n; i++) {

                sumTriangular += (i * (i + 1)) / 2;
            }
        }
        return sumTriangular;
    }
}

