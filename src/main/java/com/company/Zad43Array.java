package com.company;

//**ZADANIE #43*
//Utwórz metodę, która jako parametr przyjmuje tablicę i zwraca drugą największą liczbę w tablicy.
//> dla `[1, 2, 3, 4, 5]` zwróci `4`
public class Zad43Array {
    public static void main(String[] args) {
        int[] tablica = {2, 2, 521, 4, 521, 666, 5};
        System.out.println(drugiMaxWTablicy(tablica));
    }

    static int drugiMaxWTablicy(int[] tablica) {
        int zmiennaMax = Integer.MIN_VALUE;
        int drugaMax = Integer.MIN_VALUE;
        for (int pozycja = 0; pozycja < tablica.length; pozycja++) { //iteracja
            if (tablica[pozycja] > zmiennaMax) {
                drugaMax = zmiennaMax;
                zmiennaMax = tablica[pozycja];
            } else if (tablica[pozycja] > drugaMax && tablica[pozycja] != zmiennaMax) {
                drugaMax = tablica[pozycja];
            }

        }
        return drugaMax;
    }
}
