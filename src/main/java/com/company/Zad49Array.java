package com.company;

//**ZADANIE #49*
//Utwórz metodę, która przyjmuje trzy parametry - tablicę oraz dwie liczby które są indexami (pozycjami) zakresu.
// Metoda ma zwrócić sumę elementów w podanym przedziale.
//> Dla `([1, 2, 3, 4, 5], 2, 4)`
//> zwróci `12`, bo `3 + 4 + 5`
public class Zad49Array {
    public static void main(String[] args) {
        int[] zakres = new int[]{1, 2, 3, 4, 5, 6, 7};
        System.out.println(zwrocSumeElementow(zakres, 0, 6));
    }

    public static int zwrocSumeElementow(int[] tablica, int pierwszaPozycja, int drugaPozycja) {
        int suma = 0;

        if (pierwszaPozycja > drugaPozycja || pierwszaPozycja < 0 || drugaPozycja > tablica.length - 1) {
            System.out.println("Błędny zakres");
        } else {

            for (int i = pierwszaPozycja; i <= drugaPozycja; i++) {
                suma += tablica[i];
            }
        }

        return suma;
    }

}
