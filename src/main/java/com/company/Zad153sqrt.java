package com.company;

public class Zad153sqrt {
    public static void main(String[] args) {
        long a = 100;
        long b = 5_000_000_000L;
        System.out.println(howManyTimesSquare(a, b));
    }

    private static int howManyTimesSquare(long A, long B) {
        int maxSquareCount= 0;

        for (long number = A; number <= B; number++) {
            int counter = squareRootCounter(number);

            if (counter>maxSquareCount){
                maxSquareCount=counter;
            }
        }

        return maxSquareCount;
    }

    private static int squareRootCounter(long number) {
        int howManyTimesSqrt = 0;

        double sqrt = Math.sqrt(number);
        while (sqrt % 1 == 0) {
            howManyTimesSqrt++;
            sqrt = Math.sqrt(sqrt);
        }

        return howManyTimesSqrt;
    }
}
