package com.company.wzorceProjektowe5;

class Calculator {
    //holds current strategy
    private Strategy currentStrategy;

    void setCurrentStrategy(Strategy currentStrategy) {
        this.currentStrategy = currentStrategy;
        System.out.println("Chosen "+ currentStrategy.nameOfOps());
    }

    double calculate(int number1, int number2) {
        if (currentStrategy == null) {
            return 0;
        }
        return currentStrategy.calculate(number1, number2);
    }
}
