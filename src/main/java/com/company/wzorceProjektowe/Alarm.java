package com.company.wzorceProjektowe;

class Alarm {

    void enable() {
        System.out.println("Alarm has been enabled");
    }

    void disable() {
        System.out.println("Alarm has been disabled");
    }
}
