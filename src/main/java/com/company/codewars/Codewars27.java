package com.company.codewars;

public class Codewars27 {
    public static void main(String[] args) {
        char sign = '\u2764';
        char a = 97;
        char b = 98;
        System.out.println(a);
        System.out.println(b);
        System.out.println(longest("aaaabbbddd", "xxxyyyzzz"));
    }

    public static String longest(String s1, String s2) {
        String longString = s1 + s2;
        StringBuilder finalWord = new StringBuilder();
        for (char sign = 'a'; sign <= 'z'; sign++) {
            if (longString.contains(String.valueOf(sign))) {
                finalWord.append(sign);
            }
        }

        return finalWord.toString();
    }
}
