package com.company.generic_types.without_generic;

public class OrangeBox {
    private Orange orange;

    public Orange getOrange() {
        return orange;
    }

    public OrangeBox(Orange orange) {
        this.orange = orange;
    }
}
