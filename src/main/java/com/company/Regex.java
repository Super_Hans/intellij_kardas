package com.company;

public class Regex {
    public static void main(String[] args) {
        System.out.println(yesOrNo("Przemek Kardas"));
        System.out.println(yesOrNo("Przemek Michail Kardas"));
        System.out.println(emailOk("p_kardas@interia.pl"));
        System.out.println(doesNumberOk("880-503-436"));
        System.out.println(validatePin("a4660"));
        System.out.println(validatePin("466993"));
        System.out.println(validatePin("1234"));
        //[a-z]{3,} ([a-z]{3,}) ? ( [a-z]{3,})

    }

    private static boolean yesOrNo(String imie) {
        return imie
                .toLowerCase()
                .matches("^[a-z]{3,}( [a-z]{3,})? ([a-z]{3,})$");
    }

    private static boolean emailOk(String mail) {
        return mail
                .toLowerCase()
                .matches("[a-z][.-_a-z1-9]+@+[a-z1-9]+\\.[a-z]{2,}");
    }

    private static boolean doesNumberOk(String telNumber) {
        return telNumber.matches("[0-9]{9}|([0-9]{3}-)([0-9]{3}-)[0-9]{3}");

    }

    private static boolean validatePin(String pin) {
        // Your code here...
        return pin.matches("[0-9]{4}|[0-9]{6}");
    }
}
