package com.company.comparators;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Zad156 {
    private static List<Worker> workersList = Arrays.asList(
            new Worker("Karl", "Marx", 37, 3500),
            new Worker("Karolin", "Marx", 52, 3500),
            new Worker("Drogo", "Khan", 25, 4500),
            new Worker("Viganna", "B", 37, 8000),
            new Worker("Karolin", "Fa ja", 37, 2300),
            new Worker("Karl", "Dupatsch", 37, 3300)
    );

    public static void main(String[] args) {
        //method1();
       // method2();
        //method3();
        method4();

    }
    private static void method1(){
        showList(workersList);
    }
    private static void method2(){
        Collections.sort(workersList);
        showList(workersList);
    }
    private static void method3(){
        workersList.sort(new NameComparator());
        showList(workersList);

    }
    private static void method4(){
        workersList.sort(new SameAgeComparator());
        showList(workersList);
    }

    private static void showList(List<Worker> workersList){
        System.out.println("   Name    |  Surname   | Age | Salary ");
        System.out.println("_______________________________________");
        for (Worker worker : workersList) {
            System.out.println(worker);
        }
    }

}
