package com.company.codewars;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class Metro {
    public static void main(String[] args) {
        ArrayList<int[]> passengers = new ArrayList<>();
        passengers.add(new int[]{15, 0});
        passengers.add(new int[]{3, 5});
        passengers.add(new int[]{9, 3});
        passengers.add(new int[]{1, 10});
        System.out.println(countPassengers(passengers));
        System.out.println(countPassengers2(passengers));

    }

    static int countPassengers(ArrayList<int[]> stops) {
        int passengersLeft = 0;

        for (int[] stop : stops) {
            passengersLeft += (stop[0] - stop[1]);
        }

        return passengersLeft;
    }

    static int countPassengers2(ArrayList<int[]> stops) {
        // returns
        return stops.stream().mapToInt(array -> array[0] - array[1])
                .sum();
    }
}
