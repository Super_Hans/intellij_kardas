package com.company;
/*
Utwórz metodę, do której przekazujesz dwa parametry a ona zwróci ich sumę
> Dla `2` oraz `11` zwróci `13`
>
> Dla `-10` oraz `3` zwróci `-7`
 */
public class Zad7Metod {
    public static void main(String[] args) {
        int result = twoParameters(2,9);
        System.out.println( "result: " + result);

        int two = twoParameters(7,9);
        System.out.println( "result: " + two);

        System.out.println("result: " + (two));

        System.out.println("result: " + twoParameters(45,78));
    }

    private static int twoParameters(int numberOne, int numberTwo) {
        return numberOne + numberTwo;

    }
}
