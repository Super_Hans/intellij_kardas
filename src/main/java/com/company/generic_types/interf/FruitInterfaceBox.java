package com.company.generic_types.interf;

public class FruitInterfaceBox {
    private FruitInterface fruit;

    public FruitInterface getFruit() {
        return fruit;
    }

    public void setFruit(FruitInterface fruit) {
        this.fruit = fruit;
    }

    public FruitInterfaceBox(FruitInterface fruit) {
        this.fruit = fruit;
    }
}
