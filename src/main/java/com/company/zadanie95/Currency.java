package com.company.zadanie95;

public enum Currency {
    USD("dollar","USA"), EUR("euro","UE"), PLN("polski zloty","POLAND");
    //one parameter constructor
    private String description;
    private String country;

    Currency(String description, String country) {
        this.description = description;
        this.country = country;
    }

    public String getDescription() {
        return description;
    }
    public String getCountry () {
        return country;
    }
}
