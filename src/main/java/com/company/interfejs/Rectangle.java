package com.company.interfejs;

public class Rectangle extends Shape {
    protected int width;
    private int high;

    public Rectangle(int width, int high) {
        this.width = width;
        this.high = high;
    }

    @Override
    String showInformationAboutPerson() {
        return String.format(
                "I am rectangle and my sides are %s cm and %s cm",
                width,
                high);
    }

    @Override
    public double calcArea() {
        return width * high;
    }

    @Override
    public double calcVolume() {
        return calcVolume()*2;
    }

    @Override
    public double calcCirc() {
        return 2 * width + 2 * high;
    }
}
