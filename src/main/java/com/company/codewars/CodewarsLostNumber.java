package com.company.codewars;

public class CodewarsLostNumber {
    public static void main(String[] args) {
        int[] tab1 = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] tab2 = new int[]{1, 2, 3, 4, 5, 6, 8, 9};
        System.out.println(findDeletedNumber(tab1, tab2));
        System.out.println(findDeletedNum(tab1, tab2));

    }


    private static int findDeletedNumber(int[] arr, int[] mixedArr) {

        if (arr.length == mixedArr.length) {
            return 0;
        }

        external:
        for (int anArr : arr) {  // pętla foreach działa szybiej jak leci
            for (int aMixedArr : mixedArr) {
                if (anArr == aMixedArr) {
                    continue external; // przerywa obecne wykonanie pętli i idzie dalej zewnetrzną pętlą
                }
            }
            return anArr;
        }
        return 0;
    }

    static int findDeletedNum(int[] arr, int[] mixedArr) {
        int sum1 = 0;
        int sum2 = 0;

        for (int elementZPierwszej : arr) {
            sum1 += elementZPierwszej;
        }

        for (int elementZDrugiej : mixedArr) {
            sum2 += elementZDrugiej;

        }
        return sum1 - sum2;
    }
}
