package com.company.wzorceProjektowe6;

class Observer {
    private String name;
    private int maxTemp;
    private int minTemp;

    Observer(String name, int maxTemp, int minTemp) {
        this.name = name;
        this.maxTemp = maxTemp;
        this.minTemp = minTemp;
    }

    String getName() {
        return name;
    }

    int getMaxTemp() {
        return maxTemp;
    }

    int getMinTemp() {
        return minTemp;
    }

    void react(int currentTemp) {
        String message = String.format("%s says that %s degrees is too %%s. " +
                        "For %s %%s temperature is %%s\n",
                name,
                currentTemp,
                name);

        if (currentTemp >= maxTemp) {
            System.out.printf(message, " hot", "max", maxTemp);

        } else if (currentTemp <= minTemp) {
            System.out.printf(message, "cold", "min", minTemp);
        }
    }
}
