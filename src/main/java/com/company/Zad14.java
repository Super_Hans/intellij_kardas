package com.company;

/*Utwórz metodę, która zwróci informację (w formie wartości logicznej) czy przekazana liczba jest parzysta czy nie.
>Dla `12` zwroci `true`
>
>Dla `9` zwróci `false`
>
>Dla `1` zwróci `false`
>
>Dla `40` zwróci `true`
>
>Dla `123` zwróci `false`
*/
public class Zad14 {
    public static void main(String[] args) {
        System.out.println(czyParzysta(9));
        System.out.println(czyParzysta(18));
        System.out.println(czyParzysta2(18));
    }

    static boolean czyParzysta(int liczba) {
        if (liczba % 2 == 0) {
            return true;
        }
        return false;

    }

    static boolean czyParzysta2(int liczba) {
        return (liczba % 2 == 0);
    }
}
