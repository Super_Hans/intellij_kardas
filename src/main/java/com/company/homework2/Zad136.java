package com.company.homework2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//Maciej Czapla - TRENER [11:30 AM]
//*ZADANIE #136 - PRACA DOMOWA*
//W załączniku jest plik z loginami i hasłami. Waszym zadaniem jest zrobienie analizy tego pliku oraz napisanie metod które *ZWRÓCĄ*:
//
//- `[double]` informację jaki % osób miało taki sam login jak hasło (np. `anka40:anka40`)
//- `[int]` informację ilu użytkowników “wyciekło”
//- `[int]` informację ilu użytkowników NIE miało cyfry w haśle
//- `[double]` informację jaki % użytkowników nie używało dużych liter w haśle
//- `[int]` informację ile użytkowników miało hasło krótsze niż 5 znaków (sprawdzana długość hasła powinna być parameterem metody)
//- `[Map<Integer, Integer>]` informację jaki był rozkład długości haseł (ile razy wystapiła długość 1, ile 2, ile 3...)
//- `[List<Integer>]` listę 10 najpopularniejszych haseł
//- `[Map<String, Integer>]`  ile razy wystąpiła każda ze skrzynek mailowych (oczywiście jeśli loginem był mail) (edited)
public class Zad136 {
    private static final List<String> listOfLogin = new ArrayList<>();
    private static final String SEPARATOR = ":";
    private static final Double PERCENT = 100.0;

    public static void main(String[] args) {
        try {
            //System.out.println(whatPercentOfSameLogins("files/passwd.txt"));
            System.out.println(listOfLogins("files/passwd.txt"));
        } catch (IOException e) {
            System.out.println("Error in file reading");
            return;
        }
        System.out.println("Number of users: " + userCount());
    }

    private static List<String> listOfLogins(String filePath) throws IOException {
        List<String> listOfLogin = new ArrayList<>();

        BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
        String currentLine = bufferedReader.readLine();

        while (currentLine != null) {
            listOfLogin.add(currentLine);
            currentLine = bufferedReader.readLine();
        }
        return listOfLogin;

    }

    private static int userCount() {
        int counter = 0;
        for (String s : listOfLogin) {
            counter++;
        }
        return counter;
    }

}
//    }
//    private static int passLessThan (int number){
//        int counter = 0;
//
//
//    }
//
//    private static double whatPercentOfEqualPassAndLogin() throws IOException {
//
//        double counter = 0;
//        double lineCount = 0;
//        for (String s : listOfLogin) {
//
//
//        }
//    }
//}
////  private static double whatPercentOfEqualPassAndLogin() {}
////
////        double counter = 0.0;
////        double lineCount = 0.0;
////
////        while (line != null) {
////            String[] stringArr = line.split(SEPARATOR);
////            if (stringArr[1].equals(stringArr[0])) {
////                counter++;
////            }
////            lineCount++;
////          //  line = bufferedReader.readLine();
////
////        }
////
////        return (counter / lineCount) * PERCENT;
////    }
