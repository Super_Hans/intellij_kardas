package com.company;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Zad106 {
    public static void main(String[] args) {
        System.out.println(getWholeSet());

    }
    // bunch of variables, unique but not sorted!!!
    private static Set<Integer> getWholeSet() {
        //returns set of numbers
        Set<Integer> correctSet = new HashSet<>();
        // declaration of variable
        int value;

        Scanner s = new Scanner(System.in);
        // infinite loop
        while (true) {
            System.out.println("Give number: ");
            // variable takes value from Scanner
            value = s.nextInt();
            // check if you can add another value - Set cannot have duplicates!!!
            boolean canAdd = correctSet.add(value);
            System.out.println(canAdd);
            if (!canAdd) {
                // breaks loop if condition is true
                break;
            }
        }
        return correctSet;
    }
}
