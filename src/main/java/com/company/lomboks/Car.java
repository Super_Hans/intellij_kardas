package com.company.lomboks;

import lombok.*;

@Getter
@ToString
@AllArgsConstructor
@EqualsAndHashCode
@Setter
public class Car {
    private String name;
    @Setter(AccessLevel.NONE)
    private String colour;
    private int numberOfDoors;
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.PROTECTED)
    private int yearOfProduction;
}
